#pragma once
#include <string>
#include <vector>
#include "common.h"
#include "resources.h"
#include "resource_object.h"
using namespace std;

namespace Teng{
	class SystemSeObject : public ResourceObject{
	public:
		SystemSeObject(vector<string> command);
		void play(Config* config);
		int getPlayLoop(){ return play_loop; }
	private:
	};

}