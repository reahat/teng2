#pragma once
#include "DxLib.h"
#include "exception.h"
#include "resources.h"
#include "util.hpp"

namespace Teng{
	class Initializer{
	public:
		Initializer(){};
		~Initializer(){};
		static void initialize();
		static void loadResources(Resources* resources);
	};
}