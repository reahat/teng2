#pragma once
#include "common.h"
#include "scene_state.h"
#include "scene_logo.h"
#include "scene_title.h"
#include "scene_scenario.h"
#include "scene_config.h"
#include "scene_backlog.h"
#include "scene_menu.h"
#include "resources.h"
#include <memory>

using namespace std;
namespace Teng{
	class Scene{
	public:
		Scene(Resources* resources, Input* input, Config* config);
		~Scene();
		void preProcess();
		void postProcess(Resources* resources, Input* input, Config* config);
		scene_t getCurrentState();
		void changeState(SceneState* new_scene);

		void checkInput();
		void drawGraphic();
		void operateButton();
		void doScene();
		void operateDialog();
		void operateSound();
		void execFade();
		void addFrame(int add_count = 1);
		scene_t getNextScene();
	private:
		unique_ptr<SceneState> state;
		
	};
	
}