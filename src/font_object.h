#pragma once
#include <string>
#include <vector>
#include "common.h"
#include "resources.h"
#include "resource_object.h"
using namespace std;

namespace Teng{
	class FontObject : public ResourceObject{
	public:
		FontObject(vector<string> command);
	};

}