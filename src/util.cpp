#include "util.hpp"
#include "DxLib.h"

using namespace Teng;


Util::Fps::Fps() :
counter(0),
FpsTime_i(0),
fps(0.0)
{
	FpsTime[0] = 0;
	FpsTime[1] = 0;
	color_white = DxLib::GetColor(255, 255, 255);
}
void Util::Fps::timer(){
	if (FpsTime_i == 0)
		FpsTime[0] = GetNowCount();               //1周目の時間取得
	if (FpsTime_i == 19){
		FpsTime[1] = GetNowCount();               //50周目の時間取得
		fps = 1000.0f / ((FpsTime[1] - FpsTime[0]) / 20.0f);//測定した値からfpsを計算
		FpsTime_i = 0;//カウントを初期化
	}
	else
		FpsTime_i++;//現在何周目かカウント
	if (fps != 0)
		DrawFormatString(3, 3, color_white, "FPS %.1f", fps); //fpsを表示
	return;
}