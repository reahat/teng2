#pragma once
#include "error_code.h"
#include "error_object.h"
#include <map>
#include <string>

using namespace std;

// エラー一覧クラス
// このクラスは起動時に初期化すること
namespace Teng{
	class Errors{
	public:
		Errors(){};
		~Errors(){};
		static void         initialize();
		static ErrorObject* find(const error_t error_code);
	private:
		static map<int, string> error_map;
	};
}