#include "scene_title.h"

using namespace Teng;

SceneTitle::Button::Button(){
	
}

void SceneTitle::buttonReset(){
	start.now = start.normal;
	load.now = load.normal;
	b_config.now = b_config.normal;
	gallery.now = gallery.normal;
	exit.now = exit.normal;
}


SceneTitle::SceneTitle(Resources* resources, Input* input, Config* config) :
frame(0),
fade_type(FadeType::fadein),
bgm_state(FadeType::stop),
next_scene(SceneType::no_change),
tmp_next_scene(SceneType::no_change),
fade_completed(false),
button_clicked(false)
{
	this->resources = resources;
	this->input = input;
	this->config = config;

	// タイトル画面で使用する画像を使いやすいようセット
	for (auto img : resources->getSystemImages()){
		switch (img->getOptionType()){
		case SystemImage::bg_title:
			back_image = img->getHandle();
			break;
		case SystemImage::button_title_start_normal:
			start.now = start.normal = img->getHandle();
			DxLib::GetGraphSize(start.normal, &start.width, &start.height);
			break;
		case SystemImage::button_title_start_focus:
			start.focus = img->getHandle();
			break;
		case SystemImage::button_title_start_click:
			start.click = img->getHandle();
			break;
		case SystemImage::button_title_load_normal:
			load.now = load.normal = img->getHandle();
			DxLib::GetGraphSize(load.normal, &load.width, &load.height);
			break;
		case SystemImage::button_title_load_focus:
			load.focus = img->getHandle();
			break;
		case SystemImage::button_title_load_click:
			load.click = img->getHandle();
			break;
		case SystemImage::button_title_config_normal:
			b_config.now = b_config.normal = img->getHandle();
			DxLib::GetGraphSize(b_config.normal, &b_config.width, &b_config.height);
			break;
		case SystemImage::button_title_config_focus:
			b_config.focus = img->getHandle();
			break;
		case SystemImage::button_title_config_click:
			b_config.click = img->getHandle();
			break;
		case SystemImage::button_title_gallery_normal:
			gallery.now = gallery.normal = img->getHandle();
			DxLib::GetGraphSize(gallery.normal, &gallery.width, &gallery.height);
			break;
		case SystemImage::button_title_gallery_focus:
			gallery.focus = img->getHandle();
			break;
		case SystemImage::button_title_gallery_click:
			gallery.click = img->getHandle();
			break;
		case SystemImage::button_title_exit_normal:
			exit.now = exit.normal = img->getHandle();
			DxLib::GetGraphSize(exit.normal, &exit.width, &exit.height);
			break;
		case SystemImage::button_title_exit_focus:
			exit.focus = img->getHandle();
			break;
		case SystemImage::button_title_exit_click:
			exit.click = img->getHandle();
			break;
		default:
			break;
		}
	}

	for (auto bgm_obj : resources->getSystemBgms()){
		switch (bgm_obj->getOptionType()){
		case SystemSound::bgm_title:
			title_bgm = bgm_obj;
			break;
		default:
			break;
		}
	}
}

void SceneTitle::preProcess(){
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::ClearDrawScreen();
}

void SceneTitle::postProcess(){
	DxLib::ScreenFlip();
	addFrame(1);

}

scene_t SceneTitle::getCurrentState(){
	return SceneType::title;
}

void SceneTitle::drawGraphic(){
	//背景
	DxLib::DrawGraph(0, 0, back_image, FALSE);
	//ボタン
	DxLib::DrawGraph(BUTTON_X, BUTTON_Y_1, start.now, TRUE);
	DxLib::DrawGraph(BUTTON_X, BUTTON_Y_2, load.now, TRUE);
	DxLib::DrawGraph(BUTTON_X, BUTTON_Y_3, b_config.now, TRUE);
	DxLib::DrawGraph(BUTTON_X, BUTTON_Y_4, gallery.now, TRUE);
	DxLib::DrawGraph(BUTTON_X, BUTTON_Y_5, exit.now, TRUE);


}

void SceneTitle::checkInput(){
	input->checkKeyInput();
	
	bool cursor_in_button = false;
	if (input->cursorIn(BUTTON_X, BUTTON_X + start.width, BUTTON_Y_1, BUTTON_Y_1 + start.height) && !button_clicked){
		buttonReset();
		start.now = start.focus;
		cursor_in_button = true;
		if (input->mouseLeftClicked()){
			tmp_next_scene = SceneType::scenario;
			button_clicked = true;
			start.now = start.click;
			frame = 0;
		}
	}
	else if (input->cursorIn(BUTTON_X, BUTTON_X + load.width, BUTTON_Y_2, BUTTON_Y_2 + load.height) && !button_clicked){
		buttonReset();
		load.now = load.focus;
		cursor_in_button = true;
		if (input->mouseLeftClicked()){
			tmp_next_scene = SceneType::title_load;
			button_clicked = true;
			load.now = load.click;
			frame = 0;
		}
	}
	else if (input->cursorIn(BUTTON_X, BUTTON_X + b_config.width, BUTTON_Y_3, BUTTON_Y_3 + b_config.height) && !button_clicked){
		buttonReset();
		b_config.now = b_config.focus;
		cursor_in_button = true;
		if (input->mouseLeftClicked()){
			b_config.now = b_config.click;
			button_clicked = true;
			tmp_next_scene = SceneType::title_config;
			frame = 0;
		}
	}
	else if (input->cursorIn(BUTTON_X, BUTTON_X + gallery.width, BUTTON_Y_4, BUTTON_Y_4 + gallery.height) && !button_clicked){
		buttonReset();
		gallery.now = gallery.focus;
		cursor_in_button = true;
		if (input->mouseLeftClicked()){
			tmp_next_scene = SceneType::gallery;
			button_clicked = true;
			gallery.now = gallery.click;
			frame = 0;
		}
	}
	else if (input->cursorIn(BUTTON_X, BUTTON_X + exit.width, BUTTON_Y_5, BUTTON_Y_5 + exit.height) && !button_clicked){
		buttonReset();
		exit.now = exit.focus;
		cursor_in_button = true;
		if (input->mouseLeftClicked()){
			tmp_next_scene = SceneType::scene_exit;
			button_clicked = true;
			exit.now = exit.click;
			frame = 0;
		}
	}
	if (!cursor_in_button && !button_clicked) buttonReset();

}

void SceneTitle::doScene(){

	switch (tmp_next_scene){
	case SceneType::scenario:
		fade_type = FadeType::fadeout;
		if (fade_completed) next_scene = SceneType::scenario;
		break;
	case SceneType::title_load:
		fade_type = FadeType::fadeout;
		break;
	case SceneType::title_config:
		fade_type = FadeType::fadeout;
		if (fade_completed) next_scene = SceneType::title_config;
		break;
	case SceneType::gallery:
		fade_type = FadeType::fadeout;
		break;
	case SceneType::scene_exit:
		fade_type = FadeType::fadeout;
		if (fade_completed)	next_scene = SceneType::scene_exit;
		break;
	default:
		break;
	}

}

void SceneTitle::operateButton(){

}

void SceneTitle::operateDialog(){

}

void SceneTitle::operateSound(){
	if (bgm_state == FadeType::no_fade && !title_bgm->isPlaying()){
		title_bgm->play(config);
	}

}

void SceneTitle::execFade(){

	//画面開始時
	if (fade_type == FadeType::fadein){
		int f = static_cast<int>(frame)* 10;
		if (f < 255){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadein){
				fade_type = FadeType::no_fade;
				DxLib::SetDrawBright(255, 255, 255);
				bgm_state = FadeType::no_fade;
			}
		}
	}
	//次の画面への遷移時
	else if(fade_type == FadeType::fadeout){
		int f = 255 - static_cast<int>(frame)* 10;
		if (f > 0){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadeout){
				fade_completed = true;
			}
		}
	}
}

void SceneTitle::addFrame(int add_count){
	if (frame == ULONG_MAX) frame = 0;
	frame += add_count;
}

bool SceneTitle::cursorInButton(ResourceObject& resource){
	if (input->cursorX() < resource.getX()) return false;
	if (input->cursorX() >= resource.getX() + resource.getWidth()) return false;
	if (input->cursorY() < resource.getY()) return false;
	if (input->cursorY() >= resource.getY() + resource.getHeight()) return false;
	return true;
}