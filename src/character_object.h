#pragma once
#include <string>
#include <vector>
#include "common.h"
#include "resource_object.h"
using namespace std;

namespace Teng{
	class CharacterObject : public ResourceObject{
	public:
		CharacterObject(vector<string> command);
	};

}