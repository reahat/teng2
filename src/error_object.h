#pragma once
#include "common.h"
#include "error_code.h"
#include <string>
#include <map>
using namespace std;

namespace Teng{
	class ErrorObject : public std::domain_error{
	public:
		ErrorObject(const error_t error_code, string error_msg);
		~ErrorObject(){};
		error_t getErrorCode(){ return error_code; }
	private:
		error_t error_code;
	};
}