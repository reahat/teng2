#include "command_common.h"

using namespace Teng;


CmdFaceSet::~CmdFaceSet(){
	
}

CmdFaceSet::CmdFaceSet(vector<string> text_command, Resources* resources){
	// [0] tag
	// [1] file
	this->resources = resources;
	args = text_command;
	tag = text_command[0];
	filename = text_command[1];
	force_next = true;
}

void CmdFaceSet::execute(){

	for (auto img : resources->getFaces()){
		if (img->getFileName() == args[1]){
			img->isVisible(true);
		}
		else{
			img->isVisible(false);
		}
	}
	is_executed = true;
	putLog();
}