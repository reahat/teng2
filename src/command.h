#pragma once
#include <string>
#include <vector>
#include "resources.h"
//#include "scene_scenario.h"
#include <memory>


using namespace std;

namespace Teng{
	class CommandQueue;
	class SceneScenario;
	class Command{
	public:
		Command();
		virtual ~Command();
		//virtual void beforeExecute(SceneScenario* scene_scenario){};
		virtual void beforeExecute(void* arg1){};
		virtual void execute() = 0;
		virtual int draw(){ return 0; }
		virtual void setInput(const int any_input){}
		
		//SceneScenario側からコマンドへ介入する
		virtual int send(vector<Command*>& queue, int& it_num){ return 0; }
		//virtual int send(unique_ptr<CommandQueue> command_queue){ return 0; }


		vector<string> getArgs(){ return args; }
		const bool isExecuted() const { return is_executed; }
		void       isExecuted(const bool is_executed){ this->is_executed = is_executed; }
		const bool isForceNext()const{ return force_next; }
		void  putLog();
	protected:
		vector<string> args;
		Resources* resources;
		SceneScenario* scene_scenario;
		bool is_executed;
		bool force_next;

	};

}