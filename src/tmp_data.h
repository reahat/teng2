#pragma once
#include "scene_scenario.h"

namespace Teng{
	class TmpData{

	public:
		TmpData();
		~TmpData(){};
		void   tmpScriptFileName(const string file){ tmp_script_file_name = file; }
		string tmpScriptFileName(){ return tmp_script_file_name; }
		void tmpScenarioExist(const bool is_exist){ tmp_scenario_exist = is_exist; }
		bool tmpScenarioExist(){ return tmp_scenario_exist; }
		void tmpScreenshot(const int handle){ tmp_screenshot = handle; }
		int  tmpScreenshot(){ return tmp_screenshot; }
	private:
		bool tmp_scenario_exist;
		string tmp_script_file_name;
		int tmp_screenshot;
	};




}
