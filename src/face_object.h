#pragma once
#include <string>
#include <vector>
#include "common.h"
#include "resources.h"
#include "resource_object.h"
using namespace std;

namespace Teng{
	class FaceObject : public ResourceObject{
	public:
		FaceObject(vector<string> command);
	};

}