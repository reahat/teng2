#include "command_common.h"

using namespace Teng;


CmdGoto::CmdGoto(vector<string> text_command, Resources* resources){
	this->resources = resources;
	args = text_command;
	force_next = true;
}

void CmdGoto::execute(){
	is_executed = true;
	DEBUG putLog();
}


int CmdGoto::send(vector<Command*>& queue, int& it_num){
	
	int local_it_num = 0;
	bool goto_completed = false;
	for (auto cmd : queue){
		if (goto_completed){
			cmd->isExecuted(false);
		}
		else{
			cmd->isExecuted(true);
		}

		// goto の飛び先とラベル名が一致したとき
		if (cmd->getArgs()[0] == "@label" && !goto_completed){
			if (cmd->getArgs()[1] == queue[it_num]->getArgs()[1]){
				it_num = local_it_num;
				cmd->isExecuted(true);
				goto_completed = true;
			}
		}
		local_it_num++;
	}


	return it_num;
}