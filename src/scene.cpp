#include "scene.h"
#include "tmp_data.h"
using namespace Teng;

Scene::Scene(Resources* resources, Input* input, Config* config){
	state.reset(new SceneLogo(resources, input, config));
}

Scene::~Scene(){

}

void Scene::preProcess(){
	state->preProcess();
}

void Scene::postProcess(Resources* resources, Input* input, Config* config){
	state->postProcess();
	if (state->getNextScene() != SceneType::no_change){
		switch (state->getNextScene()){
		case SceneType::title:
			log_info("Change scene to 'Title'.");
			changeState(new SceneTitle(resources, input, config));
			break;
		case SceneType::scenario:
			log_info("Change scene to 'Scenario'.");
			changeState(new SceneScenario(resources, input, config));
			break;
		case SceneType::backlog:
			log_info("Change scene to 'Backlog'.");
			changeState(new SceneBacklog(resources, input, config));
			break;
		case SceneType::title_config:
			log_info("Change scene to 'TitleConfig'.");
			changeState(new SceneConfig(resources, input, config));
			break;
		case SceneType::menu:
			log_info("Change scene to 'Menu'.");
			changeState(new SceneMenu(resources, input, config));
			break;
		case SceneType::scene_exit:
			log_info("Change scene; Exit");
			RAISE(ENGINE_SYSTEM_STANDARD_EXIT);
			break;
		default:
			break;
		}
	}
}

scene_t Scene::getCurrentState(){
	return state->getCurrentState();
}

void Scene::changeState(SceneState* new_scene){
	state.reset(new_scene);
}

void Scene::checkInput(){
	state->checkInput();
}

void Scene::drawGraphic(){
	state->drawGraphic();
}

void Scene::operateButton(){
	state->operateButton();
}

void Scene::doScene(){
	state->doScene();
}

void Scene::operateDialog(){
	state->operateDialog();
}

void Scene::operateSound(){
	state->operateSound();
}

void Scene::execFade(){
	state->execFade();
}

void Scene::addFrame(int add_count){
	state->addFrame(add_count);
}

scene_t Scene::getNextScene(){
	return state->getNextScene();
}