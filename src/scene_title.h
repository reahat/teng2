#pragma once
#include "scene_state.h"

namespace Teng{
	class SceneTitle : public SceneState{
	public:
		SceneTitle(Resources* resources, Input* input, Config* config);
		void preProcess();
		void postProcess();
		scene_t getCurrentState();

		void checkInput();
		void drawGraphic();
		void operateButton();
		void doScene();
		void operateDialog();
		void operateSound();
		void execFade();
		void addFrame(int add_count);
		scene_t getNextScene(){ return next_scene; }
		
	private:
		Resources* resources;
		Input* input;
		Config* config;

		const int BUTTON_X = 14;
		const int BUTTON_Y_1 = 250;
		const int BUTTON_Y_2 = 295;
		const int BUTTON_Y_3 = 335;
		const int BUTTON_Y_4 = 380;
		const int BUTTON_Y_5 = 427;
		ULONG frame;
		scene_t next_scene;
		scene_t tmp_next_scene; // next_sceneをセットすると自動的にシーン遷移するので注意
		int   fade_type;
		bool  fade_completed;
		bool button_clicked;

		bool   cursorInButton(ResourceObject& resource);
		handle back_image;

		class Button{
		public:
			Button();
			handle now;
			handle normal;
			handle focus;
			handle click;
			int    width;
			int    height;
		};

		Button start;
		Button load;
		Button b_config;
		Button gallery;
		Button exit;
		void   buttonReset();

		int bgm_state;
		ResourceObject* title_bgm;
		ResourceObject* se_button_focus;
		ResourceObject* se_button_click;
		
	};

}