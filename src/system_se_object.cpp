#include "system_se_object.h"

using namespace Teng;

SystemSeObject::SystemSeObject(vector<string> command){
	// [0] ディレクトリパス
	// [1] option_type
	// [2] ファイル名
	// [3] volume
	// [4] loop_point 数値またはloopを指定。loopの場合は最初からループ
	string fullpath = command[0] + command[2];

	DxLib::SetCreateSoundDataType(DX_SOUNDDATATYPE_FILE);

	handle = DxLib::LoadSoundMem(fullpath.c_str());
	if (!Resources::isFileExist(fullpath)){
		log_error("システム音声の読み込みに失敗しました: %s", command[2].c_str());
		RAISE(ENGINE_SOUND_SYSTEM_LOAD_FAILED);
	}
	file_name = command[2];
	option_type = stoi(command[1]);
	volume = stoi(command[3]);
	if (command.size() > 4){
		if (command[4] == "loop"){
			loop_point = 0;
			play_loop = DX_PLAYTYPE_LOOP;
		}
		else if (command[4] == "once"){
			loop_point = 0;
			play_loop = DX_PLAYTYPE_BACK;
		}
		else{
			loop_point = stoi(command[4]);
			DxLib::SetLoopPosSoundMem(stoi(command[4]), handle);
			play_loop = DX_PLAYTYPE_LOOP;
		}
	}
	else{
		loop_point = 0;
		play_loop = DX_PLAYTYPE_BACK;
	}
	fade_power = 0;
	fade_type = FadeType::no_fade;
	is_playing = false;
	log_info("Sound File loading : %s", file_name.c_str());
}

void SystemSeObject::play(Config* config){
	ConfigData& cf = config->getSetting();
	double cf_vol = (static_cast<double>(cf.master_volume) / 255.0) * (static_cast<double>(cf.sound_volume) / 255.0);
	double play_vol = 255 * cf_vol * (static_cast<double>(volume) / 255.0);
	DxLib::ChangeVolumeSoundMem(static_cast<int>(play_vol), handle);
	if (DxLib::PlaySoundMem(handle, play_loop, TRUE) == -1)
		RAISE(ENGINE_SOUND_SYSTEM_PLAY_FAILED);
	is_playing = true;
}