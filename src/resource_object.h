#pragma once
#include <string>
#include "common.h"
#include "DxLib.h"
#include "config.h"
#include "exception.h"

using namespace std;

namespace Teng{
	class ResourceObject{
	public:
		//共通
		ResourceObject(){ to_delete = false; }
		string getFileName(){ return file_name; }
		handle getHandle(){ return handle; }
		int    getOptionType(){ return option_type; }
		void   reserveDelete(){ to_delete = true; }

		//画像用
		const int	getX() const { return x; }
		const int	getY() const { return y; }
		const int	getZ() const { return z; }
		const int	getWidth()	const { return width; }
		const int	getHeight() const { return height; }
		const int	isVisible()	const { return is_visible; }
		void		isVisible(const bool is_visible){ this->is_visible = is_visible; }
		void        setTransitionSpeed(const int speed){ transition_speed = speed; }
		const int   getTransitionSpeed(){ return transition_speed; }
		void        setBlendThreshold(const int threshold){ blend_threshold = threshold; }
		const int   getBlendThreshold(){ return blend_threshold; }
		// 画像の濃さはVolumeを使う

		//音声用
		void   setVolume(const int volume){ this->volume = volume; }
		int    getVolume(){ return volume; }
		int    getFadePower(){ return fade_power; }
		int    getFadeType(){ return fade_type; }
		void   setFadeType(const int fade_type){ this->fade_type = fade_type; }
		bool   isPlaying(){ return is_playing; }
		void   isPlaying(const bool is_playing){ this->is_playing = is_playing; }
		int    getPlayLoop(){ return play_loop; }
		virtual void play(Config* config);
		virtual void stop();
		virtual void fadein();
		virtual void fadeout();


	protected:
		//共通
		string file_name;
		handle handle;
		int    option_type;
		bool   to_delete;

		//画像用
		int    x;
		int    y;
		int    z;
		int    width;
		int    height;
		bool   is_visible;//使用中かどうか
		bool   is_changing;//画像切替中かどうか
		int    transition_type;//トランジションエフェクトの書類
		int    transition_speed;//トランジション速度

		//ルール関係
		bool   use_rule;//ルール使用する場合。トラン完了後はfalseに戻す
		int    blend_power;//ルール画像との合成率（ループで0-255まで変化）
		int    blend_threshold;//ルール合成のしきい値
		string rule_file;
		bool   is_after_trans;
		int    rule_handle;//使用するルール画像のハンドル

		//音声用
		int  volume;
		int  fade_power;
		int  fade_type;
		int  loop_point;
		bool is_playing;
		int  play_loop;
		//configオブジェクトへの参照をshared_ptrで持つ
	};
	

	
}