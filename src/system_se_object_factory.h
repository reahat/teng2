#pragma once
#include "resource_object_factory.h"
#include "system_se_object.h"

namespace Teng{
	class SystemSeObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}