#include "command_common.h"

using namespace Teng;

CmdRuleFree::CmdRuleFree(vector<string> text_command, Resources* resources){
	// [0] tag
	// [1] filename
	this->resources = resources;
	args = text_command;
	filename = text_command[1];
	force_next = true;
}

CmdRuleFree::~CmdRuleFree(){

}

void CmdRuleFree::execute(){
	resources->erase(ResourceType::rule, filename);

	is_executed = true;
	DEBUG putLog();
}