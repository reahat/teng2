#include "scene_menu.h"
#include "tmp_data.h"
using namespace Teng;

SceneMenu::Button::Button():
power(0),
x(0)
{
	
}



SceneMenu::SceneMenu(Resources* resources, Input* input, Config* config) :
frame(0),
fade_type(FadeType::fadein),
next_scene(SceneType::no_change),
back_alpha(0)
{
	this->resources = resources;
	this->input = input;
	this->config = config;

	// 背景色 #1b0805 透過60%
	back_color = DxLib::GetColor(27, 8, 5);

	for (auto img : resources->getSystemImages()){
		switch (img->getOptionType()){
		case SystemImage::button_menu_save_normal:
			btn_save.now = btn_save.normal = img->getHandle();
			DxLib::GetGraphSize(btn_save.normal, &btn_save.width, &btn_save.height);
			btn_save.y = 168;
			break;
		case SystemImage::button_menu_load_normal:
			btn_load.now = btn_load.normal = img->getHandle();
			DxLib::GetGraphSize(btn_load.normal, &btn_load.width, &btn_load.height);
			btn_load.y = 218;
			break;
		case SystemImage::button_menu_hskip_normal:
			btn_hskip.now = btn_hskip.normal = img->getHandle();
			DxLib::GetGraphSize(btn_hskip.normal, &btn_hskip.width, &btn_hskip.height);
			btn_hskip.y = 266;
			break;
		case SystemImage::button_menu_config_normal:
			btn_config.now = btn_config.normal = img->getHandle();
			DxLib::GetGraphSize(btn_config.normal, &btn_config.width, &btn_config.height);
			btn_config.y = 315;
			break;
		case SystemImage::button_menu_backlog_normal:
			btn_backlog.now = btn_backlog.normal = img->getHandle();
			DxLib::GetGraphSize(btn_backlog.normal, &btn_backlog.width, &btn_backlog.height);
			btn_backlog.y = 365;
			break;
		case SystemImage::button_menu_title_normal:
			btn_title.now = btn_title.normal = img->getHandle();
			DxLib::GetGraphSize(btn_title.normal, &btn_title.width, &btn_title.height);
			btn_title.y = 420;
			break;
		case SystemImage::button_menu_exit_normal:
			btn_exit.now = btn_exit.normal = img->getHandle();
			DxLib::GetGraphSize(btn_exit.normal, &btn_exit.width, &btn_exit.height);
			btn_exit.y = 470;
			break;
		case SystemImage::button_menu_back_normal:
			btn_back.now = btn_back.normal = img->getHandle();
			DxLib::GetGraphSize(btn_back.normal, &btn_back.width, &btn_back.height);
			btn_back.y = 520;
			break;

		case SystemImage::button_menu_save_focus:
			btn_save.focus = img->getHandle();
			break;
		case SystemImage::button_menu_load_focus:
			btn_load.focus = img->getHandle();
			break;
		case SystemImage::button_menu_hskip_focus:
			btn_hskip.focus = img->getHandle();
			break;
		case SystemImage::button_menu_config_focus:
			btn_config.focus = img->getHandle();
			break;
		case SystemImage::button_menu_backlog_focus:
			btn_backlog.focus = img->getHandle();
			break;
		case SystemImage::button_menu_title_focus:
			btn_title.focus = img->getHandle();
			break;
		case SystemImage::button_menu_exit_focus:
			btn_exit.focus = img->getHandle();
			break;
		case SystemImage::button_menu_back_focus:
			btn_back.focus = img->getHandle();
			break;

		case SystemImage::button_menu_save_click:
			btn_save.click = img->getHandle();
			break;
		case SystemImage::button_menu_load_click:
			btn_load.click = img->getHandle();
			break;
		case SystemImage::button_menu_hskip_click:
			btn_hskip.click = img->getHandle();
			break;
		case SystemImage::button_menu_config_click:
			btn_config.click = img->getHandle();
			break;
		case SystemImage::button_menu_backlog_click:
			btn_backlog.click = img->getHandle();
			break;
		case SystemImage::button_menu_title_click:
			btn_title.click = img->getHandle();
			break;
		case SystemImage::button_menu_exit_click:
			btn_exit.click = img->getHandle();
			break;
		case SystemImage::button_menu_back_click:
			btn_back.click = img->getHandle();
			break;
		case SystemImage::menu_base:
			menu_base = img->getHandle();
			break;
		default:
			break;
		}
	}
}

void SceneMenu::preProcess(){
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::ClearDrawScreen();
}

void SceneMenu::postProcess(){
	DxLib::ScreenFlip();
	addFrame(1);

}

scene_t SceneMenu::getCurrentState(){
	return SceneType::menu;
}

void SceneMenu::drawGraphic(){
	//シナリオ画面のスクリーンショット
	DxLib::DrawGraph(0, 0, resources->tmpData()->tmpScreenshot(), FALSE);
	//暗くする
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, back_alpha);
	DxLib::DrawBox(0, 0, 1024, 768, back_color, TRUE);
	//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	//メニューベース
	if (fade_type == FadeType::fadein && frame <= 15){
		int pow = 255 / 20 * (frame - 1);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, pow);
		// 移動後の位置がx=20なので、15フレームかけて画面外から20まで移動する必要がある
		// frame * n - d = 20 になるようにすること
		DxLib::DrawGraph(frame * 6 - 70, 0, menu_base, TRUE);
		//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
	else if (fade_type == FadeType::fadeout){
		int pow = frame * 12;
		int alpha = 255 - frame * 12;
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
		DxLib::DrawGraph(20 - pow, 0, menu_base, TRUE);
		//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
	else{
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		DxLib::DrawGraph(20, 0, menu_base, TRUE);
	}

	//ボタン
	// xの最終地点は54
	if (fade_type == FadeType::fadein){
		if (frame > 8){
			if (btn_save.x < 54){
				btn_save.x += 8;
			}
			btn_save.power += 12;
			if (btn_save.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_save.power * 3);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_save.power * 3);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
			}
			else if (btn_save.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_save.power);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
			}
			else if (btn_save.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_save.power - 10);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
			}
		}
		if (frame > 11){
			if (btn_load.x < 54){
				btn_load.x += 8;
			}
			btn_load.power += 12;
			if (btn_load.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_load.power * 3);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_load.power * 3);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
			}
			else if (btn_load.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_load.power);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
			}
			else if (btn_load.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_load.power - 10);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
			}
		}
		if (frame > 14){
			if (btn_hskip.x < 54){
				btn_hskip.x += 8;
			}
			btn_hskip.power += 12;
			if (btn_hskip.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_hskip.power * 3);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_hskip.power * 3);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
			}
			else if (btn_hskip.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_hskip.power);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
			}
			else if (btn_hskip.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_hskip.power - 10);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
			}
		}
		if (frame > 17){
			if (btn_config.x < 54){
				btn_config.x += 8;
			}
			btn_config.power += 12;
			if (btn_config.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_config.power * 3);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_config.power * 3);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
			}
			else if (btn_config.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_config.power);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
			}
			else if (btn_config.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_config.power - 10);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
			}
		}
		if (frame > 20){
			if (btn_backlog.x < 54){
				btn_backlog.x += 8;
			}
			btn_backlog.power += 12;
			if (btn_backlog.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_backlog.power * 3);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_backlog.power * 3);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
			}
			else if (btn_backlog.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_backlog.power);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
			}
			else if (btn_backlog.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_backlog.power - 10);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
			}
		}
		if (frame > 23){
			if (btn_title.x < 54){
				btn_title.x += 8;
			}
			btn_title.power += 12;
			if (btn_title.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_title.power * 3);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_title.power * 3);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
			}
			else if (btn_title.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_title.power);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
			}
			else if (btn_title.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_title.power - 10);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
			}
		}
		if (frame > 26){
			if (btn_exit.x < 54){
				btn_exit.x += 8;
			}
			btn_exit.power += 12;
			if (btn_exit.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_exit.power * 3);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_exit.power * 3);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
			}
			else if (btn_exit.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_exit.power);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
			}
			else if (btn_exit.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_exit.power - 10);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
			}
		}
		if (frame > 29){
			if (btn_back.x < 54){
				btn_back.x += 8;
			}
			btn_back.power += 12;
			if (btn_back.power < 100){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, btn_back.power * 3);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, btn_back.power * 3);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
			}
			else if (btn_back.power < 200){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_back.power);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
			}
			else if (btn_back.power < 255){
				DxLib::SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255 - btn_back.power - 10);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
				DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 230);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
			}
			else{
				//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
				fade_type = FadeType::no_fade;
			}
		}
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}

	if (fade_type == FadeType::no_fade){
		DxLib::DrawGraph(btn_save.x, btn_save.y, btn_save.normal, TRUE);
		DxLib::DrawGraph(btn_load.x, btn_load.y, btn_load.normal, TRUE);
		DxLib::DrawGraph(btn_hskip.x, btn_hskip.y, btn_hskip.normal, TRUE);
		DxLib::DrawGraph(btn_config.x, btn_config.y, btn_config.normal, TRUE);
		DxLib::DrawGraph(btn_backlog.x, btn_backlog.y, btn_backlog.normal, TRUE);
		DxLib::DrawGraph(btn_title.x, btn_title.y, btn_title.normal, TRUE);
		DxLib::DrawGraph(btn_exit.x, btn_exit.y, btn_exit.normal, TRUE);
		DxLib::DrawGraph(btn_back.x, btn_back.y, btn_back.normal, TRUE);
	}

	static const auto calc = [](int& x){return x -= 12; };
	if (fade_type == FadeType::fadeout){
		int alpha = 255 - frame * 14;
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
		DxLib::DrawGraph(calc(btn_save.x), btn_save.y, btn_save.normal, TRUE);
		DxLib::DrawGraph(calc(btn_load.x), btn_load.y, btn_load.normal, TRUE);
		DxLib::DrawGraph(calc(btn_hskip.x), btn_hskip.y, btn_hskip.normal, TRUE);
		DxLib::DrawGraph(calc(btn_config.x), btn_config.y, btn_config.normal, TRUE);
		DxLib::DrawGraph(calc(btn_backlog.x), btn_backlog.y, btn_backlog.normal, TRUE);
		DxLib::DrawGraph(calc(btn_title.x), btn_title.y, btn_title.normal, TRUE);
		DxLib::DrawGraph(calc(btn_exit.x), btn_exit.y, btn_exit.normal, TRUE);
		DxLib::DrawGraph(calc(btn_back.x), btn_back.y, btn_back.normal, TRUE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}


}

void SceneMenu::checkInput(){
	input->checkKeyInput();

	if (input->mouseRightClicked()){
		//next_scene = SceneType::scenario;
		fade_type = FadeType::fadeout;
		frame = 0;
	}
}

void SceneMenu::doScene(){


}

void SceneMenu::operateButton(){

}

void SceneMenu::operateDialog(){

}

void SceneMenu::operateSound(){

}

void SceneMenu::execFade(){

	////画面開始時
	if (fade_type == FadeType::fadein){

		int f = static_cast<int>(frame) * 15; // 15はフェード速度
		// 255 * 60% = 153
		if (f < 153){
			back_alpha = f;
		}
		else{
			if (fade_type == FadeType::fadein){
				back_alpha = 153;
			}
		}
		if (frame >= 60){
			//fade_type = FadeType::no_fade;
		}
	}

	if (fade_type == FadeType::fadeout){
		int f = static_cast<int>(frame)* 2;
		back_alpha -= f;
		if (back_alpha <= 0){
			next_scene = SceneType::scenario;
		}
	}

	//if (fade_type == FadeType::fadein){
	//	int f = static_cast<int>(frame)* 10;
	//	if (f < 255){
	//		DxLib::SetDrawBright(f, f, f);
	//	}
	//	else{
	//		if (fade_type == FadeType::fadein){
	//			fade_type = FadeType::no_fade;
	//			DxLib::SetDrawBright(255, 255, 255);
	//		}
	//	}
	//}
	////次の画面への遷移時
	//else if(fade_type == FadeType::fadeout){
	//	int f = 255 - static_cast<int>(frame)* 10;
	//	if (f > 0){
	//		DxLib::SetDrawBright(f, f, f);
	//	}
	//	else{
	//		if (fade_type == FadeType::fadeout){

	//		}
	//	}
	//}
}

void SceneMenu::addFrame(int add_count){
	if (frame == ULONG_MAX) frame = 0;
	frame += add_count;
}
