#include "scene_logo.h"
#include <filesystem>
#include <algorithm>
#include "command_queue.h"
#ifdef _DEBUG
#include <chrono>
#endif

using namespace Teng;

SceneLogo::SceneLogo(Resources* resources, Input* input, Config* config) : 
frame(0),
first_load_flg(true),
fade_type(FadeType::fadein),
next_scene(SceneType::no_change)
{
	this->resources = resources;
	this->input = input;
	this->config = config;
}

void SceneLogo::preProcess(){
	if (DxLib::ProcessMessage() != 0) RAISE(ENGINE_ERROR_DEFAULT);
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::ClearDrawScreen();
}

void SceneLogo::postProcess(){
	DxLib::ScreenFlip();
	addFrame(1);
}

scene_t SceneLogo::getCurrentState(){
	return SceneType::logo;
}

void SceneLogo::checkInput(){
	input->checkKeyInput();
}

void SceneLogo::drawGraphic(){
	list<ResourceObject*> list = resources->getSystemImages();
	for (auto it = list.begin(); it != list.end(); it++){
		// 背景
		if ((*it)->getOptionType() == SystemImage::bg_logo_1){
			DrawGraph(0, 0, (*it)->getHandle(), FALSE);
		}
	}


}

void SceneLogo::operateButton(){

}

void SceneLogo::doScene(){
	// 起動時のロード処理
	if (first_load_flg){
		first_load_flg = false;
		const string TENG_GRAPHIC_SYSTEM_GRAPHIC_MSG_WINDOW_FILE = "msg_window.png";
		const string TENG_GRAPHIC_BACKGROUND_TITLE = "start_tate.jpg";
		const string TENG_GRAPHIC_BACKGROUND_LOGO_1 = "logo.png";
		const string TENG_GRAPHIC_BACKGROUND_SAVE_1 = "save.jpg";
		const string TENG_GRAPHIC_BACKGROUND_LOAD_1 = "load.jpg";
		const string TENG_GRAPHIC_BACKGROUND_CONFIG = "config_back.jpg";
		const string TENG_GRAPHIC_SYSTEM_DIALOG_CLOSE = "confirm_exit.png";
		const string TENG_GRAPHIC_SYSTEM_DIALOG_LOAD = "confirm_load.png";
		const string TENG_GRAPHIC_SYSTEM_DIALOG_OVERWRITE = "confirm_overwrite.png";
		const string TENG_GRAPHIC_SYSTEM_DIALOG_DELETE = "confirm_delete.png";
		const string TENG_GRAPHIC_SYSTEM_SELECT_NORMAL = "select_base.png";
		const string TENG_GRAPHIC_SYSTEM_SELECT_FOCUS = "select_base_focus.png";
		const string TENG_GRAPHIC_SYSTEM_SELECT_SELECTED = "select_base_selected.png";
		const string TENG_GRAPHIC_SYSTEM_NAME_ENTRY_BOX = "name_entry_box.png";
		const string TENG_GRAPHIC_SYSTEM_MESSAGE_ARROW = "arrow.png";
		const string TENG_GRAPHIC_BACKGROUND_GALLERY = "gallery_back.png";
		const string TENG_GRAPHIC_SYSTEM_GALLERY_RULE_SLIDE = "kosuri.png";

		//タイトル画面のボタン類
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_NORMAL = "start_0.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_FOCUS = "start_01.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_CLICK = "start_02.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_NORMAL = "load_0.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_FOCUS = "load_01.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_CLICK = "load_02.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_NORMAL = "confg_0.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_FOCUS = "confg_01.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_CLICK = "confg_02.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_NORMAL = "gallery_0.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_FOCUS = "gallery_01.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_CLICK = "gallery_02.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_NORMAL = "exit_0.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_FOCUS = "exit_01.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_CLICK = "exit_02.png";

		//セーブ・ロード画面のボタン類
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_NORMAL = "1_defo.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_FOCUS = "1_select.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_CLICK = "1_on.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_NORMAL = "2_defo.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_FOCUS = "2_select.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_CLICK = "2_on.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_NORMAL = "3_defo.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_FOCUS = "3_select.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_CLICK = "3_on.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_NORMAL = "4_defo.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_FOCUS = "4_select.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_CLICK = "4_on.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_NORMAL = "5_defo.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_FOCUS = "5_select.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_CLICK = "5_on.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_NORMAL = "back_0.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_FOCUS = "back_01.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_CLICK = "back_02.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_ICON_BARA = "bara.png";
		const string TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_ICON_ARROW = "yazirushi.png";
		const string TENG_GRAPHIC_SYSTEM_RULE_SAVE_CONFIRM_DIALOG = "rule_confirm_dialog.png";
		const string TENG_GRAPHIC_SYSTEM_RULE_SAVE_CONFIRM_DIALOG_REVERSE = "rule_confirm_dialog_rev.png";

		//コンフィグ画面
		const string SYSTEM_BUTTON_CONFIG_BAR_BASE = "base_bar.png";
		const string SYSTEM_BUTTON_CONFIG_MINUS_NORMAL = "-.png";
		const string SYSTEM_BUTTON_CONFIG_MINUS_FOCUS = "-_select.png";
		const string SYSTEM_BUTTON_CONFIG_MINUS_CLICK = "-_on.png";
		const string SYSTEM_BUTTON_CONFIG_PLUS_NORMAL = "+.png";
		const string SYSTEM_BUTTON_CONFIG_PLUS_FOCUS = "+_select.png";
		const string SYSTEM_BUTTON_CONFIG_PLUS_CLICK = "+_on.png";
		const string SYSTEM_BUTTON_CONFIG_SKIP_ALL_NORMAL = "all.png";
		const string SYSTEM_BUTTON_CONFIG_SKIP_ALL_FOCUS = "all_select.png";
		const string SYSTEM_BUTTON_CONFIG_SKIP_ALL_CLICK = "all_on.png";
		const string SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_NORMAL = "already.png";
		const string SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_FOCUS = "already_select.png";
		const string SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_CLICK = "already_on.png";
		const string SYSTEM_BUTTON_CONFIG_BACK_NORMAL = "back_0.png";
		const string SYSTEM_BUTTON_CONFIG_BACK_FOCUS = "back_01.png";
		const string SYSTEM_BUTTON_CONFIG_BACK_CLICK = "back_02.png";
		const string SYSTEM_BUTTON_CONFIG_BAR_NORMAL = "bar.png";
		const string SYSTEM_BUTTON_CONFIG_BAR_FOCUS = "bar_on.png";
		const string SYSTEM_BUTTON_CONFIG_BAR_CLICK = "bar_select.png";

		//メニュー画面
		const string SYSTEM_BUTTON_MENU_SAVE_NORMAL = "save_0.png";
		const string SYSTEM_BUTTON_MENU_LOAD_NORMAL = "load_0.png";
		const string SYSTEM_BUTTON_MENU_HSKIP_NORMAL = "HyperSkip_0.png";
		const string SYSTEM_BUTTON_MENU_CONFIG_NORMAL = "confg_0.png";
		const string SYSTEM_BUTTON_MENU_BACKLOG_NORMAL = "BackLog_0.png";
		const string SYSTEM_BUTTON_MENU_TITLE_NORMAL = "Title_0.png";
		const string SYSTEM_BUTTON_MENU_EXIT_NORMAL = "exit_0.png";
		const string SYSTEM_BUTTON_MENU_BACK_NORMAL = "Back_0.png";
		const string SYSTEM_BUTTON_MENU_SAVE_FOCUS = "save_01.png";
		const string SYSTEM_BUTTON_MENU_LOAD_FOCUS = "load_01.png";
		const string SYSTEM_BUTTON_MENU_HSKIP_FOCUS = "HyperSkip_01.png";
		const string SYSTEM_BUTTON_MENU_CONFIG_FOCUS = "confg_01.png";
		const string SYSTEM_BUTTON_MENU_BACKLOG_FOCUS = "BackLog_01.png";
		const string SYSTEM_BUTTON_MENU_TITLE_FOCUS = "Title_01.png";
		const string SYSTEM_BUTTON_MENU_EXIT_FOCUS = "exit_01.png";
		const string SYSTEM_BUTTON_MENU_BACK_FOCUS = "Back_01.png";
		const string SYSTEM_BUTTON_MENU_SAVE_CLICK = "save_02.png";
		const string SYSTEM_BUTTON_MENU_LOAD_CLICK = "load_02.png";
		const string SYSTEM_BUTTON_MENU_HSKIP_CLICK = "HyperSkip_02.png";
		const string SYSTEM_BUTTON_MENU_CONFIG_CLICK = "confg_02.png";
		const string SYSTEM_BUTTON_MENU_BACKLOG_CLICK = "BackLog_02.png";
		const string SYSTEM_BUTTON_MENU_TITLE_CLICK = "Title_02.png";
		const string SYSTEM_BUTTON_MENU_EXIT_CLICK = "exit_02.png";
		const string SYSTEM_BUTTON_MENU_BACK_CLICK = "Back_02.png";
		const string SYSTEM_MENU_BASE = "menu_base.png";

		//BGM
		const string TENG_SOUND_SE_CONFIG_DEMO = "pi.wav";
		const string TENG_SOUND_SE_SELECT_CLICKED = "select_clicked.wav";
		const string TENG_SOUND_SE_SELECT_FOCUS = "select_focus.wav";
		const string TENG_SOUND_BGM_TITLE = "titleG.ogg";
		const string TENG_SOUND_SYSTEM_BUTTON_TITLE_CLICKED = "title_button_clicked.wav";
		const string TENG_SOUND_SYSTEM_BUTTON_TITLE_FOCUS = "title_button_focus.wav";


		loadImage(SystemImage::bg_title, TENG_GRAPHIC_BACKGROUND_TITLE);
		loadImage(SystemImage::bg_load_1, TENG_GRAPHIC_BACKGROUND_LOAD_1);
		loadImage(SystemImage::bg_save_1, TENG_GRAPHIC_BACKGROUND_SAVE_1);
		loadImage(SystemImage::bg_config, TENG_GRAPHIC_BACKGROUND_CONFIG);
		loadImage(SystemImage::bg_gallery, TENG_GRAPHIC_BACKGROUND_GALLERY);
		loadImage(SystemImage::msgwindow, TENG_GRAPHIC_SYSTEM_GRAPHIC_MSG_WINDOW_FILE);
		//選択肢ボックスの画像ロード
		loadImage(SystemImage::select_normal, TENG_GRAPHIC_SYSTEM_SELECT_NORMAL);
		loadImage(SystemImage::select_focus, TENG_GRAPHIC_SYSTEM_SELECT_FOCUS);
		//選択肢ボックス（既に選択したもの）
		loadImage(SystemImage::select_selected, TENG_GRAPHIC_SYSTEM_SELECT_SELECTED);
		//ネームエントリー画像ロード
		loadImage(SystemImage::name_entry_box, TENG_GRAPHIC_SYSTEM_NAME_ENTRY_BOX);
		//メッセージ待機の矢印
		loadImage(SystemImage::message_arrow, TENG_GRAPHIC_SYSTEM_MESSAGE_ARROW);
		//ギャラリー内部用のトラン画像
		loadImage(SystemImage::gallery_rule_slide, TENG_GRAPHIC_SYSTEM_GALLERY_RULE_SLIDE);
		//タイトル画面ボタン類
		//スタートボタン
		loadImage(SystemImage::button_title_start_normal, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_NORMAL);
		loadImage(SystemImage::button_title_start_focus, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_FOCUS);
		loadImage(SystemImage::button_title_start_click, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_START_CLICK);
		//ロードボタン
		loadImage(SystemImage::button_title_load_normal, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_NORMAL);
		loadImage(SystemImage::button_title_load_focus, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_FOCUS);
		loadImage(SystemImage::button_title_load_click, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_LOAD_CLICK);
		//コンフィグボタン
		loadImage(SystemImage::button_title_config_normal, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_NORMAL);
		loadImage(SystemImage::button_title_config_focus, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_FOCUS);
		loadImage(SystemImage::button_title_config_click, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_CONFIG_CLICK);
		//ギャラリーボタン
		loadImage(SystemImage::button_title_gallery_normal, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_NORMAL);
		loadImage(SystemImage::button_title_gallery_focus, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_FOCUS);
		loadImage(SystemImage::button_title_gallery_click, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_GALLERY_CLICK);
		//終了ボタン
		loadImage(SystemImage::button_title_exit_normal, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_NORMAL);
		loadImage(SystemImage::button_title_exit_focus, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_FOCUS);
		loadImage(SystemImage::button_title_exit_click, TENG_GRAPHIC_SYSTEM_BUTTON_TITLE_EXIT_CLICK);

		//セーブロード画面のボタン類
		const int SAVE_PAGE_BUTTON_X = 75;
		const int SAVE_PAGE_BUTTON_Y_1 = 255;
		const int SAVE_PAGE_BUTTON_Y_2 = 306;
		const int SAVE_PAGE_BUTTON_Y_3 = 362;
		const int SAVE_PAGE_BUTTON_Y_4 = 418;
		const int SAVE_PAGE_BUTTON_Y_5 = 468;
		const int SAVE_BACK_BUTTON_X = 25;
		const int SAVE_BACK_BUTTON_Y = 700;
		const int DIALOG_BOX_X = 300;
		const int DIALOG_BOX_Y = 250;

		loadImage(SystemImage::button_save_page1_normal, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_1);
		loadImage(SystemImage::button_save_page1_focus, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_1);
		loadImage(SystemImage::button_save_page1_click, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_1_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_1);
		loadImage(SystemImage::button_save_page2_normal, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_2);
		loadImage(SystemImage::button_save_page2_focus, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_2);
		loadImage(SystemImage::button_save_page2_click, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_2_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_2);
		loadImage(SystemImage::button_save_page3_normal, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_3);
		loadImage(SystemImage::button_save_page3_focus, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_3);
		loadImage(SystemImage::button_save_page3_click, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_3_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_3);
		loadImage(SystemImage::button_save_page4_normal, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_4);
		loadImage(SystemImage::button_save_page4_focus, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_4);
		loadImage(SystemImage::button_save_page4_click, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_4_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_4);
		loadImage(SystemImage::button_save_page5_normal, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_NORMAL, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_5);
		loadImage(SystemImage::button_save_page5_focus, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_FOCUS, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_5);
		loadImage(SystemImage::button_save_page5_click, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_PAGE_5_CLICK, SAVE_PAGE_BUTTON_X, SAVE_PAGE_BUTTON_Y_5);
		loadImage(SystemImage::button_save_back_normal, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_NORMAL, SAVE_BACK_BUTTON_X, SAVE_BACK_BUTTON_Y);
		loadImage(SystemImage::button_save_back_focus, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_FOCUS, SAVE_BACK_BUTTON_X, SAVE_BACK_BUTTON_Y);
		loadImage(SystemImage::button_save_back_click, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_BACK_CLICK, SAVE_BACK_BUTTON_X, SAVE_BACK_BUTTON_Y);
		loadImage(SystemImage::button_save_icon_bara, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_ICON_BARA);
		loadImage(SystemImage::button_save_icon_arrow, TENG_GRAPHIC_SYSTEM_BUTTON_SAVE_ICON_ARROW);
		// セーブロードの確認ダイアログ表示用ルール
		loadImage(SystemImage::rule_save_confirm_dialog, TENG_GRAPHIC_SYSTEM_RULE_SAVE_CONFIRM_DIALOG);
		loadImage(SystemImage::rule_save_confirm_dialog, TENG_GRAPHIC_SYSTEM_RULE_SAVE_CONFIRM_DIALOG_REVERSE);
		//起動時にロード ダイアログボックス
		loadImage(SystemImage::dialog_close, TENG_GRAPHIC_SYSTEM_DIALOG_CLOSE, DIALOG_BOX_X, DIALOG_BOX_Y);
		loadImage(SystemImage::dialog_delete, TENG_GRAPHIC_SYSTEM_DIALOG_DELETE, DIALOG_BOX_X, DIALOG_BOX_Y);
		loadImage(SystemImage::dialog_overwrite, TENG_GRAPHIC_SYSTEM_DIALOG_OVERWRITE, DIALOG_BOX_X, DIALOG_BOX_Y);
		loadImage(SystemImage::dialog_load, TENG_GRAPHIC_SYSTEM_DIALOG_LOAD, DIALOG_BOX_X, DIALOG_BOX_Y);
		//コンフィグ画面のボタン類
		loadImage(SystemImage::button_config_bar_base, SYSTEM_BUTTON_CONFIG_BAR_BASE);
		loadImage(SystemImage::button_config_minus_normal, SYSTEM_BUTTON_CONFIG_MINUS_NORMAL);
		loadImage(SystemImage::button_config_minus_focus, SYSTEM_BUTTON_CONFIG_MINUS_FOCUS);
		loadImage(SystemImage::button_config_minus_click, SYSTEM_BUTTON_CONFIG_MINUS_CLICK);
		loadImage(SystemImage::button_config_plus_normal, SYSTEM_BUTTON_CONFIG_PLUS_NORMAL);
		loadImage(SystemImage::button_config_plus_focus, SYSTEM_BUTTON_CONFIG_PLUS_FOCUS);
		loadImage(SystemImage::button_config_plus_click, SYSTEM_BUTTON_CONFIG_PLUS_CLICK);
		loadImage(SystemImage::button_config_skip_all_normal, SYSTEM_BUTTON_CONFIG_SKIP_ALL_NORMAL);
		loadImage(SystemImage::button_config_skip_all_focus, SYSTEM_BUTTON_CONFIG_SKIP_ALL_FOCUS);
		loadImage(SystemImage::button_config_skip_all_click, SYSTEM_BUTTON_CONFIG_SKIP_ALL_CLICK);
		loadImage(SystemImage::button_config_skip_already_normal, SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_NORMAL);
		loadImage(SystemImage::button_config_skip_already_focus, SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_FOCUS);
		loadImage(SystemImage::button_config_skip_already_click, SYSTEM_BUTTON_CONFIG_SKIP_ALREADY_CLICK);
		loadImage(SystemImage::button_config_back_normal, SYSTEM_BUTTON_CONFIG_BACK_NORMAL);
		loadImage(SystemImage::button_config_back_focus, SYSTEM_BUTTON_CONFIG_BACK_FOCUS);
		loadImage(SystemImage::button_config_back_click, SYSTEM_BUTTON_CONFIG_BACK_CLICK);
		loadImage(SystemImage::button_config_bar_normal, SYSTEM_BUTTON_CONFIG_BAR_NORMAL);
		loadImage(SystemImage::button_config_bar_focus, SYSTEM_BUTTON_CONFIG_BAR_FOCUS);
		loadImage(SystemImage::button_config_bar_click, SYSTEM_BUTTON_CONFIG_BAR_CLICK);

		loadMenuImage(SystemImage::button_menu_save_normal, SYSTEM_BUTTON_MENU_SAVE_NORMAL);
		loadMenuImage(SystemImage::button_menu_load_normal, SYSTEM_BUTTON_MENU_LOAD_NORMAL);
		loadMenuImage(SystemImage::button_menu_hskip_normal, SYSTEM_BUTTON_MENU_HSKIP_NORMAL);
		loadMenuImage(SystemImage::button_menu_config_normal, SYSTEM_BUTTON_MENU_CONFIG_NORMAL);
		loadMenuImage(SystemImage::button_menu_backlog_normal, SYSTEM_BUTTON_MENU_BACKLOG_NORMAL);
		loadMenuImage(SystemImage::button_menu_title_normal, SYSTEM_BUTTON_MENU_TITLE_NORMAL);
		loadMenuImage(SystemImage::button_menu_exit_normal, SYSTEM_BUTTON_MENU_EXIT_NORMAL);
		loadMenuImage(SystemImage::button_menu_back_normal, SYSTEM_BUTTON_MENU_BACK_NORMAL);
	
		loadMenuImage(SystemImage::button_menu_save_focus, SYSTEM_BUTTON_MENU_SAVE_FOCUS);
		loadMenuImage(SystemImage::button_menu_load_focus, SYSTEM_BUTTON_MENU_LOAD_FOCUS);
		loadMenuImage(SystemImage::button_menu_hskip_focus, SYSTEM_BUTTON_MENU_HSKIP_FOCUS);
		loadMenuImage(SystemImage::button_menu_config_focus, SYSTEM_BUTTON_MENU_CONFIG_FOCUS);
		loadMenuImage(SystemImage::button_menu_backlog_focus, SYSTEM_BUTTON_MENU_BACKLOG_FOCUS);
		loadMenuImage(SystemImage::button_menu_title_focus, SYSTEM_BUTTON_MENU_TITLE_FOCUS);
		loadMenuImage(SystemImage::button_menu_exit_focus, SYSTEM_BUTTON_MENU_EXIT_FOCUS);
		loadMenuImage(SystemImage::button_menu_back_focus, SYSTEM_BUTTON_MENU_BACK_FOCUS);

		loadMenuImage(SystemImage::button_menu_save_click, SYSTEM_BUTTON_MENU_SAVE_CLICK);
		loadMenuImage(SystemImage::button_menu_load_click, SYSTEM_BUTTON_MENU_LOAD_CLICK);
		loadMenuImage(SystemImage::button_menu_hskip_click, SYSTEM_BUTTON_MENU_HSKIP_CLICK);
		loadMenuImage(SystemImage::button_menu_config_click, SYSTEM_BUTTON_MENU_CONFIG_CLICK);
		loadMenuImage(SystemImage::button_menu_backlog_click, SYSTEM_BUTTON_MENU_BACKLOG_CLICK);
		loadMenuImage(SystemImage::button_menu_title_click, SYSTEM_BUTTON_MENU_TITLE_CLICK);
		loadMenuImage(SystemImage::button_menu_exit_click, SYSTEM_BUTTON_MENU_EXIT_CLICK);
		loadMenuImage(SystemImage::button_menu_back_click, SYSTEM_BUTTON_MENU_BACK_CLICK);
		loadMenuImage(SystemImage::menu_base, SYSTEM_MENU_BASE);

		//音声のロード
		loadBgm(SystemSound::bgm_title, TENG_SOUND_BGM_TITLE, 255);
		loadSe(SystemSound::se_config_demo, TENG_SOUND_SE_CONFIG_DEMO, 255);
		//loadSe(SystemSound::se_select_clicked, TENG_SOUND_SE_SELECT_CLICKED, 255);
		//loadSe(SystemSound::se_select_focus, TENG_SOUND_SE_SELECT_FOCUS, 255);
		//loadSe(SystemSound::se_button_title_clicked, TENG_SOUND_SYSTEM_BUTTON_TITLE_CLICKED, 255);
		//loadSe(SystemSound::se_button_title_focus, TENG_SOUND_SYSTEM_BUTTON_TITLE_FOCUS, 255);

		//フォント作成
		const string font_name = "KozMinPro-Bold.otf";
		const int FONT_SIZE_MSG = 22;
		const int FONT_SIZE_RUBI = 10;
		const int FONT_SIZE_SAVE_DATE = 34;
		const int FONT_SIZE_SAVE_MSG_AND_CONFIRM = 26;
		const int FONT_SIZE_SAVE_YESNO = 35;
		loadFont(FontType::font_msg, FONT_SIZE_MSG, font_name, false);
		loadFont(FontType::font_rubi, FONT_SIZE_RUBI, font_name, false);
		loadFont(FontType::font_save_date, FONT_SIZE_SAVE_DATE, font_name, true);
		loadFont(FontType::font_save_msg_and_confirm, FONT_SIZE_SAVE_MSG_AND_CONFIRM, font_name, true);
		loadFont(FontType::font_handle_save_yesno, FONT_SIZE_SAVE_YESNO, font_name, true);

		//#ifdef _DEBUG
		// デバッグ時はロード時間計測
		//const auto start_time = std::chrono::system_clock::now();
		//loadScript();
		//const auto end_time = std::chrono::system_clock::now();
		//const auto time_span = end_time - start_time;
		//auto span = std::chrono::duration_cast<std::chrono::milliseconds>(time_span).count();
		//DEBUG printf("[INFO] Script file load %d[ms] completed.", span);
		//#endif
		//#ifndef _DEBUG
		//loadScript();
		//#endif
	}

	if (DxLib::GetASyncLoadNum() == 0 && fade_type == FadeType::no_fade && frame > 60){
		//DEBUG printf("[INFO] RESOURCES LOAD COMPLETED\n");
		frame = 0;
		fade_type = FadeType::fadeout;
	}


}

void SceneLogo::loadScript(){
	namespace sys = std::tr2::sys;
	const string INSTALL_DIR = Resources::getRegistryString();
	const string DIR_SCRIPT = "script\\";

	string script_dir = INSTALL_DIR + DIR_SCRIPT;
	sys::path p(script_dir);
	
	// TODO 現時点ではすべてスクリプトファイルを検索してロードしているが、
	//      シナリオが確定したら、ファイル名を指定して4並行くらいで非同期ロードしてもよいはず
	sys::recursive_directory_iterator itr(p);
	sys::recursive_directory_iterator last;
	// TODO とりあえず画面が完全にブラックアウトした時点でthread.join()する
	for (; itr != last; ++itr){
		auto fullpath = (*itr);
		if (sys::is_regular_file(fullpath.path())){
			auto lambda = [&, fullpath]{
				string filename = fullpath.path().filename();
				if (filename.substr(filename.length() - 4) == ".asc"){
					log_info("Load script : %s", filename.c_str());
					filename = filename.substr(0, filename.length() - 4);
					resources->commandQueue().push_back(new CommandQueue(resources, filename));
				}
			};
			th_list.push_back(thread(lambda));
		};
	}
}

void SceneLogo::operateDialog(){

}

void SceneLogo::operateSound(){

}

void SceneLogo::execFade(){
	//画面開始時
	if (fade_type == FadeType::fadein){
		int f = static_cast<int>(frame)* 10;
		if (f < 255){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadein){
				fade_type = FadeType::no_fade;
				DxLib::SetDrawBright(255, 255, 255);
				loadScript();
			}
		}
	}
	//ロード完了時
	else if(fade_type == FadeType::fadeout){
		int f = 255 - static_cast<int>(frame)* 10;
		if (f > 0){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadeout){
				// 画面が完全にブラックアウトしたら、シナリオの読込待機/確認する
				for (thread& th : th_list){
					th.join();
				}
				//タイトル画面へ
				fade_type = FadeType::no_fade; //破棄するので別になんでもいい
				frame = 0;
				next_scene = SceneType::title;
			}
		}
	}

}

void SceneLogo::addFrame(int add_count){
	if (frame == ULONG_MAX) frame = 0;
	frame += add_count;
}


void SceneLogo::loadImage(const int option_type, const string& filename, const int x, const int y){
	vector<string> cmd;
	cmd.push_back(Util::convertString(option_type));
	cmd.push_back(filename);
	cmd.push_back(Util::convertString(x));
	cmd.push_back(Util::convertString(y));
	resources->add(ResourceType::system_graphic, cmd);
}

void SceneLogo::loadMenuImage(const int option_type, const string& filename, const int x, const int y){
	static const string SYSTEM_MENU_DIR = "menu\\";
	vector<string> cmd;
	cmd.push_back(Util::convertString(option_type));
	string dir_file = SYSTEM_MENU_DIR + filename;
	cmd.push_back(dir_file);
	cmd.push_back(Util::convertString(x));
	cmd.push_back(Util::convertString(y));
	resources->add(ResourceType::system_graphic, cmd);
}

void SceneLogo::loadBgm(const int option_type, const string& filename, const int volume, const int loop_point){
	vector<string> cmd;
	cmd.push_back(Util::convertString(option_type));
	cmd.push_back(filename);
	cmd.push_back(Util::convertString(volume));
	cmd.push_back(Util::convertString(loop_point));
	resources->add(ResourceType::system_bgm, cmd);
}

void SceneLogo::loadSe(const int option_type, const string& filename, const int volume, const int loop_point){
	vector<string> cmd;
	cmd.push_back(Util::convertString(option_type));
	cmd.push_back(filename);
	cmd.push_back(Util::convertString(volume));
	string loop;
	if (loop_point == -1) {
		loop = "once";
	}
	else{
		loop = Util::convertString(loop_point);
	}
	cmd.push_back(loop);
	resources->add(ResourceType::system_se, cmd);
}

void SceneLogo::loadFont(const int option_type, const int size, const string& font_file, const bool premul){
	const string DEFAULT_FONT_NAME = "KozMinPro-Bold.otf";
	string font_name = font_file.empty() ? DEFAULT_FONT_NAME : font_file;
	vector<string> cmd;
	cmd.push_back(Util::convertString(option_type));
	cmd.push_back(Util::convertString(size));
	cmd.push_back(font_name);
	string s_premul = premul ? "1" : "0";
	cmd.push_back(s_premul);
	resources->add(ResourceType::font, cmd);
}