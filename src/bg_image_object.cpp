#include "bg_image_object.h"

using namespace Teng;

BgImageObject::BgImageObject(vector<string> command){
	// [0] ファイル名
	string fullpath = Resources::PathBgImage() + command[0];
	handle = DxLib::LoadGraph(fullpath.c_str());
	if (!Resources::isFileExist(fullpath)){
		log_error("背景画像の読み込みに失敗しました: %s", command[0].c_str());
		RAISE(ENGINE_GRAPHIC_BACKGROUND_LOADGRAPH_FAILED);
	}
	file_name = command[0];
	log_info("BgImage File loading : %s", file_name.c_str());
}
