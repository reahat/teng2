#include "scene_backlog.h"
#include "DxLib.h"

using namespace Teng;

SceneBacklog::SceneBacklog(Resources* resources, Input* input, Config* config) :
frame(0),
fade_type(FadeType::no_fade),
next_scene(SceneType::no_change),
block_cnt(0),
line_cnt(0),
seeklog_flg(false),
log_line_cnt(0)
{
	this->resources = resources;
	this->input = input;
	this->config = config;
	color_black = DxLib::GetColor(0, 0, 0);
	logs = resources->getLog();
	a_log = logs.logs();
	for (ResourceObject* font : resources->getFonts()){
		if (font->getOptionType() == FontType::font_msg){
			msg_font = font->getHandle();
			break;
		}
	}

	for (ResourceObject* bg : resources->getBgImages()){
		if (bg->isVisible()){
			background = bg->getHandle();
			break;
		}
	}
}


void SceneBacklog::preProcess(){
	if (DxLib::ProcessMessage() != 0) RAISE(ENGINE_ERROR_DEFAULT);
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::ClearDrawScreen();
}

void SceneBacklog::postProcess(){
	DxLib::ScreenFlip();
	addFrame(1);
}

scene_t SceneBacklog::getCurrentState(){
	return SceneType::backlog;
}

void SceneBacklog::checkInput(){
	input->checkKeyInput();

	if (input->mouseRightClicked()){
		next_scene = SceneType::scenario;
	}


}

void SceneBacklog::drawGraphic(){

	DxLib::DrawGraph(0, 0, background, FALSE);

	//画面に透過グレーをかぶせる
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);
	DxLib::DrawBox(0, 0, 1024, 768, color_black, TRUE);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);


	if (block_cnt > 0 || line_cnt > 0){ //ログが無いときのエラー回避
		//表示
		log_line_cnt = 0;

		a_log_it = a_log.end();
		std::advance(a_log_it, block_cnt*(-1));
		log_msgs = *a_log_it;
		msg_it = log_msgs.end();

		if (log_msgs.size() < static_cast<unsigned int>(line_cnt)){
			std::advance(msg_it, -1);
		}
		else{
			std::advance(msg_it, line_cnt*(-1));
		}



		while (a_log_it != a_log.end() && log_line_cnt < 10){
			log_msgs = *a_log_it;
			while (msg_it != log_msgs.end() && log_line_cnt < 10){

				if (!a_log_it->getSpeaker().empty() && msg_it == log_msgs.begin()){
					DxLib::DrawStringToHandle(
						TENG_BACKLOG_POINT_X - 80,
						TENG_BACKLOG_POINT_Y + (TENG_MESSAGE_FONT_SIZE + TENG_BACKLOG_LINE_SPACE) * log_line_cnt,
						a_log_it->getSpeaker().c_str(),
						color_white,
						msg_font);
				}

				DxLib::DrawStringToHandle(
					TENG_BACKLOG_POINT_X,
					TENG_BACKLOG_POINT_Y + (TENG_MESSAGE_FONT_SIZE + TENG_BACKLOG_LINE_SPACE) * log_line_cnt,
					(*msg_it).c_str(),
					color_white,
					msg_font);
				msg_it++;
				log_line_cnt += 1;
			}
			if (log_line_cnt < 10) a_log_it++;
			if (a_log_it == a_log.end()) break;
			log_msgs = *a_log_it;
			if (log_line_cnt == 10) break;
			msg_it = log_msgs.begin();
		}
	}



	////// スクロールバー表示位置の計算 //////
	//ログは全部で何行あるか
	int all_log_lines = 0;//ログ全体の行数
	for (list<string> log_block : a_log){
		for (string line : log_block){
			all_log_lines++;
		}
	}
	//ログ全体の行数から、最下の９行を除いた数
	int lines_for_bar = all_log_lines - 9;
	//ブロック数は先頭から数えたいが、block_cntは下からの数なので逆順にする
	//行数は逆順にしなくても問題はない
	list<BacklogMessage> reverse_log_blocks = a_log;
	reverse_log_blocks.reverse();

	//現在は全体の何行目を表示しているか
	int blocks = 0;
	int lines = 0;
	int line_sum = 0;

	bool break_flg = false;
	for (list<string> log_block : reverse_log_blocks){
		blocks++;
		for(string line : log_block){
			lines++;
			line_sum++;
			if (blocks == block_cnt && lines == line_cnt){
				break_flg = true;
				break;
			}
		}
		if (break_flg) break;
		lines = 0;
	}
	line_sum -= 10;
	int now_top_line = lines_for_bar - line_sum;


	//最下画面の１０行を除き、全部でlines_for_bar行ある。
	//現在表示中のトップ行はnow_top_line
	//現在、全体のbar_ratio * 100%の位置にいる

	double bar_ratio = (double)now_top_line / (double)lines_for_bar;
	if (bar_ratio < 0) bar_ratio = 1.0;
	//DEBUG printf("bar-ratio:%f\n", bar_ratio);
	//上ボタンの下部は250(隙間を開けて260ということにする), 下ボタンの上部は630、余裕を持って600。差分は
	int button_diff_y = 600 - 260;
	//ログのカーソル位置が１行目のとき、強制的にバーを最上部にする
	//(割り算の結果bar_ratioは絶対にゼロにならないため誤魔化す）
	if (now_top_line == 1) bar_ratio = 0;
	int bar_point = (int)((double)button_diff_y * bar_ratio) + 260;

	////// スクロールバー表示位置ここまで //////


	if (block_cnt > 0 || line_cnt > 0){

		////// バークリック //////
		if (cursorIn(930, 980, 260, 610)){
			if (input->mouseLeftPushing()){
				int cursor_x, cursor_y;
				DxLib::GetMousePoint(&cursor_x, &cursor_y);
				int click_point = cursor_y;
				//クリック地点は全体の何％の位置にあるか
				double clicked_per = (double)(click_point - 260) / (double)(600 - 260);
				//先頭からprint_top_line行目を先頭行として表示する
				int print_top_line = (int)((double)lines_for_bar * clicked_per);
				//これを後ろから数えた行数に変換
				int print_top_line_reverse = all_log_lines - print_top_line + 1;
				if (print_top_line_reverse < 1) print_top_line_reverse = 1;
				if (print_top_line_reverse > all_log_lines) print_top_line_reverse = all_log_lines;
				int b_block = 0;
				int b_line = 0;
				int line_total = 0;
				for each(list<string> block in reverse_log_blocks){
					bool flag = false;
					b_block++;
					for each(string li in block){
						b_line++;
						line_total++;
						if (line_total == print_top_line_reverse){
							flag = true;
							break;
						}
					}
					if (flag) break;
					b_line = 0;
				}
				block_cnt = b_block;
				line_cnt = b_line;
			}
		}
		////// バークリックここまで //////



		////// スクロール可能箇所を掴んだ場合 //////
		static bool scroll_bar_hold_flg = false;
		//バーを初めて掴んだ
		if (cursorIn(930, 980, bar_point, bar_point + 10) && input->mouseLeftClicked()){
			scroll_bar_hold_flg = true;
		}
		//バーから手を離した
		if (input->mouseLeftPushing() == 0){
			scroll_bar_hold_flg = false;
		}
		//バーを掴んだままの状態
		if (scroll_bar_hold_flg){
			int cursor_x, cursor_y;
			DxLib::GetMousePoint(&cursor_x, &cursor_y);
			bar_point = cursor_y - 5;

			//バーが上に行き過ぎないようにする
			double rat = 1.0f / (double)lines_for_bar;
			//ログのカーソル位置が１行目のとき、強制的にバーを最上部にする
			//(割り算の結果bar_ratioは絶対にゼロにならないため誤魔化す）
			if (now_top_line == 1) rat = 0;
			int pot = (int)((double)button_diff_y * rat) + 260;
			if (bar_point < pot) bar_point = pot;
			//バーが下に行き過ぎないようにする
			if (bar_point > button_diff_y + 260) bar_point = button_diff_y + 260;

			//バーの位置をもとにログの表示位置を再計算
			int click_point = bar_point;
			//クリック地点は全体の何％の位置にあるか
			double clicked_per = (double)(click_point - 260) / (double)(600 - 260);
			//先頭からprint_top_line行目を先頭行として表示する
			int print_top_line = (int)((double)lines_for_bar * clicked_per);
			//これを後ろから数えた行数に変換
			int print_top_line_reverse = all_log_lines - print_top_line + 1;
			if (print_top_line_reverse < 1) print_top_line_reverse = 1;
			if (print_top_line_reverse > all_log_lines) print_top_line_reverse = all_log_lines;
			int b_block = 0;
			int b_line = 0;
			int line_total = 0;
			for each(list<string> block in reverse_log_blocks){
				bool flag = false;
				b_block++;
				for each(string li in block){
					b_line++;
					line_total++;
					if (line_total == print_top_line_reverse){
						flag = true;
						break;
					}
				}
				if (flag) break;
				b_line = 0;
			}
			block_cnt = b_block;
			line_cnt = b_line;


		}
	}

	////// スクロールバー可能箇所を掴んだ場合ここまで //////

	//上ボタン
	DxLib::DrawBox(930, 200, 980, 250, color_white, TRUE);
	//下ボタン
	DxLib::DrawBox(930, 630, 980, 680, color_white, TRUE);
	//スクロールバー
	DxLib::DrawBox(930, bar_point, 970, bar_point + 10, color_white, TRUE);

	if (block_cnt > 0 || line_cnt > 0){
		if (cursorIn(930, 980, 200, 250)){
			if (input->mouseLeftClicked()){

				a_log_it = a_log.end();
				std::advance(a_log_it, block_cnt*(-1));
				log_msgs = *a_log_it;
				msg_it = log_msgs.end();
				std::advance(msg_it, line_cnt*(-1));

				if (msg_it != log_msgs.begin()){
					line_cnt++;
				}
				else{
					if (a_log_it != a_log.begin()){
						block_cnt++;
						log_msgs = *a_log_it;
						msg_it = log_msgs.end();
						line_cnt = 1;
					}
				}
			}
		}
		//下ボタン
		if (cursorIn(930, 980, 630, 680)){
			if (input->mouseLeftClicked()){

				a_log_it = a_log.end();
				std::advance(a_log_it, block_cnt*(-1));
				log_msgs = *a_log_it;
				msg_it = log_msgs.end();
				std::advance(msg_it, line_cnt*(-1));

				if (log_line_cnt == 10){
					int tmp_block_cnt = block_cnt;
					int tmp_line_cnt = line_cnt;
					msg_it++;
					if (msg_it == log_msgs.end()){
						a_log_it++;
						block_cnt--;
						log_msgs = *a_log_it;
						line_cnt = log_msgs.size();
					}
					else{
						line_cnt--;
					}




					//限界までスクロールすると、一行不足するので再計算
					//常に10件は画面に表示するようにする
					int buttoned_block_cnt = block_cnt;
					int buttoned_line_cnt = line_cnt;

					log_line_cnt = 0;
					a_log_it = a_log.end();
					std::advance(a_log_it, block_cnt*(-1));
					log_msgs = *a_log_it;
					msg_it = log_msgs.end();
					std::advance(msg_it, line_cnt*(-1));

					while (a_log_it != a_log.end() && log_line_cnt < 10){
						log_msgs = *a_log_it;
						while (msg_it != log_msgs.end() && log_line_cnt < 10){
							msg_it++;
							log_line_cnt += 1;
						}
						if (log_line_cnt < 10) a_log_it++;
						if (a_log_it == a_log.end()) break;
						log_msgs = *a_log_it;
						if (log_line_cnt == 10) break;
						msg_it = log_msgs.begin();
					}
					if (log_line_cnt == 9){
						block_cnt = tmp_block_cnt;
						line_cnt = tmp_line_cnt;
					}
					else{
						block_cnt = buttoned_block_cnt;
						line_cnt = buttoned_line_cnt;
					}
				}
			}
		}

	}

}

void SceneBacklog::operateButton(){

}

void SceneBacklog::doScene(){
	a_log_it = a_log.end();

	// 10行をカウントしたら打ち止め
	log_line_cnt = 0;
	// バックログ画面初期時
	if (!seeklog_flg){
		block_cnt = 0;
		line_cnt = 0;
		//logs = message->getLog();
		//a_log = logs.logs();
		a_log_it = a_log.end();
		log_line_cnt = 0;
		while (a_log_it != a_log.begin() && log_line_cnt < 10){
			// A-3を指す
			if (log_line_cnt < 10){
				a_log_it--;
				block_cnt++;
			}
			log_msgs = *a_log_it;
			msg_it = log_msgs.end();
			line_cnt = 0;
			while (msg_it != log_msgs.begin() && log_line_cnt < 10){
				log_line_cnt += 1;
				msg_it--;
				line_cnt++;
			}
		}
		seeklog_flg = true;
	}





}

void SceneBacklog::operateDialog(){

}

void SceneBacklog::operateSound(){

}

void SceneBacklog::execFade(){

}

void SceneBacklog::addFrame(int add_count){
	if (frame == ULONG_MAX) frame = 0;
	frame += add_count;
}


bool SceneBacklog::cursorInButton(ResourceObject& resource){
	if (input->cursorX() < resource.getX()) return false;
	if (input->cursorX() >= resource.getX() + resource.getWidth()) return false;
	if (input->cursorY() < resource.getY()) return false;
	if (input->cursorY() >= resource.getY() + resource.getHeight()) return false;
	return true;
}

bool SceneBacklog::cursorIn(const int left, const int right, const int top, const int bottom){
	if (input->cursorX() < left) return false;
	if (input->cursorX() > right) return false;
	if (input->cursorY() < top) return false;
	if (input->cursorY() > bottom) return false;
	return true;
}