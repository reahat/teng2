#pragma once
#include <string>
#include <vector>
#include "common.h"
#include "resource_object.h"
using namespace std;

namespace Teng{
	class BgmObject : public ResourceObject{
	public:
		BgmObject(vector<string> command);
	};

}