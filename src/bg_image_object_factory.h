#pragma once
#include "resource_object_factory.h"
#include "bg_image_object.h"

namespace Teng{
	class BgImageObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}