#include "command_common.h"

using namespace Teng;


CmdFaceClear::~CmdFaceClear(){
	
}

CmdFaceClear::CmdFaceClear(vector<string> text_command, Resources* resources){
	this->resources = resources;
	args = text_command;
	tag = text_command[0];
	//filename = text_command[1];
	force_next = true;
}

void CmdFaceClear::execute(){
	for (auto img : resources->getFaces()){
		img->isVisible(false);
	}
	is_executed = true;
	putLog();
}