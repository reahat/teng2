#pragma once
#include "config.h"
#include <fstream>
#include <iostream>

using namespace Teng;

Teng::Config::Config(){
	auto_msg_flag = false;
	//セーブデータ保存ディレクトリが存在しなければ作る
	ExpandEnvironmentStrings(DIR_CONFIG, savedata_dir, sizeof(savedata_dir));
	if (MakeSureDirectoryPathExists(savedata_dir)) {
		//なにもしない
	}
	else {
		RAISE(ENGINE_SAVE_DIR_MAKE_FAILED);
	}
	load();
}

Teng::Config::~Config(){

}

//設定ファイルの読み込み または作成
int Teng::Config::load(){
	char dir[1024];
	sprintf_s(dir, "%s%s", savedata_dir, CONFIG_FILE_NAME);

	std::ifstream ifs(dir, std::ios::in | std::ios::binary);

	//読み込み失敗時
	if (ifs.fail()){
		//設定ファイル作成
		makeConfig(dir);
		log_info("Config file created.");
		return 1;
	}
	//設定ファイル読み込み成功時
	else{
		ifs.read((char *)&setting, sizeof(ConfigData));
		ifs.close();
		log_info("Config file load succeeded.");
	}

	return 0;
}

//設定ファイルの作成
int Teng::Config::makeConfig(const char *file){
	//設定デフォルト値を適用する
	setting.auto_message_wait = TENG_CONFIG_DEFAULT_AUTO_MESSAGE_WAIT;
	setting.bgm_volume = TENG_CONFIG_DEFAULT_BGM_VOLUME;
	setting.master_volume = TENG_CONFIG_DEFAULT_MASTER_VOLUME;
	setting.message_wait = TENG_CONFIG_DEFAULT_MESSAGE_WAIT;
	setting.force_skip = TENG_CONFIG_DEFAULT_FORCE_SKIP;
	setting.sound_volume = TENG_CONFIG_DEFAULT_SOUND_VOLUME;
	setting.draw_gallery_menu = false;
	for (size_t i = 0; i < 64; i++){
		setting.cg_collected[i] = false;
	}
	//設定ファイルを作成
	std::ofstream ofs;
	ofs.open(file, std::ios::out | std::ios::binary | std::ios::trunc);
	//ファイル作成失敗
	if (!ofs){
		log_error("設定ファイルの作成に失敗しました: %s", file);
		RAISE(ENGINE_CONFIG_FILE_MAKE_FAILURE);
	}
	//設定ファイルの書き込み
	ofs.write((char *)&setting, sizeof(ConfigData));
	if (!ofs.good()){
		log_error("設定ファイルの上書きに失敗しました: %s", file);
		RAISE(ENGINE_CONFIG_FILE_WRITE_FAILURE);
	}
	ofs.close();
	return 0;
}

//ConfigData settingのgetter
Teng::ConfigData Teng::Config::getSetting(){
	return setting;
}


void Teng::Config::setConfig(ConfigType type, const int value, const char* string){
	switch (type){
	case eConfigType_master_volume:
		setting.master_volume = value;
		break;
	case eConfigType_sound_volume:
		setting.sound_volume = value;
		break;
	case eConfigType_bgm_volume:
		setting.bgm_volume = value;
		break;
	case eConfigType_force_skip:
		setting.force_skip = value;
		break;
	case eConfigType_message_wait:
		setting.message_wait = value;
		break;
	case eConfigType_auto_message_wait:
		setting.auto_message_wait = value;
		break;
	case eConfigType_heroine_name:
		strcpy_s(setting.heroine_name, string);
	default:
		break;
	}
}

void Teng::Config::backupConfig(){
	old_setting = setting;
}

void Teng::Config::restoreConfig(){
	setting = old_setting;
}

void Teng::Config::outputConfig(){
	std::ofstream ofs;
	char fullpath[1024];
	sprintf_s(fullpath, "%s%s", savedata_dir, CONFIG_FILE_NAME);
	ofs.open(fullpath, std::ios::out | std::ios::binary | std::ios::trunc);
	//ファイル作成失敗
	if (!ofs){
		log_error("設定ファイルの作成に失敗しました: %s", fullpath);
		throw ENGINE_CONFIG_FILE_MAKE_FAILURE;
	}
	//設定ファイルの書き込み
	ofs.write((char *)&setting, sizeof(ConfigData));
	if (!ofs.good()){
		log_error("設定ファイルの上書きに失敗しました: %s", fullpath);
		throw ENGINE_CONFIG_FILE_WRITE_FAILURE;
	}
	ofs.close();
}

void Teng::Config::setCollectedGraphic(const int gallery_num){
	setting.cg_collected[gallery_num] = true;
	log_info("画像[%d]を表示済にセットしました", gallery_num);
}

void Teng::Config::setDisplayGalleryMenuFlag(){
	setting.draw_gallery_menu = true;
	log_info("ギャラリー表示フラグをONにしました。");
}


int  Teng::Config::calcPlayVolume(const int& master_vol, const int& type_vol, const int& file_vol){
	double master = static_cast<double>(master_vol);
	double type   = static_cast<double>(type_vol);
	double file   = static_cast<double>(file_vol);
	return static_cast<int>(255 * (master / 255) * (type / 255) * (file / 255));
}