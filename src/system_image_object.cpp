#include "system_image_object.h"

using namespace Teng;

SystemImageObject::SystemImageObject(vector<string> command){
	// [0] ディレクトリパス
	// [1] option_type
	// [2] ファイル名
	// [3] x
	// [4] y
	string fullpath = command[0] + command[2];
	handle = DxLib::LoadGraph(fullpath.c_str());
	if (!Resources::isFileExist(fullpath)){
		log_error("[ERROR] システム画像の読み込みに失敗しました: %s", command[2].c_str());
		RAISE(ENGINE_GRAPHIC_SYSTEM_GRAPHIC_LOADGRAPH_FAILED);
	}
	file_name = command[2];
	option_type = stoi(command[1]);
	x = stoi(command[3]);
	y = stoi(command[4]);
	//非同期のため、ここでサイズ取得はできない
	//DxLib::GetGraphSize(handle, &width, &height);
	log_info("Image File loading : %s", file_name.c_str());
}

