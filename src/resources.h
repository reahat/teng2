#pragma once
#include "common.h"
#include "util.hpp"
#include "exception.h"
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <Windows.h>
#include <list>
#include "backlog.h"
#include "resource_object_factory.h"
#include "config.h"
#include "input.h"
//#include "tmp_data.h"
//#include "command_queue.h"
// リソース種別ごとにオブジェクトファクトリを読み込むこと
#include "character_object_factory.h"
#include "system_image_object_factory.h"
#include "bg_image_object_factory.h"
#include "system_bgm_object_factory.h"
#include "bgm_object_factory.h"
#include "font_object_factory.h"
#include "system_se_object_factory.h"
#include "rule_object_factory.h"
#include "face_object_factory.h"

#include "resource_object.h"

using namespace std;

namespace Teng{
	class CommandQueue;
	class TmpData;
	class Resources{
	public:
		Resources(Config* config, Input* input);
		~Resources();
		void add(resource_t type, vector<string> command);
		list<ResourceObject*> getSystemImages(){ return system_images; }
		list<ResourceObject*> getSystemBgms(){ return system_bgms; }
		list<ResourceObject*> getSystemSes(){ return system_ses; }
		list<ResourceObject*> getCharacters(){ return characters; }
		list<ResourceObject*> getBgImages(){ return bg_images; }
		list<ResourceObject*> getRules(){ return rules; }
		list<ResourceObject*> getFonts(){ return fonts; }
		list<ResourceObject*> getFaces(){ return faces; }

		static string InstallDir(){ return install_dir; }
		static string PathSystemImage(){ return path_system_image; }
		static string PathSystemSound(){ return path_system_sound; }
		static string PathSystemSe(){ return path_system_se; }
		static string PathCharacter(){ return path_character; }
		static string PathBgImage(){ return path_bg_image; }
		static string PathRuleImage(){ return path_rule_image; }
		static string PathFont(){ return path_font; }
		static string PathFace(){ return path_face_image; }

		static bool isFileExist(const string& fullpath);
		static string getRegistryString(); // インストール先をレジストリから取得

		Config* getConfig(){ return config; }
		Input* getInput(){ return input; }
		vector<CommandQueue*>& commandQueue(){ return command_queues; }
		Backlog& getLog(){ return log; }
		TmpData* tmpData(){ return tmp; }
		void erase(const ResourceType type, const string& file);
		const int getBgFadeType(){ return bg_fade_type; }
		void      setBgFadeType(const int type){ bg_fade_type = type; }

		ResourceObject* getBgBefore(){ return bg_before; }
		ResourceObject* getBgAfter(){ return bg_after; }
		ResourceObject* getBgRule(){ return bg_rule; }
		void setBgBefore(ResourceObject* bg_before){ this->bg_before = bg_before; }
		void setBgAfter(ResourceObject* bg_after){ this->bg_after = bg_after; }
		void setBgRule(ResourceObject* bg_rule){ this->bg_rule = bg_rule; }
	private:
		string makeFullPath(const string& dir);

		static string install_dir;
		static string path_system_image;
		static string path_system_sound;
		static string path_system_se;
		static string path_character;
		static string path_bg_image;
		static string path_rule_image;
		static string path_font;
		static string path_face_image;

		ResourceObjectFactory* character_factory;
		list<ResourceObject*>  characters;
		ResourceObjectFactory* system_image_factory;
		list<ResourceObject*>  system_images;
		ResourceObjectFactory* system_se_factory;
		list<ResourceObject*>  system_ses;
		ResourceObjectFactory* bg_image_factory;
		list<ResourceObject*>  bg_images;
		ResourceObjectFactory* rule_factory;
		list<ResourceObject*>  rules;
		ResourceObjectFactory* system_bgm_factory;
		list<ResourceObject*>  system_bgms;
		ResourceObjectFactory* bgm_factory;
		list<ResourceObject*>  bgms;
		ResourceObjectFactory* face_factory;
		list<ResourceObject*>  faces;
		ResourceObjectFactory* font_factory;
		list<ResourceObject*>  fonts;

		vector<CommandQueue*> command_queues;
		Backlog log;

		Config* config;
		Input* input;

		TmpData* tmp;
		int bg_fade_type;
		ResourceObject* bg_before;
		ResourceObject* bg_after;
		ResourceObject* bg_rule;
	};





}