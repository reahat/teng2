#include "resources.h"
#include "tmp_data.h"
using namespace Teng;

string Resources::install_dir;
string Resources::path_system_image;
string Resources::path_system_sound;
string Resources::path_system_se;
string Resources::path_character;
string Resources::path_bg_image;
string Resources::path_font;
string Resources::path_rule_image;
string Resources::path_face_image;;

Resources::Resources(Config* config, Input* input){
	const string DIR_SYSTEM_IMAGE = "system_image";
	const string DIR_CHARACTER    = "character_image";
	const string DIR_SYSTEM_SOUND = "system_sound";
	const string DIR_SYSTEM_SE    = "system_sound";
	const string DIR_BG_IMAGE     = "background_image";
	const string DIR_FONT         = "font";
	const string DIR_RULE_IMAGE   = "rule_image";
	const string DIR_FACE_IMAGE   = "face_image";

	Resources::install_dir = getRegistryString();
	path_system_image = makeFullPath(DIR_SYSTEM_IMAGE);
	path_system_sound = makeFullPath(DIR_SYSTEM_SOUND);
	path_system_se    = makeFullPath(DIR_SYSTEM_SE);
	path_character    = makeFullPath(DIR_CHARACTER);
	path_bg_image     = makeFullPath(DIR_BG_IMAGE);
	path_font         = makeFullPath(DIR_FONT);
	path_rule_image   = makeFullPath(DIR_RULE_IMAGE);
	path_face_image   = makeFullPath(DIR_FACE_IMAGE);

	character_factory    = new CharacterObjectFactory();
	system_image_factory = new SystemImageObjectFactory();
	system_bgm_factory   = new SystemBgmObjectFactory();
	system_se_factory    = new SystemSeObjectFactory();
	bgm_factory          = new BgmObjectFactory();
	bg_image_factory     = new BgImageObjectFactory();
	rule_factory         = new RuleObjectFactory();
	font_factory         = new FontObjectFactory();
	face_factory         = new FaceObjectFactory();

	this->config = config;
	this->input = input;
	tmp = new TmpData();
	bg_fade_type = FadeType::no_fade;
}

Resources::~Resources(){
//	delete log;
	delete tmp;
}

void Resources::add(resource_t type, vector<string> command){
	//vector<string> cmd_with_dir;
	switch (type){
	case ResourceType::system_graphic:
		// commandにはディレクトリ名がないため、無理矢理追加する
		// TODO やめましょう
		Util::push_front(command, path_system_image);
		system_images.push_back(system_image_factory->create(command));
		break;
	case ResourceType::back_image:
		bg_images.push_back(bg_image_factory->create(command));
		break;
	case ResourceType::rule:
		rules.push_back(rule_factory->create(command));
		break;
	case ResourceType::character:
		characters.push_back(character_factory->create(command));
		break;
	case ResourceType::face:
		faces.push_back(face_factory->create(command)); 
		break;
	case ResourceType::system_bgm:
		Util::push_front(command, path_system_sound);
		system_bgms.push_back(system_bgm_factory->create(command));
		break;
	case ResourceType::system_se:
		Util::push_front(command, path_system_se);
		system_ses.push_back(system_se_factory->create(command));
		break;
	case ResourceType::font:
		fonts.push_back(font_factory->create(command));
		break;
	default:
		break;
	}
}

string Resources::getRegistryString(){
	HKEY hKey;
	HKEY hParentKey = HKEY_CURRENT_USER;
	LPCSTR lpszSubKey = "Software\\Cherry Creampie\\Ephialtes";
	LPCSTR installDirKey = "InstallDir";
	DWORD dwType = REG_SZ;
	LONG rc;
	char szText[512];
	DWORD dwByte = sizeof(szText) / sizeof(szText[0]);
	rc = RegOpenKeyEx(hParentKey, lpszSubKey, 0, KEY_ALL_ACCESS, &hKey);
	if (rc != ERROR_SUCCESS) RAISE(ENGINE_REGISTRY_OPEN_FAILURE);
	rc = RegQueryValueEx(hKey, installDirKey, NULL, &dwType, (LPBYTE)szText, &dwByte);
	RegCloseKey(hKey);
	if (rc != ERROR_SUCCESS) RAISE(ENGINE_REGISTRY_READ_FAILURE);
	return string(szText);
}

string Resources::makeFullPath(const string& dir){
	return install_dir + dir + "\\";
}

bool Resources::isFileExist(const string& fullpath){
	return PathFileExists(fullpath.c_str()) == TRUE ? true : false;
}

void Resources::erase(const ResourceType type, const string& file){
	if (type == ResourceType::back_image){
		for (auto it = bg_images.begin(); it != bg_images.end(); it++){
			if ((*it)->getFileName() == file){
				DxLib::DeleteGraph((*it)->getHandle());
				bg_images.erase(it);
				break;
			}
		}
	}
	else if (type == ResourceType::rule){
		for (auto it = rules.begin(); it != rules.end(); it++){
			if ((*it)->getFileName() == file){
				DxLib::DeleteGraph((*it)->getHandle());
				rules.erase(it);
				break;
			}
		}
	}
	else if (type == ResourceType::face){
		for (auto it = faces.begin(); it != faces.end(); it++){
			if ((*it)->getFileName() == file){
				DxLib::DeleteGraph((*it)->getHandle());
				faces.erase(it);
				break;
			}
		}
	}

}