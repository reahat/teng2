#include "command_common.h"

using namespace Teng;


CmdBgDraw::~CmdBgDraw(){
	
}

CmdBgDraw::CmdBgDraw(vector<string> text_command, Resources* resources){
	// [0] tag
	// [1] file
	// [2] fadetype
	// [3] speed
	// [4] rulefile
	// [5] rulepower
	this->resources = resources;
	args = text_command;
	tag = text_command[0];
	filename = text_command[1];
	force_next = true;
}

void CmdBgDraw::execute(){
	//vector<string> script;

	for (auto img : resources->getBgImages()){
		if (img->getFileName() == args[1]){
			img->isVisible(true);
		}
		else{
			img->isVisible(false);
		}
	}
	is_executed = true;
	putLog();
}