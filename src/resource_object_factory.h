#pragma once
#include "resource_object.h"
#include <vector>

namespace Teng{
	class ResourceObjectFactory{
	public:
		virtual ResourceObject* create(vector<string> command) = 0;
	};

}