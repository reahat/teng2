#include "scene_config.h"
using namespace Teng;

SceneConfig::SceneConfig(Resources* resources, Input* input, Config* config) : 
frame(0),
fade_type(FadeType::fadein),
next_scene(SceneType::no_change),
back_clicked(false),
mouse_left_clicked(false),
keep_btn(none)
{
	this->resources = resources;
	this->input = input;
	this->config = config;
	
	skip_status = config->getSetting().force_skip;



	list<ResourceObject*> list = resources->getSystemImages();
	for (auto it = list.begin(); it != list.end(); it++){
		// 背景
		switch ((*it)->getOptionType()){
		case SystemImage::bg_config:
			background = (*it)->getHandle();
			break;
		case SystemImage::button_config_skip_all_normal:
			btn_all.normal = btn_all.now = (*it)->getHandle();
			btn_all.x = SKIP_ALL_X;
			btn_all.y = SKIP_Y;
			DxLib::GetGraphSize(btn_all.normal, &btn_all.width, &btn_all.height);
			break;
		case SystemImage::button_config_skip_all_focus:
			btn_all.focus = (*it)->getHandle();
			break;
		case SystemImage::button_config_skip_all_click:
			btn_all.click = (*it)->getHandle();
			break;
		case SystemImage::button_config_skip_already_normal:
			btn_already.normal = btn_already.now = (*it)->getHandle();
			btn_already.x = SKIP_ALREADY_X;
			btn_already.y = SKIP_Y;
			DxLib::GetGraphSize(btn_already.normal, &btn_already.width, &btn_already.height);
			break;
		case SystemImage::button_config_skip_already_focus:
			btn_already.focus = (*it)->getHandle();
			break;
		case SystemImage::button_config_skip_already_click:
			btn_already.click = (*it)->getHandle();
			break;
		case SystemImage::button_config_minus_normal:
			btn_minus1.now = btn_minus1.normal = (*it)->getHandle();
			btn_minus1.x = MINUS_X;
			btn_minus1.y = MINUS_Y1;
			DxLib::GetGraphSize(btn_minus1.normal, &btn_minus1.width, &btn_minus1.height);
			btn_minus2.now = btn_minus2.normal = btn_minus3.now = btn_minus3.normal = btn_minus4.now = btn_minus4.normal = btn_minus5.now = btn_minus5.normal = btn_minus1.normal;
			btn_minus2.x = btn_minus3.x = btn_minus4.x = btn_minus5.x = btn_minus1.x;
			btn_minus2.y = MINUS_Y2;
			btn_minus3.y = MINUS_Y3;
			btn_minus4.y = MINUS_Y4;
			btn_minus5.y = MINUS_Y5;
			btn_minus2.width = btn_minus3.width = btn_minus4.width = btn_minus5.width = btn_minus1.width;
			btn_minus2.height = btn_minus3.height = btn_minus4.height = btn_minus5.height = btn_minus1.height;
			break;
		case SystemImage::button_config_minus_focus:
			btn_minus1.focus = btn_minus2.focus = btn_minus3.focus = btn_minus4.focus = btn_minus5.focus = (*it)->getHandle();
			break;
		case SystemImage::button_config_minus_click:
			btn_minus1.click = btn_minus2.click = btn_minus3.click = btn_minus4.click = btn_minus5.click = (*it)->getHandle();
			break;
		case SystemImage::button_config_bar_base:
			bar_base = (*it)->getHandle();
			int tmp_y;
			DxLib::GetGraphSize(bar_base, &bar_base_width, &tmp_y);
			break;
		case SystemImage::button_config_bar_normal:
			bar1.now = bar1.normal = bar2.normal = bar3.normal = bar4.normal = bar5.normal = (*it)->getHandle();
			bar1.y = BAR_Y1;
			bar2.y = BAR_Y2;
			bar3.y = BAR_Y3;
			bar4.y = BAR_Y4;
			bar5.y = BAR_Y5;
			DxLib::GetGraphSize(bar1.normal, &bar1.width, &bar1.height);
			bar2.width = bar3.width = bar4.width = bar5.width = bar1.width;
			bar2.height = bar3.height = bar4.height = bar5.height = bar1.height;
			break;
		case SystemImage::button_config_bar_focus:
			bar1.focus = bar2.focus = bar3.focus = bar4.focus = bar5.focus = (*it)->getHandle();
			break;
		case SystemImage::button_config_bar_click:
			bar1.click = bar2.click = bar3.click = bar4.click = bar5.click = (*it)->getHandle();
			break;
		case SystemImage::button_config_plus_normal:
			btn_plus1.now = btn_plus1.normal = (*it)->getHandle();
			btn_plus1.x = PLUS_X;
			btn_plus1.y = MINUS_Y1;
			DxLib::GetGraphSize(btn_plus1.normal, &btn_plus1.width, &btn_plus1.height);
			btn_plus2.now = btn_plus2.normal = btn_plus3.now = btn_plus3.normal = btn_plus4.now = btn_plus4.normal = btn_plus5.now = btn_plus5.normal = btn_plus1.normal;
			btn_plus2.x = btn_plus3.x = btn_plus4.x = btn_plus5.x = btn_plus1.x;
			btn_plus2.y = MINUS_Y2;
			btn_plus3.y = MINUS_Y3;
			btn_plus4.y = MINUS_Y4;
			btn_plus5.y = MINUS_Y5;
			btn_plus2.width = btn_plus3.width = btn_plus4.width = btn_plus5.width = btn_plus1.width;
			btn_plus2.height = btn_plus3.height = btn_plus4.height = btn_plus5.height = btn_plus1.height;
			break;
		case SystemImage::button_config_plus_focus:
			btn_plus1.focus = btn_plus2.focus = btn_plus3.focus = btn_plus4.focus = btn_plus5.focus = (*it)->getHandle();
			break;
		case SystemImage::button_config_plus_click:
			btn_plus1.click = btn_plus2.click = btn_plus3.click = btn_plus4.click = btn_plus5.click = (*it)->getHandle();
			break;
		case SystemImage::button_config_back_normal:
			btn_back.now = btn_back.normal = (*it)->getHandle();
			btn_back.x = BACK_X;
			btn_back.y = BACK_Y;
			DxLib::GetGraphSize(btn_back.normal, &btn_back.width, &btn_back.height);
			break;
		case SystemImage::button_config_back_focus:
			btn_back.focus = (*it)->getHandle();
			break;
		case SystemImage::button_config_back_click:
			btn_back.click = (*it)->getHandle();
			break;
		default:
			break;
		}
	}
	bar1.power = config->getSetting().message_wait;
	bar2.power = config->getSetting().auto_message_wait;
	bar3.power = volumeToPower(config->getSetting().master_volume);
	bar4.power = volumeToPower(config->getSetting().bgm_volume);
	bar5.power = volumeToPower(config->getSetting().sound_volume);
	bar1.x = powerToX(bar1.power);
	bar2.x = powerToX(bar2.power);
	bar3.x = powerToX(bar3.power);
	bar4.x = powerToX(bar4.power);
	bar5.x = powerToX(bar5.power);
	tmp_bar3_vol = config->getSetting().master_volume;
	tmp_bar4_vol = config->getSetting().bgm_volume;
	tmp_bar5_vol = config->getSetting().sound_volume;
}

void SceneConfig::preProcess(){
	if (DxLib::ProcessMessage() != 0) RAISE(ENGINE_ERROR_DEFAULT);
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::ClearDrawScreen();
}

void SceneConfig::postProcess(){
	DxLib::ScreenFlip();
	addFrame(1);
}

scene_t SceneConfig::getCurrentState(){
	//TODO シナリオから遷移の場合もある
	return SceneType::title_config;
}

void SceneConfig::checkInput(){
	input->checkKeyInput();
	if (fade_type != FadeType::no_fade){
		input->disableAllKeys();
	}
	mouse_left_clicked = input->mouseLeftClicked();

	enum{
		none, all, already, back,
		minus1, minus2, minus3, minus4, minus5,
		plus1, plus2, plus3, plus4, plus5,
		ebar1, ebar2, ebar3, ebar4, ebar5
	};
	static int before = none;

	// 全て ボタン
	if ((skip_status != sk_all && (cursorIn(btn_all) && mouse_left_clicked) || keep_btn == all)){
		btn_all.now = btn_all.click;
		skip_status = sk_all;
		keep_btn = all;
		btn_already.now = btn_already.normal;
	} else if (skip_status == sk_all || (cursorIn(btn_all) && keep_btn == none)){
		btn_all.now = btn_all.focus;
	}

	// 既読のみ ボタン
	if (skip_status != sk_already && (cursorIn(btn_already) && mouse_left_clicked) || keep_btn == already){
		btn_already.now = btn_already.click;
		keep_btn = already;
		skip_status = sk_already;
		btn_all.now = btn_all.normal;
	} else if (skip_status == sk_already || (cursorIn(btn_already) && keep_btn == none)){
		btn_already.now = btn_already.focus;
	}
	// 設定値反映
	if (before == all && !input->mouseLeftPushing()){
		skip_status = sk_all;
		config->setConfig(ConfigType::eConfigType_force_skip, TRUE);
	}
	else if (before == already && !input->mouseLeftPushing()){
		skip_status = sk_already;
		config->setConfig(ConfigType::eConfigType_force_skip, FALSE);
	}
	// カーソル外かつボタンを掴んでいないとき、画像をノーマルに戻す
	if (!cursorIn(btn_all) && skip_status != sk_all){
		btn_all.now = btn_all.normal;
	}
	if (!cursorIn(btn_already) && skip_status != sk_already){
		btn_already.now = btn_already.normal;
	}

	before = keep_btn;

	// マイナスボタン
	bool btn_clicked_now = false;
	if (cursorIn(btn_minus1) && (btn_clicked_now = mouse_left_clicked) || keep_btn == minus1){
		btn_minus1.now = btn_minus1.click;
		keep_btn = minus1;
		if (btn_clicked_now) clickMinusButton(bar1);
	} else if (cursorIn(btn_minus1) && keep_btn == none){
		btn_minus1.now = btn_minus1.focus;
	}
	else{
		btn_minus1.now = btn_minus1.normal;
	}

	if (cursorIn(btn_minus2) && (btn_clicked_now = mouse_left_clicked) || keep_btn == minus2){
		btn_minus2.now = btn_minus2.click;
		keep_btn = minus2;
		if (btn_clicked_now) clickMinusButton(bar2);
	} else if (cursorIn(btn_minus2) && keep_btn == none){
		btn_minus2.now = btn_minus2.focus;
	}
	else{
		btn_minus2.now = btn_minus2.normal;
	}

	if (cursorIn(btn_minus3) && (btn_clicked_now = mouse_left_clicked) || keep_btn == minus3){
		btn_minus3.now = btn_minus3.click;
		keep_btn = minus3;
		if (btn_clicked_now) clickMinusButton(bar3);
	} else if (cursorIn(btn_minus3) && keep_btn == none){
		btn_minus3.now = btn_minus3.focus;
	}
	else{
		btn_minus3.now = btn_minus3.normal;
	}

	if (cursorIn(btn_minus4) && (btn_clicked_now = mouse_left_clicked) || keep_btn == minus4){
		btn_minus4.now = btn_minus4.click;
		keep_btn = minus4;
		if (btn_clicked_now) clickMinusButton(bar4);
	} else if (cursorIn(btn_minus4) && keep_btn == none){
		btn_minus4.now = btn_minus4.focus;
	}
	else{
		btn_minus4.now = btn_minus4.normal;
	}

	if (cursorIn(btn_minus5) && (btn_clicked_now = mouse_left_clicked) || keep_btn == minus5){
		btn_minus5.now = btn_minus5.click;
		keep_btn = minus5;
		if (btn_clicked_now) clickMinusButton(bar5);
	} else if (cursorIn(btn_minus5) && keep_btn == none){
		btn_minus5.now = btn_minus5.focus;
	}
	else{
		btn_minus5.now = btn_minus5.normal;
	}

	// プラスボタン
	if (cursorIn(btn_plus1) && (btn_clicked_now = mouse_left_clicked) || keep_btn == plus1){
		btn_plus1.now = btn_plus1.click;
		keep_btn = plus1;
		if (btn_clicked_now) clickPlusButton(bar1);
	} else if (cursorIn(btn_plus1) && keep_btn == none){
		btn_plus1.now = btn_plus1.focus;
	}
	else{
		btn_plus1.now = btn_plus1.normal;
	}

	if (cursorIn(btn_plus2) && (btn_clicked_now = mouse_left_clicked) || keep_btn == plus2){
		btn_plus2.now = btn_plus2.click;
		keep_btn = plus2;
		if (btn_clicked_now) clickPlusButton(bar2);
	} else if (cursorIn(btn_plus2) && keep_btn == none){
		btn_plus2.now = btn_plus2.focus;
	}
	else{
		btn_plus2.now = btn_plus2.normal;
	}

	if (cursorIn(btn_plus3) && (btn_clicked_now = mouse_left_clicked) || keep_btn == plus3){
		btn_plus3.now = btn_plus3.click;
		keep_btn = plus3;
		if (btn_clicked_now) clickPlusButton(bar3);
	} else if (cursorIn(btn_plus3) && keep_btn == none){
		btn_plus3.now = btn_plus3.focus;
	}
	else{
		btn_plus3.now = btn_plus3.normal;
	}

	if (cursorIn(btn_plus4) && (btn_clicked_now = mouse_left_clicked) || keep_btn == plus4){
		btn_plus4.now = btn_plus4.click;
		keep_btn = plus4;
		if (btn_clicked_now) clickPlusButton(bar4);
	} else if (cursorIn(btn_plus4) && keep_btn == none){
		btn_plus4.now = btn_plus4.focus;
	}
	else{
		btn_plus4.now = btn_plus4.normal;
	}

	if (cursorIn(btn_plus5) && (btn_clicked_now = mouse_left_clicked) || keep_btn == plus5){
		btn_plus5.now = btn_plus5.click;
		keep_btn = plus5;
		if (btn_clicked_now) clickPlusButton(bar5);
	} else if (cursorIn(btn_plus5) && keep_btn == none){
		btn_plus5.now = btn_plus5.focus;
	}
	else{
		btn_plus5.now = btn_plus5.normal;
	}

	// Backボタン
	if (cursorIn(btn_back) && mouse_left_clicked || back_clicked){
		btn_back.now = btn_back.click;
		if (!back_clicked){
			fade_type = FadeType::fadeout;
			frame = 0;
			back_clicked = true;
			// フェードアウト完了までに出力する
			thread = std::thread(&SceneConfig::writeConfig, this);
		}
	} else if (cursorIn(btn_back)){
		btn_back.now = btn_back.focus;
	}
	else{
		if(!back_clicked) btn_back.now = btn_back.normal;
	}

	// 右クリックで戻る
	if (input->mouseRightClicked()){
		btn_back.now = btn_back.click;
		if (!back_clicked){
			fade_type = FadeType::fadeout;
			frame = 0;
			back_clicked = true;
			// フェードアウト完了までに出力する
			thread = std::thread(&SceneConfig::writeConfig, this);
		}

	}

	// メッセージスピード
	if (cursorIn(bar1) && mouse_left_clicked || keep_btn == ebar1){
		bar1.now = bar1.click;
		keep_btn = ebar1;
		if (mouse_left_clicked){
			bar1x = input->cursorX() - bar1.x;
		}
		bar1.x = input->cursorX() - bar1x;
		if (bar1.x < BAR_BASE_X) bar1.x = BAR_BASE_X;
		if (bar1.x > BAR_BASE_X + bar_base_width - bar1.width)
			bar1.x = BAR_BASE_X + bar_base_width - bar1.width;
	} else if (cursorIn(bar1) && keep_btn == none){
		bar1.now = bar1.focus;
	}
	else{
		bar1.now = bar1.normal;
	}

	// オート速度
	if (cursorIn(bar2) && mouse_left_clicked || keep_btn == ebar2){
		bar2.now = bar2.click;
		keep_btn = ebar2;
		if (mouse_left_clicked){
			bar2x = input->cursorX() - bar2.x;
		}
		bar2.x = input->cursorX() - bar2x;
		if (bar2.x < BAR_BASE_X) bar2.x = BAR_BASE_X;
		if (bar2.x > BAR_BASE_X + bar_base_width - bar2.width)
			bar2.x = BAR_BASE_X + bar_base_width - bar2.width;
	} else if (cursorIn(bar2) && keep_btn == none){
		bar2.now = bar2.focus;
	}
	else{
		bar2.now = bar2.normal;
	}

	// マスターボリューム
	if (cursorIn(bar3) && mouse_left_clicked || keep_btn == ebar3){
		bar3.now = bar3.click;
		keep_btn = ebar3;
		if (mouse_left_clicked){
			bar3x = input->cursorX() - bar3.x;
		}
		bar3.x = input->cursorX() - bar3x;
		//バーを掴んでいる時の音量変化
		bar3.power = XtoPower(bar3.x);

		if (bar3.x < BAR_BASE_X) bar3.x = BAR_BASE_X;
		if (bar3.x > BAR_BASE_X + bar_base_width - bar3.width)
			bar3.x = BAR_BASE_X + bar_base_width - bar3.width;
	} else if (cursorIn(bar3) && keep_btn == none){
		bar3.now = bar3.focus;
	}
	else{
		bar3.now = bar3.normal;
	}

	// BGM音量
	if (cursorIn(bar4) && mouse_left_clicked || keep_btn == ebar4){
		bar4.now = bar4.click;
		keep_btn = ebar4;
		if (mouse_left_clicked){
			bar4x = input->cursorX() - bar4.x;
		}
		bar4.x = input->cursorX() - bar4x;
		//バーを掴んでいる時の音量変化
		bar4.power = XtoPower(bar4.x);
		if (bar4.x < BAR_BASE_X) bar4.x = BAR_BASE_X;
		if (bar4.x > BAR_BASE_X + bar_base_width - bar1.width)
			bar4.x = BAR_BASE_X + bar_base_width - bar1.width;
	} else if (cursorIn(bar4) && keep_btn == none){
		bar4.now = bar4.focus;
	}
	else{
		bar4.now = bar4.normal;
	}
	
	// SE音量
	if (cursorIn(bar5) && mouse_left_clicked || keep_btn == ebar5){
		bar5.now = bar5.click;
		keep_btn = ebar5;
		if (mouse_left_clicked){
			bar5x = input->cursorX() - bar5.x;
			auto ses = resources->getSystemSes();
			for (ResourceObject* se : ses){
				if (se->getOptionType() == SystemSound::se_config_demo){
					DxLib::ChangeVolumeSoundMem(
						calcPlaySeVolume(*se),
						se->getHandle());
					DxLib::PlaySoundMem(se->getHandle(), se->getPlayLoop());
					break;
				}
			}
		}
		bar5.x = input->cursorX() - bar5x;
		//バーを掴んでいる時の音量変化
		bar5.power = XtoPower(bar5.x);
		if (bar5.x < BAR_BASE_X) bar5.x = BAR_BASE_X;
		if (bar5.x > BAR_BASE_X + bar_base_width - bar1.width)
			bar5.x = BAR_BASE_X + bar_base_width - bar1.width;
	} else if (cursorIn(bar5) && keep_btn == none){
		bar5.now = bar5.focus;
	}
	else{
		bar5.now = bar5.normal;
	}

	//クリックを解除したらボタンは通常へ
	if (!input->mouseLeftPushing()){
		//バーは近い固定位置に移動
		if      (keep_btn == ebar1) adjustBarLoc(bar1);
		else if (keep_btn == ebar2) adjustBarLoc(bar2);
		else if (keep_btn == ebar3) adjustBarLoc(bar3);
		else if (keep_btn == ebar4) adjustBarLoc(bar4);
		else if (keep_btn == ebar5) adjustBarLoc(bar5);

		//バー位置をConfigオブジェクトへ反映
		switch (keep_btn){
		case ebar1:
			adjustBarLoc(bar1);
			config->setConfig(ConfigType::eConfigType_message_wait, bar1.power);
			break;
		case ebar2:
			adjustBarLoc(bar2);
			config->setConfig(ConfigType::eConfigType_auto_message_wait, bar2.power);
			break;
		case ebar3:
			adjustBarLoc(bar3);
			config->setConfig(ConfigType::eConfigType_master_volume, powerToVolume(bar3.power));
			break;
		case ebar4:
			adjustBarLoc(bar4);
			config->setConfig(ConfigType::eConfigType_bgm_volume, powerToVolume(bar4.power));
			break;
		case ebar5:
			adjustBarLoc(bar5);
			config->setConfig(ConfigType::eConfigType_sound_volume, powerToVolume(bar5.power));
			break;
		default:
			break;
		}

		keep_btn = none;
	}
}

int SceneConfig::volumeToPower(const int& vol){
	int pow;
	if      (vol < VOL_1)  pow = 0;
	else if (vol < VOL_2)  pow = 1;
	else if (vol < VOL_3)  pow = 2;
	else if (vol < VOL_4) pow = 3;
	else if (vol < VOL_5) pow = 4;
	else if (vol < VOL_6) pow = 5;
	else if (vol < VOL_7) pow = 6;
	else if (vol < VOL_8) pow = 7;
	else if (vol < VOL_9) pow = 8;
	else                  pow = 9;
	return pow;
}

int SceneConfig::powerToVolume(const int& pow){
	int vol;
	switch (pow){
	case 0:  vol = VOL_0; break;
	case 1:  vol = VOL_1; break;
	case 2:  vol = VOL_2; break;
	case 3:  vol = VOL_3; break;
	case 4:  vol = VOL_4; break;
	case 5:  vol = VOL_5; break;
	case 6:  vol = VOL_6; break;
	case 7:  vol = VOL_7; break;
	case 8:  vol = VOL_8; break;
	case 9:  vol = VOL_9; break;
	default: vol = VOL_9; break;
	}
	return vol;
}

int SceneConfig::XtoPower(const int& x){
	// バーを右に移動できる限界のバー左端X座標
	int right_limit = BAR_BASE_X + bar_base_width - bar1.width;
	double scale_one = (right_limit - BAR_BASE_X) / 9;
	double scale_half = scale_one / 2;
	int power;

	if      (x < BAR_BASE_X)	                          power = 0;
	else if (x < BAR_BASE_X + scale_half)                 power = 0;
	else if (x < BAR_BASE_X + scale_one * 1 + scale_half) power = 1;
	else if (x < BAR_BASE_X + scale_one * 2 + scale_half) power = 2;
	else if (x < BAR_BASE_X + scale_one * 3 + scale_half) power = 3;
	else if (x < BAR_BASE_X + scale_one * 4 + scale_half) power = 4;
	else if (x < BAR_BASE_X + scale_one * 5 + scale_half) power = 5;
	else if (x < BAR_BASE_X + scale_one * 6 + scale_half) power = 6;
	else if (x < BAR_BASE_X + scale_one * 7 + scale_half) power = 7;
	else if (x < BAR_BASE_X + scale_one * 8 + scale_half) power = 8;
	else    power = 9;
	return power;
}

void SceneConfig::adjustBarLoc(Btn& btn){
	// バーを右に移動できる限界のバー左端X座標
	int right_limit = BAR_BASE_X + bar_base_width - bar1.width;
	double scale_one = (right_limit - BAR_BASE_X) / 9;
	double scale_half = scale_one / 2;

	if (btn.x < BAR_BASE_X){
		btn.x = BAR_BASE_X;
		btn.power = 0;
	}
	else if (btn.x < BAR_BASE_X + scale_half){
		btn.x = BAR_BASE_X;
		btn.power = 0;
	}
	else if (btn.x < BAR_BASE_X + scale_one * 1 + scale_half){
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 1);
		btn.power = 1;
	}
	else if (btn.x < BAR_BASE_X + scale_one * 2 + scale_half){
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 2);
		btn.power = 2;
	}
	else if (btn.x < BAR_BASE_X + scale_one * 3 + scale_half){
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 3);
		btn.power = 3;
	}
	else if (btn.x < BAR_BASE_X + scale_one * 4 + scale_half){
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 4);
		btn.power = 4;
	}
	else if (btn.x < BAR_BASE_X + scale_one * 5 + scale_half){
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 5);
		btn.power = 5;
	}
	else if (btn.x < BAR_BASE_X + scale_one * 6 + scale_half){
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 6);
		btn.power = 6;
	}
	else if (btn.x < BAR_BASE_X + scale_one * 7 + scale_half){
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 7);
		btn.power = 7;
	}
	else if (btn.x < BAR_BASE_X + scale_one * 8 + scale_half){
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 8);
		btn.power = 8;
	}
	else{
		btn.x = static_cast<int>(BAR_BASE_X + scale_one * 9);
		btn.power = 9;
	}
}

int SceneConfig::powerToX(const int& pow){
	int right_limit = BAR_BASE_X + bar_base_width - bar1.width;
	double scale_one = (right_limit - BAR_BASE_X) / 9;
	return static_cast<int>(BAR_BASE_X + scale_one * pow);
}

void SceneConfig::clickMinusButton(Btn& btn){
	if (btn.power > 0){
		btn.power--;
		btn.x = powerToX(btn.power);
	}
}

void SceneConfig::clickPlusButton(Btn& btn){
	if (btn.power < 9){
		btn.power++;
		btn.x = powerToX(btn.power);
	}
}



bool SceneConfig::cursorIn(const Btn& button)const{
	if (input->cursorX() < button.x) return false;
	if (input->cursorX() >= button.x + button.width) return false;
	if (input->cursorY() < button.y) return false;
	if (input->cursorY() >= button.y + button.height) return false;
	return true;
}

void SceneConfig::drawGraphic(){
	DrawGraph(0, 0, background, FALSE);
	DrawGraph(BAR_BASE_X, BAR_BASE_Y1, bar_base, TRUE);
	DrawGraph(BAR_BASE_X, BAR_BASE_Y2, bar_base, TRUE);
	DrawGraph(BAR_BASE_X, BAR_BASE_Y3, bar_base, TRUE);
	DrawGraph(BAR_BASE_X, BAR_BASE_Y4, bar_base, TRUE);
	DrawGraph(BAR_BASE_X, BAR_BASE_Y5, bar_base, TRUE);
	DrawGraph(btn_all.x, btn_all.y, btn_all.now, TRUE);
	DrawGraph(btn_already.x, btn_already.y, btn_already.now, TRUE);
	DrawGraph(btn_minus1.x, btn_minus1.y, btn_minus1.now, TRUE);
	DrawGraph(btn_minus2.x, btn_minus2.y, btn_minus2.now, TRUE);
	DrawGraph(btn_minus3.x, btn_minus3.y, btn_minus3.now, TRUE);
	DrawGraph(btn_minus4.x, btn_minus4.y, btn_minus4.now, TRUE);
	DrawGraph(btn_minus5.x, btn_minus5.y, btn_minus5.now, TRUE);
	DrawGraph(btn_plus1.x, btn_plus1.y, btn_plus1.now, TRUE);
	DrawGraph(btn_plus2.x, btn_plus2.y, btn_plus2.now, TRUE);
	DrawGraph(btn_plus3.x, btn_plus3.y, btn_plus3.now, TRUE);
	DrawGraph(btn_plus4.x, btn_plus4.y, btn_plus4.now, TRUE);
	DrawGraph(btn_plus5.x, btn_plus5.y, btn_plus5.now, TRUE);
	DrawGraph(btn_back.x, btn_back.y, btn_back.now, TRUE);
	DrawGraph(bar1.x, bar1.y, bar1.now, TRUE);
	DrawGraph(bar2.x, bar2.y, bar2.now, TRUE);
	DrawGraph(bar3.x, bar3.y, bar3.now, TRUE);
	DrawGraph(bar4.x, bar4.y, bar4.now, TRUE);
	DrawGraph(bar5.x, bar5.y, bar5.now, TRUE);
}

void SceneConfig::operateButton(){

}

void SceneConfig::doScene(){

	checkVolume();


}

void SceneConfig::operateDialog(){

}

void SceneConfig::operateSound(){

}

void SceneConfig::execFade(){
	//画面開始時
	if (fade_type == FadeType::fadein){
		int f = static_cast<int>(frame)* 10;
		if (f < 255){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadein){
				fade_type = FadeType::no_fade;
				DxLib::SetDrawBright(255, 255, 255);
			}
		}
	}
	//タイトルに戻るとき
	else if(fade_type == FadeType::fadeout){
		int f = 255 - static_cast<int>(frame)* 10;
		if (f > 0){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (fade_type == FadeType::fadeout){
				thread.join();
				//タイトル画面へ
				fade_type = FadeType::no_fade; //破棄するので別になんでもいい
				frame = 0;
				next_scene = SceneType::title;
			}
		}
	}

}

void SceneConfig::addFrame(int add_count){
	if (frame == ULONG_MAX) frame = 0;
	frame += add_count;
}

void SceneConfig::writeConfig(){
	config->outputConfig();
}

void SceneConfig::checkVolume(){

	enum{
		none, all, already, back,
		minus1, minus2, minus3, minus4, minus5,
		plus1, plus2, plus3, plus4, plus5,
		ebar1, ebar2, ebar3, ebar4, ebar5
	};

	static int before_msg_pow = bar1.power;
	static int before_auto_msg_pow = bar2.power;
	static int before_master_pow = bar3.power;
	static int before_bgm_pow = bar4.power;
	static int before_se_pow = bar5.power;

	if (bar1.power != before_msg_pow){
		config->setConfig(ConfigType::eConfigType_message_wait, bar1.power);
	}
	if (bar2.power != before_auto_msg_pow){
		config->setConfig(ConfigType::eConfigType_auto_message_wait, bar2.power);
	}
	if (bar3.power != before_master_pow){
		config->setConfig(ConfigType::eConfigType_master_volume, powerToVolume(bar3.power));
		auto bgms = resources->getSystemBgms();
		for (ResourceObject* bgm : bgms){
			if (bgm->isPlaying()){
				DxLib::ChangeVolumeSoundMem(
					calcPlayBgmVolume(*bgm),
					bgm->getHandle());
			}
		}
	}
	if (bar4.power != before_bgm_pow){
		config->setConfig(ConfigType::eConfigType_bgm_volume, powerToVolume(bar4.power));
		auto bgms = resources->getSystemBgms();
		for (ResourceObject* bgm : bgms){
			if (bgm->isPlaying()){
				DxLib::ChangeVolumeSoundMem(
					calcPlayBgmVolume(*bgm),
					bgm->getHandle());
			}
		}
	}
	if (bar5.power != before_se_pow || ((keep_btn == ebar5 || keep_btn == plus5) && mouse_left_clicked && bar5.power == 9)){
		config->setConfig(ConfigType::eConfigType_sound_volume, powerToVolume(bar5.power));
		auto ses = resources->getSystemSes();
		for (ResourceObject* se : ses){
			if (se->getOptionType() == SystemSound::se_config_demo){
				DxLib::ChangeVolumeSoundMem(
					calcPlaySeVolume(*se),
					se->getHandle());
				DxLib::PlaySoundMem(se->getHandle(), se->getPlayLoop());
				break;
			}
		}
	}



	

	before_master_pow = bar3.power;
	before_bgm_pow = bar4.power;
	before_se_pow = bar5.power;

}

int SceneConfig::calcPlayBgmVolume(ResourceObject& bgm){
	return config->calcPlayVolume(
		config->getSetting().master_volume,
		config->getSetting().bgm_volume,
		bgm.getVolume()
		);
}

int SceneConfig::calcPlaySeVolume(ResourceObject& se){
	return config->calcPlayVolume(
		config->getSetting().master_volume,
		config->getSetting().sound_volume,
		se.getVolume()
		);
}