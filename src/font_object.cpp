#include "font_object.h"

using namespace Teng;

FontObject::FontObject(vector<string> command){
	// [0] font_type
	// [1] size
	// [2] font_name
	// [3] is_premul
	DxLib::SetUseASyncLoadFlag(FALSE);
	const char* DEFAULT_FONT_NAME = "KozMinPro-Bold";

	if (command[3] == "1") DxLib::SetFontCacheUsePremulAlphaFlag(TRUE);

	string fullpath = Resources::PathFont() + command[2];

	// ファイルのサイズを得る
	int fontFileSize = static_cast<int>(DxLib::FileRead_size(fullpath.c_str()));
	// フォントファイルを開く
	int fontFileHandle = DxLib::FileRead_open(fullpath.c_str());
	// フォントデータ格納用のメモリ領域を確保
	void *buffer = malloc(fontFileSize);
	// フォントファイルを丸ごとメモリに読み込む
	DxLib::FileRead_read(buffer, fontFileSize, fontFileHandle);

	// AddFontMemResourceEx引数用
	DWORD font_num = 0;

	if (AddFontMemResourceEx(buffer, fontFileSize, NULL, &font_num) > 0) {
	}
	else{
		// フォント読込エラー処理
		RemoveFontMemResourceEx(buffer);
		free(buffer);
		RAISE(ENGINE_RESOURCE_FONT_FILE_LOAD_FAILURE);
	}

	DxLib::SetFontCacheUsePremulAlphaFlag(FALSE);
	DxLib::SetUseASyncLoadFlag(TRUE);

	option_type = stoi(command[0]);
	handle = CreateFontToHandle(
		DEFAULT_FONT_NAME,
		stoi(command[1]),
		-1,
		DX_FONTTYPE_ANTIALIASING);

	log_info("Font data created : %s, size = %s, %d bytes", DEFAULT_FONT_NAME, command[1].c_str(), fontFileSize);
}
