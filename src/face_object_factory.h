#pragma once
#include "resource_object_factory.h"
#include "face_object.h"

namespace Teng{
	class FaceObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}