#pragma once
#include "scene_state.h"
#include "command_queue.h"
#include <memory>

namespace Teng{
	class TmpData;
	class SceneScenario : public SceneState{
	public:
		SceneScenario(){};//tmp用
		SceneScenario(Resources* resources, Input* input, Config* config);
		void test();
		void preProcess();
		void postProcess();
		scene_t getCurrentState();

		void checkInput();
		void drawGraphic();
		void operateButton();
		void doScene();
		void operateDialog();
		void operateSound();
		void execFade();
		void addFrame(int add_count);
		scene_t getNextScene(){ return next_scene; }
		shared_ptr<CommandQueue>& getCommandQueue(){ return command_queue; }

		const int getJumpFromLine(){ return jump_from_line; }
		const string getJumpFromScript(){ return jump_from_script; }
		void setCmdList(vector<Command*>& cmd_list){ this->cmd_list = cmd_list; }
		void setItNum(const int& it_num){ this->it_num = it_num; }
	private:
		class Button{
		public:
			Button(){};
			handle now;
			handle normal;
			handle focus;
			handle click;
			int    width;
			int    height;
		};
		Button btn_save;
		Button btn_load;
		Button bgn_auto;
		Button btn_qsave;
		Button btn_qload;
		bool executable;
		Command* tmp_cmd;

		handle msg_window;

		Resources* resources;
		Input* input;
		Config* config;
		ULONG frame;
		scene_t next_scene;

		int screen_fade_type;
		bool display_msg_window;
		Command* message_cmd;
		int msg_draw_result;

		//@jumpが使用されたスクリプトファイル名・行番号
		string jump_from_script;
		int    jump_from_line;

		int execJump();
		shared_ptr<CommandQueue> command_queue;

		vector<Command*> cmd_list;
		int it_num;
		string now_script;
		//フェード中にクリックで強制完了
		void fadeForceComplete(const ResourceType type);
	};

}