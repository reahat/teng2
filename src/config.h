#pragma once
#include <string>
#include "common.h"
#include "exception.h"
#include <imagehlp.h>
#pragma comment(lib, "imagehlp.lib")

using namespace std;

namespace Teng{
	class Scene;
	struct ConfigData{
		int master_volume;
		int sound_volume;
		int bgm_volume;
		int force_skip; //未読文章もスキップ
		int message_wait;
		int auto_message_wait;
		char heroine_name[15]; //全角7文字まで
		bool draw_gallery_menu;
		bool cg_collected[64];
	};

	enum ConfigType{
		eConfigType_master_volume,
		eConfigType_sound_volume,
		eConfigType_bgm_volume,
		eConfigType_force_skip,
		eConfigType_message_wait,
		eConfigType_auto_message_wait,
		eConfigType_common_cg_recollection,
		eConfigType_heroine_name
	};

	class Config
	{
	public:
		Config();
		~Config();
		int load(); //設定ファイルの読み込み
		int makeConfig(const char *file); //設定ファイルの作成
		ConfigData getSetting();
		void setConfig(ConfigType type, const int value, const char* string = "");
		void backupConfig();
		void restoreConfig();
		void outputConfig();
		void setAutoMsgFlag(bool auto_flag){ auto_msg_flag = auto_flag; }
		bool getAutoMsgFlag(){ return auto_msg_flag; }
		void setDisplayGalleryMenuFlag();

		void setCollectedGraphic(const int gallery_num);
		int  calcPlayVolume(const int& master_vol, const int& type_vol, const int& file_vol);

	private:
		const char* DIR_CONFIG = "%APPDATA%\\Cherry Creampie\\Ephialtes\\data\\";
		const char* CONFIG_FILE_NAME = "config.cf";
		const int TENG_CONFIG_DEFAULT_AUTO_MESSAGE_WAIT = 5;
		const int TENG_CONFIG_DEFAULT_BGM_VOLUME = 200;
		const int TENG_CONFIG_DEFAULT_MASTER_VOLUME = 210;
		const int TENG_CONFIG_DEFAULT_MESSAGE_WAIT = 5;
		const int TENG_CONFIG_DEFAULT_FORCE_SKIP = 0;
		const int TENG_CONFIG_DEFAULT_SOUND_VOLUME = 255;

		//コンフィグ設定
		ConfigData setting;
		ConfigData old_setting;
		int id;
		//オートモード状態
		bool auto_msg_flag;
		char savedata_dir[512];

	};
}