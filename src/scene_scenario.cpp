#include "scene_scenario.h"
#include "tmp_data.h"
#include "command.h"
#include "common.h"
using namespace Teng;

SceneScenario::SceneScenario(Resources* resources, Input* input, Config* config) :
frame(0),
display_msg_window(true),
executable(true),
msg_draw_result(MsgDrawState::msg_drawing),
jump_from_line(-1),
jump_from_script("")
{
	const string DEFAULT_SCRIPT_FILE = "scenario1.asc";
	now_script = DEFAULT_SCRIPT_FILE;
	this->resources = resources;
	this->input = input;
	this->config = config;

	for (auto img : resources->getSystemImages()){
		switch (img->getOptionType()){
		case SystemImage::msgwindow:
			msg_window = img->getHandle();
			break;
		default:
			break;
		}
	}

	for (CommandQueue* queue : resources->commandQueue()){
		//ロード直後のCommandQueueにはscene_scenarioがセットされていないので
		queue->setSceneScenario(this);
	}

	if (resources->tmpData()->tmpScenarioExist()){
		for (CommandQueue* queue : resources->commandQueue()){
			if (queue->getScriptFilename() == resources->tmpData()->tmpScriptFileName()){
				screen_fade_type = FadeType::no_fade;
				//executable = false;
				resources->tmpData()->tmpScenarioExist(false);
				command_queue.reset(queue);
				break;
			}
		}
	}
	else{
		for (CommandQueue* queue : resources->commandQueue()){
			if (queue->getScriptFilename() == DEFAULT_SCRIPT_FILE){
				command_queue.reset(queue);
				screen_fade_type = FadeType::fadein;
			}
		}
	}


	DxLib::DeleteGraph(resources->tmpData()->tmpScreenshot());

}

void SceneScenario::preProcess(){
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::ClearDrawScreen();
}

void SceneScenario::postProcess(){
	DxLib::ScreenFlip();
	addFrame(1);
}

scene_t SceneScenario::getCurrentState(){
	return SceneType::scenario;
}

void SceneScenario::checkInput(){
	input->checkKeyInput();
	bool mouse_left_clicked = input->mouseLeftClicked();
	//表示途中にクリックで高速表示
	if (msg_draw_result == MsgDrawState::msg_drawing && mouse_left_clicked){
		message_cmd->setInput(1);
	}

	if (msg_draw_result == MsgDrawState::msg_plus_stopping && mouse_left_clicked){
		message_cmd->setInput(2);
	}

	if (msg_draw_result == MsgDrawState::msg_complete && mouse_left_clicked){
		executable = true;
	}
	//背景のフェード中にクリック
	if (resources->getBgFadeType() != FadeType::no_fade && mouse_left_clicked){
		fadeForceComplete(ResourceType::back_image);
		executable = true;
	}

	if (input->mouseWheelUpped()){
		resources->tmpData()->tmpScriptFileName(command_queue->getScriptFilename());
		resources->tmpData()->tmpScenarioExist(true);
		next_scene = SceneType::backlog;

		for (auto it = cmd_list.begin(); it != cmd_list.end(); it++){
			if (!(*it)->isExecuted()){
				it--;
				(*it)->isExecuted(false);
				//message_cmd->
				break;
			}
		}

	}


}

void SceneScenario::doScene(){


	//コマンド実行
	//TODO キューの中身が多いとループ回数が多すぎるので要改善
	cmd_list = command_queue->get();
	for (it_num = 0; it_num < static_cast<int>(cmd_list.size()); it_num++){

		//キューから未実行のコマンドを実行
		if (!cmd_list[it_num]->isExecuted() && executable){
			//実行するタグ
			int exec_tag = command_queue->tagToType(cmd_list[it_num]->getArgs()[0]);
			cmd_list[it_num]->beforeExecute(this);
			cmd_list[it_num]->execute();

			//executeで実行することができないもの
			//TODO goto/jumpをタグ側のexecute()で実行させる
			//scene_scenarioのコピーをコマンドに持たせればよい(@return参照)
			if (exec_tag == CmdTag::TENG_TAG_GOTO){
				cmd_list[it_num]->send(command_queue->get(), it_num);
			}
			else if (exec_tag == CmdTag::TENG_TAG_JUMP){
				jump_from_script = command_queue->getScriptFilename();
				jump_from_line = it_num;
				int result = execJump();
				log_info("Jump from %s # %d", jump_from_script.c_str(), jump_from_line);
				if (result == 2) continue; // ラベルを指定しない場合
			}
			else if (exec_tag == CmdTag::TENG_TAG_RETURN){
				continue;
			}
			else if (exec_tag == CmdTag::TENG_TAG_FREEBGIMAGE){

			}

			//連続実行可能なコマンドの場合、次のコマンドを実行
			executable = cmd_list[it_num]->isForceNext();

			if (cmd_list[it_num]->getArgs()[0] == "@msg" || cmd_list[it_num]->getArgs()[0] == "@say"){
				message_cmd = cmd_list[it_num];
			}

		}
		
	}



}

int SceneScenario::execJump(){
	// ジャンプ先のファイル名だけ指定した場合
	// @jump scn-001
	if (cmd_list[it_num]->getArgs().size() == 2){
		for (CommandQueue* queue : resources->commandQueue()){
			if (cmd_list[it_num]->getArgs()[1] + ".asc" == queue->getScriptFilename()){
				command_queue.reset(queue);
				break;
			}
		}

		//command_queue.reset(new CommandQueue(resources, cmd_list[it_num]->getArgs()[1], this));
		cmd_list = command_queue->get();
		it_num = -1;//次のループでゼロになる
		executable = true;
		//continue;
		return 2;
	}
	// ジャンプ先のファイル名とラベル名を指定した場合
	// @jump scn-001, label
	else if (cmd_list[it_num]->getArgs().size() == 3){
		string label_to = cmd_list[it_num]->getArgs()[2];
		command_queue.reset(new CommandQueue(resources, cmd_list[it_num]->getArgs()[1], this));
		cmd_list = command_queue->get();
		it_num = 0;
		CmdJump::send(command_queue->get(), label_to, it_num);
	}
	return 3;
}

void SceneScenario::drawGraphic(){
	static const int MSG_WINDOW_X = 0;
	static const int MSG_WINDOW_Y = 500;
	static const int FACE_X = 100;
	static const int FACE_Y = 600;

	//背景描画
	//フェードなし
	if (resources->getBgFadeType() == FadeType::no_fade){
		for (auto bg : resources->getBgImages()){
			if (bg->isVisible()){
				DxLib::DrawGraph(0, 0, bg->getHandle(), TRUE);
				break;
			}
		}
	}
	//ルール有り
	else if (resources->getBgFadeType() == FadeType::use_rule){
		//最初にafterを描画
		ResourceObject* after = resources->getBgAfter();
		ResourceObject* before = resources->getBgBefore();
		ResourceObject* rule = resources->getBgRule();
		DxLib::DrawGraph(0, 0, after->getHandle(), FALSE);
		//beforeとルールを合成して描画
		DxLib::DrawBlendGraph(
			0, 0, 
			before->getHandle(),
			FALSE,
			rule->getHandle(),
			255 - before->getVolume(),
			before->getBlendThreshold());
	}
	//クロスフェード
	else if (resources->getBgFadeType() == FadeType::crossfade){
		//先にafterを描画
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		DxLib::DrawGraph(0, 0, resources->getBgAfter()->getHandle(), FALSE);
		//beforeを半透明で描画
		ResourceObject* before = resources->getBgBefore();
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, before->getVolume());
		DxLib::DrawGraph(0, 0, before->getHandle(), FALSE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
	

	//メッセージウィンドウ描画
	if (display_msg_window){
		DxLib::DrawGraph(MSG_WINDOW_X, MSG_WINDOW_Y, msg_window, TRUE);
	}

	//フェイス描画
	for (auto face : resources->getFaces()){
		if (face->isVisible()){
			DxLib::DrawGraph(FACE_X, FACE_Y, face->getHandle(), TRUE);
			break;
		}
	}

	//メッセージ描画
	msg_draw_result = message_cmd->draw();



	
	//DrawGraphicの後でないとダメ
	//右クリックでメニューへ
	if (input->mouseRightClicked()){
		DxLib::SetUseASyncLoadFlag(FALSE);
		//スクリーンショットを作成して、メニュー画面の背景にする
		int tmp_handle = DxLib::MakeGraph(1024, 768);
		DxLib::GetDrawScreenGraph(0, 0, 1024, 768, tmp_handle);
		resources->tmpData()->tmpScreenshot(tmp_handle);
		next_scene = SceneType::menu;
		DxLib::SetUseASyncLoadFlag(TRUE);
		//シナリオに戻った時の復元用
		resources->tmpData()->tmpScriptFileName(command_queue->getScriptFilename());
		resources->tmpData()->tmpScenarioExist(true);

		for (auto it = cmd_list.begin(); it != cmd_list.end(); it++){
			if (!(*it)->isExecuted()){
				it--;
				(*it)->isExecuted(false);
				break;
			}
		}

	}

}

void SceneScenario::operateButton(){

}

void SceneScenario::operateDialog(){

}

void SceneScenario::operateSound(){
	
}

void SceneScenario::execFade(){

	if (resources->getBgFadeType() == FadeType::use_rule || resources->getBgFadeType() == FadeType::crossfade){
		ResourceObject* before = resources->getBgBefore();
		ResourceObject* after = resources->getBgAfter();
		ResourceObject* rule = resources->getBgRule();

		// powerの分だけ表示を薄くする
		before->setVolume(before->getVolume() - before->getTransitionSpeed());
		// フェード完了したら
		if (before->getVolume() <= 0){
			before->setFadeType(FadeType::no_fade);
			before->isVisible(false);
			after->setFadeType(FadeType::no_fade);
			after->setVolume(255);
			//rule->isVisible(false);
			resources->setBgFadeType(FadeType::no_fade);
			executable = true;
		}

	}


	//画面開始時
	if (screen_fade_type == FadeType::fadein){
		int f = static_cast<int>(frame)* 10;
		if (f < 255){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (screen_fade_type == FadeType::fadein){
				screen_fade_type = FadeType::no_fade;
				DxLib::SetDrawBright(255, 255, 255);
			}
		}
	}
	//次の画面への遷移時
	else if(screen_fade_type == FadeType::fadeout){
		int f = 255 - static_cast<int>(frame)* 10;
		if (f > 0){
			DxLib::SetDrawBright(f, f, f);
		}
		else{
			if (screen_fade_type == FadeType::fadeout){
			}
		}
	}
}

void SceneScenario::addFrame(int add_count){
	if (frame == ULONG_MAX) frame = 0;
	frame += add_count;
}

void SceneScenario::fadeForceComplete(const ResourceType type){
	switch (type){
	case ResourceType::back_image:
	{
		ResourceObject* before = resources->getBgBefore();
		ResourceObject* after = resources->getBgAfter();
		before->setVolume(0);
		before->setFadeType(FadeType::no_fade);
		before->isVisible(false);
		after->setVolume(255);
		after->setFadeType(FadeType::no_fade);
		resources->setBgFadeType(FadeType::no_fade);
	}
		break;
	default:
		break;
	}

}