#pragma once
#include "common.h"
#include "command.h"
#include "resources.h"
#include "DxLib.h"
#include <list>
#include <string>
#include <map>
#include "scene_scenario.h"
//�R�}���h
#include "command_common.h"


using namespace std;

namespace Teng{
	class CommandQueue{
	public:
		CommandQueue(Resources* resources);
		CommandQueue(Resources* resources, const string script_file, SceneScenario* scene_scenaro = nullptr);
		~CommandQueue(){};
		void readScriptFile(const string script_path);
		vector<Command*> get() const { return queue; }
		const string getScriptFilename(){ return script_filename; }
		int tagToType(const string tag){ return command_map[tag]; }
		void setSceneScenario(SceneScenario* scene_scenario){ this->scene_scenario = scene_scenario; }
	private:
		Resources* resources;
		void add(Command* command);
		void loadScript(const string script_path);
		void scriptToCommand();

		map<string, int> command_map;
		void makeCommandMap();

		vector<Command*> queue;
		vector<Command*>::iterator it_queue;
		
		SceneScenario* scene_scenario;
		list<vector<string>> script_list;

		string script_filename;
	};



}
