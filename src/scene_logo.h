#pragma once
#include "scene_state.h"
#include "exception.h"
#include "util.hpp"
#include <thread>
#include <list>

namespace Teng{
	class SceneLogo : public SceneState{
	public:
		SceneLogo(Resources* resources, Input* input, Config* config);
		void preProcess();
		void postProcess();
		scene_t getCurrentState();

		void checkInput();
		void drawGraphic();
		void operateButton();
		void doScene();
		void operateDialog();
		void operateSound();
		void execFade();
		void addFrame(int add_count);
		scene_t getNextScene(){ return next_scene; }
	private:
		Resources* resources;
		Input* input;
		Config* config;
		ULONG frame;
		bool  first_load_flg;
		int   fade_type;
		scene_t next_scene;

		list<thread> th_list;

		// リソース読み込みヘルパ
		void loadImage(const int option_type, const string& filename, const int x = 0, const int y = 0);
		void loadMenuImage(const int option_type, const string& filename, const int x = 0, const int y = 0);
		void loadBgm(const int option_type, const string& filename, const int volume = 255, const int loop_point = 0);
		void loadSe(const int option_type, const string& filename, const int volume = 255, const int loop_point = -1);
		void loadFont(const int option_type, const int size, const string& font_name = "", const bool premul = false);
		void loadScript();
	};

}