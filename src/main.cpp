#include "initializer.h"
#include "scene.h"
#include "input.h"
#include "resources.h"
#include "config.h"
#include <string>
#include <vector>

using namespace std;
using namespace Teng;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
	try{
		Initializer::initialize();
		shared_ptr<Input> input(new Input());
		shared_ptr<Config> config(new Config());
		shared_ptr<Resources> resources(new Resources(config.get(), input.get()));
		Initializer::loadResources(resources.get()); // Resourcesのコンストラクタから呼ぶべき
		unique_ptr<Scene> scene(new Scene(resources.get(), input.get(), config.get()));

		Util::Fps fps;
	
		while (TRUE){
			scene->preProcess();
			scene->checkInput();
			scene->operateButton();
			scene->doScene();
			scene->drawGraphic();
			scene->operateDialog();
			scene->operateSound();
			scene->execFade();
			DEBUG fps.timer();
			scene->postProcess(resources.get(), input.get(), config.get());
		}
	}
	// TENG内定義エラー
	catch (Exception* e){
		if(e->getErrorCode() != ENGINE_SYSTEM_STANDARD_EXIT) e->dialog();
		delete e;
	}
	// C++側で出力されるエラー
	catch (exception& e){
		Exception::dialog(e);
	}
	


	
	DxLib_End();

	return 0;
}

