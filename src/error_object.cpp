#include "error_object.h"

using namespace Teng;

ErrorObject::ErrorObject(error_t error_code, string error_msg) : std::domain_error(error_msg){
	this->error_code = error_code;
}