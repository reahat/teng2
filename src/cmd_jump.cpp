#include "command_common.h"

using namespace Teng;


CmdJump::CmdJump(vector<string> text_command, Resources* resources){
	this->resources = resources;
	args = text_command;
	force_next = true;
}

void CmdJump::execute(){
	is_executed = true;
	DEBUG putLog();
}


//int CmdJump::send(unique_ptr<CommandQueue> command_queue){
//	command_queue.reset(new CommandQueue(resources, args[1]));

//	return 0;
//}

int CmdJump::send(vector<Command*>& queue, const string& label_to, int& it_num){
	
	int local_it_num = 0;
	bool goto_completed = false;
	for (auto cmd : queue){
		if (goto_completed){
			cmd->isExecuted(false);
		}
		else{
			cmd->isExecuted(true);
		}

		// goto の飛び先とラベル名が一致したとき
		if (cmd->getArgs()[0] == "@label" && !goto_completed){
			if (cmd->getArgs()[1] == label_to){
				it_num = local_it_num;
				cmd->isExecuted(true);
				goto_completed = true;
			}
		}
		local_it_num++;
	}


	return it_num;
}