#pragma once
#include "scene_state.h"
#include "exception.h"
#include "util.hpp"
#include <thread>
#include <list>
#include "resources.h"

namespace Teng{
	class SceneMenu : public SceneState{
	public:
		SceneMenu(Resources* resources, Input* input, Config* config);
		void preProcess();
		void postProcess();
		scene_t getCurrentState();

		void checkInput();
		void drawGraphic();
		void operateButton();
		void doScene();
		void operateDialog();
		void operateSound();
		void execFade();
		void addFrame(int add_count);
		scene_t getNextScene(){ return next_scene; }
	private:
		Resources* resources;
		Input* input;
		Config* config;
		ULONG frame;
		bool  first_load_flg;
		int   fade_type;
		scene_t next_scene;

		struct Button{
			Button();
			handle now;
			handle normal;
			handle focus;
			handle click;
			int x, y;
			int width, height;
			int power;
		};
		Button btn_save, btn_load, btn_hskip, btn_config;
		Button btn_backlog, btn_title, btn_exit, btn_back;
		handle menu_base;
		handle back_color;
		int    back_alpha;
	};

}