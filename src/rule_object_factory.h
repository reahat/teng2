#pragma once
#include "resource_object_factory.h"
#include "rule_object.h"

namespace Teng{
	class RuleObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}