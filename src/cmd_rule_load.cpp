#include "command_common.h"
using namespace Teng;

CmdRuleLoad::~CmdRuleLoad(){

}


CmdRuleLoad::CmdRuleLoad(vector<string> text_command, Resources* resources){
	this->resources = resources;
	args = text_command;
	tag = text_command[0];
	filename = text_command[1];
	force_next = true;
}

void CmdRuleLoad::execute(){
	vector<string> script;
	script.push_back(filename);
	resources->add(ResourceType::rule, script);
	is_executed = true;
	DEBUG putLog();
}