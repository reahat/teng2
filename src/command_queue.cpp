#include "command_queue.h"

using namespace Teng;

CommandQueue::CommandQueue(Resources* resources, const string script_file, SceneScenario* scene_scenario){
	this->resources = resources;
	if(scene_scenario != nullptr) this->scene_scenario = scene_scenario;
	const string DIR_SCRIPT = "script\\";
	//const string DEFAULT_SCRIPT_FILE = "scenario1.asc";
	const string INSTALL_DIR = Resources::getRegistryString();
	string script_path = INSTALL_DIR + DIR_SCRIPT + script_file + ".asc";

	makeCommandMap();
	
	script_filename = script_file + ".asc";
	loadScript(script_path);

}

//ファイル名と現在の行数を引数に取るロード時用のコンストラクタ用意する
//新規時は問題ないが、ロード時はloadScriptを別スレッドにて実行すること
// #include <thread> する


void CommandQueue::loadScript(string script_path){
	if (!Resources::isFileExist(script_path)){
		log_error("Script file missing : %s", script_path.c_str());
		RAISE(ENGINE_SCRIPT_FILE_READ_FAILURE);
	}
	//スクリプトファイルの読み込み・分割
	readScriptFile(script_path);
	scriptToCommand();
	
}

void CommandQueue::readScriptFile(const string script_path){
	DxLib::SetUseASyncLoadFlag(FALSE);
	// 一番長いコマンドは@msg/@sayで、25文字 * 2バイト * 3行 = 150
	const int ARGUMENT_MAX_CHARS = 256;
	char c;
	//auto list_it   = script_list.begin();
	
	//auto vector_it = list_it->begin();
	vector<string> cmd_vec;
	//auto vec_it = cmd_vec.begin();

	int script_fp = DxLib::FileRead_open(script_path.c_str());

	char cmd_buf[ARGUMENT_MAX_CHARS];
	int  pos = 0;

	while (TRUE){
		//一文字読み込み
		c = DxLib::FileRead_getc(script_fp);
		//ファイルの終わりのとき
		if(DxLib::FileRead_eof(script_fp) != 0){
			//最終行が空行のときはpushしない
			if (pos != 0){
				cmd_buf[pos] = c;
				cmd_buf[pos + 1] = '\0';
				cmd_vec.push_back(string(cmd_buf));
				script_list.push_back(cmd_vec);
			}
			break;
		}
		//文章先頭の空白部分を読み飛ばす
		while( (c == ' ' || c == '\t') && pos == 0 && !FileRead_eof(script_fp) ) {
			c = DxLib::FileRead_getc(script_fp);
		}

		//区切り文字が出てきたら、次の引数とみなす
		if (c == ',' || c == ' '){
			while (c == ',' || c == ' '){
				c = DxLib::FileRead_getc(script_fp);
			}
			cmd_buf[pos] = '\0';
			cmd_vec.push_back(string(cmd_buf));
			pos = 0;
		}

		//改行文字が出てきた場合，次の行へ移動
		if( c == '\n' || c == '\r') {
			//空行は読み飛ばす
			if( pos == 0 ) {
				continue;
			}
			//\0を文字列の最後に付ける
			cmd_buf[pos] = '\0';
			//引数として追加
			cmd_vec.push_back(string(cmd_buf));
			//コメント行は格納しない
			if (cmd_vec[0].at(0) != '#'){
				script_list.push_back(cmd_vec);
			}
			//次のコマンドとする
			cmd_vec.clear();
			//書き込み位置を0にする
			pos = 0;
		}else {
			//1文字読み込む
			cmd_buf[pos] = c;
			//文字書き込み位置をずらす
			pos++;
		}
	}
	DxLib::FileRead_close(script_fp);
	DxLib::SetUseASyncLoadFlag(TRUE);
	log_info("Script file \"%s\" load completed.", script_filename.c_str());
}

void CommandQueue::makeCommandMap(){
	command_map.insert(std::pair<std::string, int>("#", TENG_TAG_COMMENT));
	command_map.insert(std::pair<std::string, int>("@msg", TENG_TAG_MESSAGE));
	command_map.insert(std::pair<std::string, int>("@message", TENG_TAG_MESSAGE));
	command_map.insert(std::pair<std::string, int>("@char.load", TENG_TAG_LOADCHAR));
	command_map.insert(std::pair<std::string, int>("@char.draw", TENG_TAG_DRAWCHAR));
	command_map.insert(std::pair<std::string, int>("@char.hide", TENG_TAG_HIDECHAR));
	command_map.insert(std::pair<std::string, int>("@char.free", TENG_TAG_FREECHAR));
	command_map.insert(std::pair<std::string, int>("@char.change", TENG_TAG_CHANGECHAR));
	command_map.insert(std::pair<std::string, int>("@import", TENG_TAG_IMPORT));
	command_map.insert(std::pair<std::string, int>("@return", TENG_TAG_RETURN));
	command_map.insert(std::pair<std::string, int>("@label", TENG_TAG_LABEL));
	command_map.insert(std::pair<std::string, int>("@goto", TENG_TAG_GOTO));
	command_map.insert(std::pair<std::string, int>("@bg.load", TENG_TAG_LOADBGIMAGE));
	command_map.insert(std::pair<std::string, int>("@bg.draw", TENG_TAG_DRAWBGIMAGE));
	command_map.insert(std::pair<std::string, int>("@bg.change", TENG_TAG_CHANGEBGIMAGE));
	command_map.insert(std::pair<std::string, int>("@rule.load", TENG_TAG_LOADRULE));
	command_map.insert(std::pair<std::string, int>("@rule.free", TENG_TAG_FREERULE));
	command_map.insert(std::pair<std::string, int>("@bg.free", TENG_TAG_FREEBGIMAGE));
	command_map.insert(std::pair<std::string, int>("@face.load", TENG_TAG_LOADFACE));
	command_map.insert(std::pair<std::string, int>("@face.set", TENG_TAG_SETFACE));
	command_map.insert(std::pair<std::string, int>("@face.clear", TENG_TAG_CLEARFACE));
	command_map.insert(std::pair<std::string, int>("@face.free", TENG_TAG_FREEFACE));
	command_map.insert(std::pair<std::string, int>("@bgm.load", TENG_TAG_LOADBGM));
	command_map.insert(std::pair<std::string, int>("@bgm.play", TENG_TAG_PLAYBGM));
	command_map.insert(std::pair<std::string, int>("@bgm.stop", TENG_TAG_STOPBGM));
	command_map.insert(std::pair<std::string, int>("@bgm.free", TENG_TAG_FREEBGM));
	command_map.insert(std::pair<std::string, int>("@se.load", TENG_TAG_LOADSE));
	command_map.insert(std::pair<std::string, int>("@se.play", TENG_TAG_PLAYSE));
	command_map.insert(std::pair<std::string, int>("@se.stop", TENG_TAG_STOPSE));
	command_map.insert(std::pair<std::string, int>("@se.free", TENG_TAG_FREESE));
	command_map.insert(std::pair<std::string, int>("@select", TENG_TAG_SELECT));
	command_map.insert(std::pair<std::string, int>("@char.love", TENG_TAG_LOVE));
	command_map.insert(std::pair<std::string, int>("@if", TENG_TAG_IF));
	command_map.insert(std::pair<std::string, int>("@nameset", TENG_TAG_NAMESET));
	command_map.insert(std::pair<std::string, int>("@say", TENG_TAG_SAY));
	command_map.insert(std::pair<std::string, int>("@window.settitle", TENG_TAG_SETTITLE));
	command_map.insert(std::pair<std::string, int>("@window.resettitle", TENG_TAG_RESETTITLE));
	command_map.insert(std::pair<std::string, int>("@wait", TENG_TAG_WAIT));
	command_map.insert(std::pair<std::string, int>("@msg.hide", TENG_TAG_HIDEMSG));
	command_map.insert(std::pair<std::string, int>("@msg.display", TENG_TAG_DISPMSG));
	command_map.insert(std::pair<std::string, int>("@movie.play", TENG_TAG_PLAYMOVIE));
	command_map.insert(std::pair<std::string, int>("@jump", TENG_TAG_JUMP));
	command_map.insert(std::pair<std::string, int>("@flag.displayGalleryMenu", TENG_TAG_DISPGALLERY));
	command_map.insert(std::pair<std::string, int>("@msg.ex", TENG_TAG_EXMESSAGE));
}

void CommandQueue::scriptToCommand(){
	for (auto cmd : script_list){
		int cmd_type = cmd.at(0)[0] == '@' ? command_map[cmd.at(0)] : TENG_TAG_EXMESSAGE;
		if (cmd_type == TENG_TAG_EXMESSAGE){
			CmdExMsg* exmsg = new CmdExMsg(cmd, resources);
			cmd = exmsg->getCmd();
			delete exmsg;
			cmd_type = cmd.at(0) == "@msg" ? TENG_TAG_MESSAGE : TENG_TAG_SAY;
		}

		switch (cmd_type){
		case TENG_TAG_COMMENT:
			break;
		case TENG_TAG_MESSAGE:
			queue.push_back(new CmdMsg(cmd, resources));
			break;
		case TENG_TAG_SAY:
			queue.push_back(new CmdSay(cmd, resources));
			break;
		case TENG_TAG_LOADCHAR:
			break;
		case TENG_TAG_DRAWCHAR:
			break;
		case TENG_TAG_HIDECHAR:
			break;
		case TENG_TAG_FREECHAR:
			break;
		case TENG_TAG_CHANGECHAR:
			break;
		case TENG_TAG_IMPORT:
			break;
		case TENG_TAG_JUMP:
			queue.push_back(new CmdJump(cmd, resources));
			break;
		case TENG_TAG_RETURN:
			queue.push_back(new CmdReturn(cmd, resources, scene_scenario));
			break;
		case TENG_TAG_LABEL:
			queue.push_back(new CmdLabel(cmd, resources));
			break;
		case TENG_TAG_GOTO:
			queue.push_back(new CmdGoto(cmd, resources));
			break;
		case TENG_TAG_LOADBGIMAGE:
			queue.push_back(new CmdBgLoad2(cmd, resources));
			break;
		case TENG_TAG_DRAWBGIMAGE:
			queue.push_back(new CmdBgDraw(cmd, resources));
			break;
		case TENG_TAG_CHANGEBGIMAGE:
			queue.push_back(new CmdBgChange(cmd, resources));
			break;
		case TENG_TAG_LOADRULE:
			queue.push_back(new CmdRuleLoad(cmd, resources));
			break;
		case TENG_TAG_FREEBGIMAGE:
			queue.push_back(new CmdBgFree(cmd, resources));
			break;
		case TENG_TAG_FREERULE:
			queue.push_back(new CmdRuleFree(cmd, resources));
			break;
		case TENG_TAG_LOADFACE:
			queue.push_back(new CmdFaceLoad(cmd, resources));
			break;
		case TENG_TAG_SETFACE:
			queue.push_back(new CmdFaceSet(cmd, resources));
			break;
		case TENG_TAG_CLEARFACE:
			queue.push_back(new CmdFaceClear(cmd, resources));
			break;
		case TENG_TAG_FREEFACE:
			queue.push_back(new CmdFaceFree(cmd, resources));
			break;
		case TENG_TAG_LOADBGM:
			break;
		case TENG_TAG_PLAYBGM:
			break;
		case TENG_TAG_STOPBGM:
			break;
		case TENG_TAG_FREEBGM:
			break;
		case TENG_TAG_LOADSE:
			break;
		case TENG_TAG_PLAYSE:
			break;
		case TENG_TAG_STOPSE:
			break;
		case TENG_TAG_FREESE:
			break;
		case TENG_TAG_SELECT:
			break;
		case TENG_TAG_LOVE:
			break;
		case TENG_TAG_IF:
			break;
		case TENG_TAG_NAMESET:
			queue.push_back(new CmdNameset(cmd, resources));
			break;
		case TENG_TAG_SETTITLE:
			break;
		case TENG_TAG_RESETTITLE:
			break;
		case TENG_TAG_WAIT:
			break;
		case TENG_TAG_HIDEMSG:
			break;
		case TENG_TAG_DISPMSG:
			break;
		case TENG_TAG_PLAYMOVIE:
			break;
		case TENG_TAG_DISPGALLERY:
			break;
		default:
			log_error("Invalid command \"%s\"", cmd.at(0).c_str());
			throw ENGINE_SCRIPT_INVALID_COMMAND;
		}

		//#ifdef _DEBUG
		//printf("[INFO] Load script : ");
		//for (auto in : cmd){ printf("%s ", in.c_str()); }
		//printf("\n");
		//#endif
	}
}