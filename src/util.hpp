#pragma once
#include <vector>
#include <string>
#include <sstream>
#include <stdarg.h>

namespace Teng{
	class Util{
	public:
		template <class T> static void push_front(std::vector<T> &dst, T &src);
		template <class T_in> static std::string convertString(T_in input);
		template <class T_ic> static bool included(T_ic val, T_ic arr[]);
		class Fps{
		public:
			Fps();
			void timer();
		private:
			int counter;
			int FpsTime[2];
			int FpsTime_i;
			double fps;
			int color_white;
		};
	};

	// list::push_frontのvector版。多用しないこと
	template <class T> void Util::push_front(std::vector<T> &dst, T &src){
		std::vector<T> new_vec;
		new_vec.push_back(src);
		new_vec.insert(new_vec.end(), dst.begin(), dst.end());
		dst = new_vec;
	}

	// 入力をstringに変換する
	template <class T_in> std::string Util::convertString(T_in input){
		std::stringstream ss;
		ss << input;
		return ss.str();
	}

	// 配列に指定した値が含まれているかを検査
	// usage:
	// int collection[] = { 1, 2, 3 };
	// Util::included(2, collection); => true
	template <class T_ic> bool Util::included(T_ic val, T_ic arr[]){
		for (auto arr_val : arr){
			if (val == arr_val) return true;
		}
		return false;
	}

}