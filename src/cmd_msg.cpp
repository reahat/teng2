#include "command_common.h"

using namespace Teng;


CmdMsg::~CmdMsg(){
	
}

CmdMsg::CmdMsg(vector<string> text_command, Resources* resources) : 
current_line(0),
current_char(0),
draw_state(MsgDrawState::msg_drawing),
plus_location(),
char_name("")
{
	Backlog log;
	msg_color = DxLib::GetColor(32, 32, 32);
	for (auto font : resources->getFonts()){
		if (font->getOptionType() == FontType::font_msg){
			msg_font = font->getHandle();
			continue;
		}
		if (font->getOptionType() == FontType::font_rubi){
			rubi_font = font->getHandle();
			continue;
		}
	}

	// 通常
	// [0] tag
	// [1] full_msg
	// 簡略
	// [0] full_msg
	this->resources = resources;
	args = text_command;
	tag = text_command[0];
	full_msg = args[1];
	force_next = false;
}

void CmdMsg::execute(){
	// バックログに入る際、最後に実行したmsgをexecuted=falseにする
	// → シナリオ画面に戻ったとき、再度msgが実行されるので、中身の実行をスキップする
	// 回避しないと、既存のメッセージの末尾にmsgが追加される
	if (current_line == msg_line.size() - 1){
		is_executed = true;
		return;
	}
	// メッセージの行分割まで
	
	//メッセージ全体のサイズ
	string::size_type msg_length = full_msg.length();
	//行のメッセージを格納
	string line;
	int line_num = 0;//何行目か
	int bytenum = 1; //1バイト目か2バイト目か
	int char_line_cnt = 0; //25文字になったらリセット
	int mark_cnt = 0; //改行等で文字を進める

	//セリフの２行目以降インデント判定
	bool second_line_indent = false;
	string first_char;
	first_char += full_msg[0];
	first_char += full_msg[1];
	if (first_char == "「" || first_char == "（"){
		second_line_indent = true;
	}

	//話者名の置換
	if (char_name == "$"){
		char_name = resources->getConfig()->getSetting().heroine_name;
	}

	for (size_t i = 0; i < msg_length; i++){

		//名前代替文字の場合
		if (full_msg[i] == '$' && bytenum == 1){
			full_msg.replace(i, 1, resources->getConfig()->getSetting().heroine_name);
			msg_length = full_msg.length();
		}

		//改行指定文字の場合
		if (full_msg[i] == '|' && bytenum == 1){
			msg_line.push_back(line);
			line.clear();
			char_line_cnt = 0;
			line_num++;
			if (second_line_indent){
				line += "　"; //全角SP
				char_line_cnt = 2;
			}
			continue;
		}

		//プラス記号の位置を保存
		//「１２+３４５+６７」の場合、配列の[4]と[6]に値(存在を示す1)がセットされる
		if (full_msg[i] == '+' && bytenum == 1){
			plus_location[line_num].push_back(i - mark_cnt);
			mark_cnt++;
			continue;
		}

		//三点リーダ・ダッシュの分断回避
		if (char_line_cnt == 48 && i < msg_length - 2){ //25文字目。25文字目が最終文字の場合は気にしない
			//25文字目と次行の1文字目が共にダッシュor三点リーダのとき、26文字目に記号の2つ目を入れる
			if (isContinuousChars(full_msg[i], full_msg[i+1], full_msg[i+2], full_msg[i+3])){
				line += full_msg[i];
				line += full_msg[i + 1];
				line += full_msg[i + 2];
				line += full_msg[i + 3];
				i += 4;
				char_line_cnt = 0;
				msg_line.push_back(line);
				line.clear();
				bytenum = 1;
				line_num++;
				if (second_line_indent){
					line += "　"; //全角SP
					char_line_cnt = 2;
				}
			}
		}

		//行頭禁則文字
		//行頭が禁則文字の場合、前の行を26文字にする
		//if ((!second_line_indent && char_line_cnt == 0) || (second_line_indent && char_line_cnt == 2)){
		if (char_line_cnt == 0){
			if (isFrontProhibitChar(full_msg[i], full_msg[i + 1])){
				//前の行の最後に文字を追加
				msg_line[line_num - 1] = msg_line[line_num - 1] + full_msg[i] + full_msg[i + 1];
				i += 2;

				//行頭禁則文字が最後の文字の場合は26文字にした時点で終了
				//↑で i += 2 したために、full_msg で out of lange してしまう
				if (i < msg_length){
					//元に戻す場合に備えてバックアップ
					string bk_before_line = msg_line[line_num - 1];
					int    bk_i = i;
					int    bk_char_line_cnt = char_line_cnt;
					string bk_line = line;

					//さらに次の文字も行頭禁則になってしまう場合、前行を24文字にする
					if (isFrontProhibitChar(full_msg[i], full_msg[i + 1])){
						string::size_type before_line_length = msg_line[line_num - 1].length();
						msg_line[line_num - 1].erase(before_line_length - 4, before_line_length);
						char adj[5];
						adj[0] = full_msg[i - 4];
						adj[1] = full_msg[i - 3];
						adj[2] = full_msg[i - 2];
						adj[3] = full_msg[i - 1];
						adj[4] = '\0';
						line += string(adj);
						char_line_cnt += 4;
					}

					//それでも次の文字が行頭禁則になる場合は、前行を23文字にする
					if (isFrontProhibitChar(full_msg[i], full_msg[i + 1])){
						string::size_type before_line_length = msg_line[line_num - 1].length();
						msg_line[line_num - 1].erase(before_line_length - 2, before_line_length);
						char adj[7];
						adj[0] = full_msg[i - 6];
						adj[1] = full_msg[i - 5];
						adj[2] = full_msg[i - 4];
						adj[3] = full_msg[i - 3];
						adj[4] = full_msg[i - 2];
						adj[5] = full_msg[i - 1];
						adj[6] = '\0';
						line.clear();
						line += string(adj);
						char_line_cnt += 2;
					}

					//それでもやっぱり行頭禁則文字になる場合は、前行を26文字にした時点に戻して諦める
					if (isFrontProhibitChar(full_msg[i - 6], full_msg[i - 5])){
						msg_line[line_num - 1] = bk_before_line;
						i = bk_i;
						char_line_cnt = bk_char_line_cnt;
						line = bk_line;
					}
					string tmp_sp;
					tmp_sp += line[0];
					tmp_sp += line[1];
					if (second_line_indent && line_num > 0 && tmp_sp != "　")
						line = "　" + line;
				}
			}
		}

		//通常は１バイト格納する
		line += full_msg[i];
		char_line_cnt++;
		bytenum = bytenum == 1 ? 2 : 1;


		//文字本体の後にルビのカッコが来る
		if (bytenum == 1 && full_msg[i + 1] == '['){
			Rubi rubi;
			rubi.base_line_num = line_num;
			rubi.base_loc_byte = line.size() - 2;
			i += 2;//開カッコの次の文字へ
			mark_cnt += 2; //開きカッコ、閉じカッコの分
			int rubi_bytenum = 1;
			while (!(rubi_bytenum == 1 && full_msg[i] == ']')){
				rubi.rubi_text += full_msg[i];
				mark_cnt++;
				i++;
				rubi_bytenum = rubi_bytenum == 1 ? 2 : 1;
			}
			
			rubi_list.push_back(rubi);
		}

		//行末まで来たら
		if (char_line_cnt >= 50 || i == msg_length-1){
			//行末禁則文字なら行末を１文字削り、次の行へ持ち越す
			if (isEndProhibitChar(full_msg[i - 1], full_msg[i])){
				line.erase(i - 1 - mark_cnt, i + 1 - mark_cnt);
				i -= 2;
			}

			char_line_cnt = 0;
			msg_line.push_back(line);
			line.clear();
			bytenum = 1;
			line_num++;
			if (second_line_indent){
				line += "　"; //全角SP
				char_line_cnt = 2;
			}
		}
	}

	//ログ格納
	BacklogMessage log_line;
	int cnt = 0;
	for (string line : msg_line){
		if (cnt == 0) log_line.setSpeaker(char_name);
		log_line.add(line);
		cnt++;
	}
	resources->getLog().push(log_line);

	is_executed = true;
	DEBUG putLog();
}

bool CmdMsg::isShiftJisFirstByte(const unsigned char c){
		return (c >= 0x81 && c <= 0x9F) || (c >= 0xE0 && c <= 0xFC);
}

bool CmdMsg::isFrontProhibitChar(const unsigned char first_byte, const unsigned char second_byte){
	const char* PROHIBITS = "）」］』、。，．！？ー〜−〃々ヽヾ・ぁぃぅぇぉっゃゅょァィゥェォッャュョヵヶ";
	size_t prohibits_length = string(PROHIBITS).length();
	for (size_t i = 0; i < prohibits_length; i+=2){
		if ((unsigned char)PROHIBITS[i] == first_byte && (unsigned char)PROHIBITS[i + 1] == second_byte){
			return true;
		}
	}
	return false;
}

bool CmdMsg::isEndProhibitChar(const unsigned char first_byte, const unsigned char second_byte){
	const char* PROHIBITS = "（「［『";
	size_t prohibits_length = sizeof(PROHIBITS);
	for (size_t i = 0; i < prohibits_length; i+=2){
		if ((unsigned char)PROHIBITS[i] == first_byte && (unsigned char)PROHIBITS[i + 1] == second_byte){
			return true;
		}
	}
	return false;
}


bool CmdMsg::isContinuousChars(
	const unsigned char first_1,
	const unsigned char first_2,
	const unsigned char second_1,
	const unsigned char second_2)
{
	const char* continuous_1 = "…";
	const char* continuous_2 = "―"; // 0x81 0x5c

	if (first_1 == second_1 && first_2 == second_2 && first_1 == (unsigned char)continuous_1[0] && first_2 == (unsigned char)continuous_1[1])
		return true;
	if (first_1 == second_1 && first_2 == second_2 && first_1 == (unsigned char)continuous_2[0] && first_2 == (unsigned char)continuous_2[1])
		return true;

	return false;
}
void CmdMsg::update(){

}

int CmdMsg::draw(){
	static const int MESSAGE_POINT_X = 100;
	static const int MESSAGE_POINT_Y = 550;
	static const int LINE_DISTANCE = 28;
	static const int MESSAGE_SPEAKER_NAME_X = 100;
	static const int MESSAGE_SPEAKER_NAME_Y = 515;

	auto next_plus_it = plus_location[current_line].begin();
	int msg_lines = msg_line.size();
	int current_line_chars = msg_line[current_line].length();
	//次のプラスの位置。クリックしたらpop_frontすればよい
	int next_plus = -1;
	if (plus_location[current_line].size() > 0){
		next_plus = *next_plus_it;
	}


	//高速表示時に、行の限界を超えて表示した場合の対応
	if (current_char > current_line_chars) current_char = current_line_chars;

	//名前表示
	DxLib::DrawStringToHandle(
		MESSAGE_SPEAKER_NAME_X,
		MESSAGE_SPEAKER_NAME_Y,
		char_name.c_str(),
		msg_color,
		msg_font);

	//現在行まで表示
	for (int i = 0; i <= current_line; i++){
		//表示完了した行はすべて表示
		if (i < current_line){
			//テキスト本体
			DxLib::DrawStringToHandle(
				MESSAGE_POINT_X,
				MESSAGE_POINT_Y + LINE_DISTANCE * i,
				(msg_line[i]).c_str(),
				msg_color,
				msg_font);
			//ルビ
			for (auto rubi : rubi_list){
				if (i == rubi.base_line_num){
					DxLib::DrawStringToHandle(
						MESSAGE_POINT_X + rubi.base_loc_byte * 11 + calcRubiXAdjust(rubi.rubi_text),
						MESSAGE_POINT_Y + LINE_DISTANCE * i - RUBI_FONT_SIZE,
						rubi.rubi_text.c_str(),
						msg_color,
						rubi_font);
				}
			}
		}
		//表示途中の行
		else{
			DxLib::DrawStringToHandle(
				MESSAGE_POINT_X,
				MESSAGE_POINT_Y + LINE_DISTANCE * i,
				(msg_line[i].substr(0, current_char)).c_str(),
				msg_color,
				msg_font);
			//ルビ
			for (auto rubi : rubi_list){
				if (i == rubi.base_line_num){
					if (current_char > rubi.base_loc_byte){
						DxLib::DrawStringToHandle(
							MESSAGE_POINT_X + rubi.base_loc_byte * 11 + calcRubiXAdjust(rubi.rubi_text),
						MESSAGE_POINT_Y + LINE_DISTANCE * i - RUBI_FONT_SIZE,
						rubi.rubi_text.c_str(),
						msg_color,
						rubi_font);
					}
				}
			}
		}
	}

	//行末まで表示したら
	if (current_char >= current_line_chars && current_line < msg_lines-1){
		current_line++;
		current_char = 0;
	}

	//次のプラスが存在するとき
	if (next_plus >= 0 && current_char + 1 >= next_plus){
		draw_state = MsgDrawState::msg_plus_stopping;
	}
	//行内に次のプラスが存在しないとき
	else{

		//表示中の行は表示文字を増やす
		if (current_char < current_line_chars){
			//current_char += 2;
			current_char += checkMsgAdvance();


		}
	}

	


	//表示中にクリックしたら早送り
	if (draw_state == MsgDrawState::msg_highspeed){
		current_char += 4;
	}
	
	//最後まで表示したらステータスを完了にする
	if (current_line == msg_lines - 1 && current_char >= current_line_chars){
		draw_state = MsgDrawState::msg_complete;
	}

	return draw_state;
}

const int CmdMsg::checkMsgAdvance(){
	static int f = 0;
	int ret = 0;

	switch (resources->getConfig()->getSetting().message_wait){
	case 0:
		ret = (f % 7 == 0) ? 2 : 0;
		break;
	case 1:
		ret = (f % 6 == 0) ? 2 : 0;
		break;
	case 2:
		ret = (f % 5 == 0) ? 2 : 0;
		break;
	case 3:
		ret = (f % 4 == 0) ? 2 : 0;
		break;
	case 4:
		ret = (f % 3 == 0) ? 2 : 0;
		break;
	case 5:
		ret = (f % 2 == 0) ? 0 : 2;
		break;
	case 6:
		ret = (f % 3 == 0) ? 0 : 2;
		break;
	case 7:
		ret = (f % 4 == 0) ? 0 : 2;
		break;
	case 8:
		ret = (f % 5 == 0) ? 0 : 2;
		break;
	case 9:
		ret = 2;
		break;
	default:
		ret = 2;
		break;
	}

	f++;
	return ret;
}

void CmdMsg::setInput(const int any_input){
	switch (any_input){
	case 1:
		//表示途中にクリックで高速表示
		draw_state = MsgDrawState::msg_highspeed;
		break;
	case 2:
		//プラスストップ中にクリック
		draw_state = MsgDrawState::msg_drawing;
		plus_location[current_line].pop_front();
		break;
	default:
		break;
	}
}

int CmdMsg::calcRubiXAdjust(const string& rubi){
	int x_adjust = 0;
	if (rubi.size() <= 2){
		x_adjust = MSG_FONT_SIZE / 2 - RUBI_FONT_SIZE / 2;
	}
	else if (rubi.size() >= 6){
		if (rubi.size() == 6) x_adjust = -4;
		if (rubi.size() == 8) x_adjust = -9;
	}
	return x_adjust;
}

int CmdMsg::send(vector<Command*>& queue, int& it_num){

	return 0;
}