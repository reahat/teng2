#include "command_common.h"
using namespace Teng;

CmdBgChange::~CmdBgChange(){

}

CmdBgChange::CmdBgChange(vector<string> text_command, Resources* resources){
	// [0] tag
	// [1] bg_filename
	// [2] fade_type
	// [3] fade_power
	// [4] rule_filename
	// [5] fade_threshold
	size_t cmd_size = text_command.size();
	this->resources = resources;
	args = text_command;

	static const int DEFAULT_POWER = 5;
	static const int DEFAULT_THRESHOLD = 64;

	tag = text_command[0];
	bg_filename = text_command[1];

	if (text_command[2] == "none"){
		fade_type = FadeType::no_fade;
	}
	else if (text_command[2] == "rule"){
		fade_type = FadeType::use_rule;
	}
	else if (text_command[2] == "cross" || text_command[2] == "crossfade"){
		fade_type = FadeType::crossfade;
	}
	else{
		fade_type = FadeType::no_fade;
	}

	if (cmd_size < 4){
		fade_power = DEFAULT_POWER;
	}
	else{
		fade_power = stoi(text_command[3]);
	}

	if (cmd_size < 5){
		rule_filename = "";
	}
	else{
		rule_filename = text_command[4];
	}

	if (cmd_size < 6){
		fade_threshold = DEFAULT_THRESHOLD;
	}
	else{
		fade_threshold = stoi(text_command[5]);
	}
	

}

void CmdBgChange::execute(){
	int cnt = 0;
	for (auto bg : resources->getBgImages()){
		// before画像
		if (bg->isVisible()){
			bg->setFadeType(FadeType::fadeout);//いらない
			bg->setVolume(255);
			resources->setBgBefore(bg);
			//しきい値、変化powerはbefore側に持つ
			bg->setBlendThreshold(fade_threshold);
			bg->setTransitionSpeed(fade_power);
			cnt++;
		}
		// after画像
		if (bg->getFileName() == bg_filename){
			bg->isVisible(true);
			bg->setFadeType(FadeType::fadein);//不要
			bg->setVolume(0);
			resources->setBgAfter(bg);
			cnt++;
		}
		if (cnt == 2) break;
	}
	// 現在crossfade中、none中、rule中のどれか
	resources->setBgFadeType(fade_type);
	
	if (fade_type == FadeType::use_rule){
		for (auto rule : resources->getRules()){
			if (rule->getFileName() == rule_filename){
				rule->isVisible(true);
				resources->setBgRule(rule);
				break;
			}
		}
	}

	// フェード完了かクリックするまで次のコマンドを実行しない
	force_next = false;
	is_executed = true;
	putLog();
}