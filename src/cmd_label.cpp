#include "command_common.h"

using namespace Teng;


CmdLabel::CmdLabel(vector<string> text_command, Resources* resources){
	this->resources = resources;
	args = text_command;
	force_next = true;
}

void CmdLabel::execute(){
	is_executed = true;
	DEBUG putLog();
}