#include "command_common.h"

using namespace Teng;


CmdBgLoad2::~CmdBgLoad2(){
	
}

CmdBgLoad2::CmdBgLoad2(vector<string> text_command, Resources* resources){
	this->resources = resources;
	args = text_command;
	tag = text_command[0];
	filename = text_command[1];
	force_next = true;
}

void CmdBgLoad2::execute(){
	vector<string> script;
	script.push_back(filename);
	resources->add(ResourceType::back_image, script);
	is_executed = true;
	DEBUG putLog();
}