#pragma once
#include "scene_state.h"
#include "exception.h"
#include <thread>

namespace Teng{
	class SceneConfig : public SceneState{
	public:
		SceneConfig(Resources* resources, Input* input, Config* config);
		void preProcess();
		void postProcess();
		scene_t getCurrentState();

		void checkInput();
		void drawGraphic();
		void operateButton();
		void doScene();
		void operateDialog();
		void operateSound();
		void execFade();
		void addFrame(int add_count);
		scene_t getNextScene(){ return next_scene; }
		bool cursorIn(const int& x, const int& y, const ResourceObject& obj);
	private:
		thread thread;
		static const int BACK_X = 27;
		static const int BACK_Y = 696;
		static const int SKIP_ALL_X = 450;
		static const int SKIP_Y = 251;
		static const int SKIP_ALREADY_X = 579;
		static const int MINUS_X = SKIP_ALL_X;
		static const int PLUS_X = 886;
		static const int MINUS_Y1 = 313;
		static const int MINUS_Y2 = 366;
		static const int MINUS_Y3 = 419;
		static const int MINUS_Y4 = 476;
		static const int MINUS_Y5 = 526;
		static const int BAR_BASE_X = 487;
		static const int BAR_BASE_Y1 = MINUS_Y1 + 9;
		static const int BAR_BASE_Y2 = MINUS_Y2 + 9;
		static const int BAR_BASE_Y3 = MINUS_Y3 + 9;
		static const int BAR_BASE_Y4 = MINUS_Y4 + 9;
		static const int BAR_BASE_Y5 = MINUS_Y5 + 9;
		static const int BAR_Y1 = MINUS_Y1 + 4;
		static const int BAR_Y2 = MINUS_Y2 + 4;
		static const int BAR_Y3 = MINUS_Y3 + 4;
		static const int BAR_Y4 = MINUS_Y4 + 4;
		static const int BAR_Y5 = MINUS_Y5 + 4;
		static const int VOL_0 = 0;
		static const int VOL_1 = 70;
		static const int VOL_2 = 90;
		static const int VOL_3 = 110;
		static const int VOL_4 = 140;
		static const int VOL_5 = 170;
		static const int VOL_6 = 190;
		static const int VOL_7 = 210;
		static const int VOL_8 = 230;
		static const int VOL_9 = 250;

		typedef enum eButtonState{
			btn_st_normal,
			btn_st_focus,
			btn_st_click
		}ButtonState;

		struct Btn{
			handle now = 0;
			handle normal = 0;
			handle focus = 0;
			handle click = 0;
			int x = 0;
			int y = 0;
			int width = 0;
			int height = 0;
			int power = 0;
			int status = ButtonState::btn_st_normal;
		};
		handle background;
		handle bar_base;
		int bar_base_width;
		bool mouse_left_clicked;

		Btn btn_all;
		Btn btn_already;
		Btn btn_minus1, btn_minus2, btn_minus3, btn_minus4, btn_minus5;
		Btn btn_plus1, btn_plus2, btn_plus3, btn_plus4, btn_plus5;
		Btn btn_back;
		Btn bar1, bar2, bar3, bar4, bar5;
		int tmp_bar3_vol, tmp_bar4_vol, tmp_bar5_vol;
		int keep_btn;

		int bar1x, bar2x, bar3x, bar4x, bar5x;

		int skip_status;
		enum{ sk_already, sk_all};
		bool back_clicked;

		Resources* resources;
		Input* input;
		Config* config;
		ULONG frame;
		int   fade_type;
		scene_t next_scene;
		bool cursorIn(const Btn& button)const;
		void adjustBarLoc(Btn& btn);
		int volumeToPower(const int& vol);
		int powerToVolume(const int& pow);
		int powerToX(const int& pow);
		int XtoPower(const int& x);
		void clickPlusButton(Btn& btn);
		void clickMinusButton(Btn& btn);
	    void writeConfig();
		void checkVolume();
		int  calcPlayBgmVolume(ResourceObject& bgm);
		int  calcPlaySeVolume(ResourceObject& se);
	};

}