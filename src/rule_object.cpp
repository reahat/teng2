#include "rule_object.h"

using namespace Teng;

RuleObject::RuleObject(vector<string> command){
	// [0] ファイル名
	string fullpath = Resources::PathRuleImage() + command[0];
	handle = DxLib::LoadBlendGraph(fullpath.c_str());
	if (!Resources::isFileExist(fullpath)){
		log_error("ルール画像の読み込みに失敗しました: %s", command[0].c_str());
		RAISE(ENGINE_GRAPHIC_RULE_GRAPHIC_LOADGRAPH_FAILED);
	}
	file_name = command[0];
	log_info("RuleImage File loading : %s", file_name.c_str());
}
