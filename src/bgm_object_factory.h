#pragma once
#include "resource_object_factory.h"
#include "bgm_object.h"

namespace Teng{
	class BgmObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}