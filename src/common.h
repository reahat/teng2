#pragma once
#include "error_code.h"
#include <sstream>
#include <vector>

#ifdef _DEBUG
#define DEBUG //デバッグ時のみ使用する関数 例：DEBUG printf("...");
#define IF_DEBUG_TRUE TRUE
#define IF_DEBUG_FALSE FALSE
#else
#define DEBUG 1 ? (void)0 :
#define IF_DEBUG_TRUE FALSE
#define IF_DEBUG_FALSE TRUE
#endif

typedef int error_t;
typedef int handle;
typedef int scene_t;
typedef int resource_t;

// 例外生成 RAISE(ERROR_CODE)
#define RAISE(e) Exception::raise(e)

// デバッグ用ログ出力
typedef enum eLogType{debug, info, warn, error}LogType;
#ifdef _DEBUG
#define log_info(lpszFormat, ...)  Logger::OutputLog(info, _T(__FUNCTION__), lpszFormat, __VA_ARGS__)
#define log_warn(lpszFormat, ...)  Logger::OutputLog(warn, _T(__FUNCTION__), lpszFormat, __VA_ARGS__)
#define log_error(lpszFormat, ...) Logger::OutputLog(error, _T(__FUNCTION__), lpszFormat, __VA_ARGS__)
#define log_debug(lpszFormat, ...) Logger::OutputLog(debug, _T(__FUNCTION__), lpszFormat, __VA_ARGS__)
#else
#define log_info(...)
#define log_warn(...)
#define log_error(...)
#define log_debug(...)
#endif
namespace Teng{
	class Logger{
	public:
		static void OutputLog(const LogType log_type, const char* lpszFuncName, const char* lpszFormat, ...);
	};
}

typedef enum eSceneType{
	no_change,
	scene_exit,
	logo,
	title,
	title_load,
	title_config,
	menu,
	gallery,
	scenario,
	scenario_load,
	scenario_config,
	backlog,
	movie,
	save
} SceneType;

typedef enum eResourceType{
	system_graphic,
	system_bgm,
	system_se,
	system_effect,
	back_image,
	rule,
	character,
	face,
	font
} ResourceType;

typedef enum eSystemGraphicType{
	none,
	msgwindow,
	btn_save,
	btn_load,
	close_dialog,
	bg_save_1,
	bg_load_1,
	bg_title,
	bg_logo_1,
	bg_config,
	dialog_close,
	dialog_load,
	dialog_overwrite,
	dialog_delete,
	select_normal,
	select_focus,
	select_selected,
	name_entry_box,
	message_arrow,
	bg_gallery,
	gallery_pic_small,
	gallery_pic_large,
	gallery_rule_slide,
	button_title_start_normal,
	button_title_start_focus,
	button_title_start_click,
	button_title_load_normal,
	button_title_load_focus,
	button_title_load_click,
	button_title_config_normal,
	button_title_config_focus,
	button_title_config_click,
	button_title_gallery_normal,
	button_title_gallery_focus,
	button_title_gallery_click,
	button_title_exit_normal,
	button_title_exit_focus,
	button_title_exit_click,
	button_save_page1_normal,
	button_save_page1_focus,
	button_save_page1_click,
	button_save_page2_normal,
	button_save_page2_focus,
	button_save_page2_click,
	button_save_page3_normal,
	button_save_page3_focus,
	button_save_page3_click,
	button_save_page4_normal,
	button_save_page4_focus,
	button_save_page4_click,
	button_save_page5_normal,
	button_save_page5_focus,
	button_save_page5_click,
	button_save_back_normal,
	button_save_back_focus,
	button_save_back_click,
	button_save_icon_bara,
	button_save_icon_arrow,
	rule_save_confirm_dialog,
	rule_save_confirm_dialog_reverse,
	button_config_bar_base,
	button_config_minus_normal,
	button_config_minus_focus,
	button_config_minus_click,
	button_config_plus_normal,
	button_config_plus_focus,
	button_config_plus_click,
	button_config_skip_all_normal,
	button_config_skip_all_focus,
	button_config_skip_all_click,
	button_config_skip_already_normal,
	button_config_skip_already_focus,
	button_config_skip_already_click,
	button_config_back_normal,
	button_config_back_focus,
	button_config_back_click,
	button_config_bar_normal,
	button_config_bar_focus,
	button_config_bar_click,
	//右クリックメニュー
	button_menu_save_normal,
	button_menu_load_normal,
	button_menu_hskip_normal,
	button_menu_config_normal,
	button_menu_backlog_normal,
	button_menu_title_normal,
	button_menu_exit_normal,
	button_menu_back_normal,
	button_menu_save_focus,
	button_menu_load_focus,
	button_menu_hskip_focus,
	button_menu_config_focus,
	button_menu_backlog_focus,
	button_menu_title_focus,
	button_menu_exit_focus,
	button_menu_back_focus,
	button_menu_save_click,
	button_menu_load_click,
	button_menu_hskip_click,
	button_menu_config_click,
	button_menu_backlog_click,
	button_menu_title_click,
	button_menu_exit_click,
	button_menu_back_click,
	menu_base
} SystemImage;

typedef enum eSystemSound{
	sound_default,
	bgm_title,
	se_config_demo,
	se_select_clicked,
	se_select_focus,
	se_button_title_clicked,
	se_button_title_focus
} SystemSound;

//画像・音声共用
typedef enum eFadeType{
	stop,
	no_fade,
	fadein,
	fadeout,
	crossfade,
	use_rule
} FadeType;


//コマンドタグ
typedef enum eCommandType{
	TENG_TAG_COMMENT,
	TENG_TAG_MESSAGE,
	TENG_TAG_LOADCHAR,
	TENG_TAG_DRAWCHAR,
	TENG_TAG_HIDECHAR,
	TENG_TAG_FREECHAR,
	TENG_TAG_IMPORT,
	TENG_TAG_RETURN,
	TENG_TAG_LABEL,
	TENG_TAG_GOTO,
	TENG_TAG_LOADBGIMAGE,
	TENG_TAG_DRAWBGIMAGE,
	TENG_TAG_CHANGEBGIMAGE,
	TENG_TAG_LOADRULE,
	TENG_TAG_FREEBGIMAGE,
	TENG_TAG_FREERULE,
	TENG_TAG_LOADFACE,
	TENG_TAG_SETFACE,
	TENG_TAG_CLEARFACE,
	TENG_TAG_FREEFACE,
	TENG_TAG_LOADBGM,
	TENG_TAG_PLAYBGM,
	TENG_TAG_STOPBGM,
	TENG_TAG_FREEBGM,
	TENG_TAG_LOADSE,
	TENG_TAG_PLAYSE,
	TENG_TAG_STOPSE,
	TENG_TAG_FREESE,
	TENG_TAG_SELECT,
	TENG_TAG_LOVE,
	TENG_TAG_IF,
	TENG_TAG_NAMESET,
	TENG_TAG_SAY,
	TENG_TAG_SETTITLE,
	TENG_TAG_RESETTITLE,
	TENG_TAG_WAIT,
	TENG_TAG_HIDEMSG,
	TENG_TAG_DISPMSG,
	TENG_TAG_PLAYMOVIE,
	TENG_TAG_JUMP,
	TENG_TAG_CHANGECHAR,
	TENG_TAG_DISPGALLERY,
	TENG_TAG_EXMESSAGE
} CmdTag;

typedef enum eFontType{
	font_msg,
	font_rubi,
	font_save_date,
	font_save_msg_and_confirm,
	font_handle_save_yesno
} FontType;

typedef enum eMsgDrawState{
	msg_drawing,
	msg_highspeed,
	msg_complete,
	msg_plus_stopping
} MsgDrawState;

typedef enum eInputKey{
	key_none,
	mouse_left,
	mouse_right,
	key_enter,
	key_escape
} InputKey;



