#include "command_common.h"

using namespace Teng;



CmdBgFree::CmdBgFree(vector<string> text_command, Resources* resources){
	// @bg.free filename
	// [0] tag
	// [1] filename
	this->resources = resources;
	this->scene_scenario = scene_scenario;
	args = text_command;
	force_next = true;
}

CmdBgFree::~CmdBgFree(){

}


void CmdBgFree::execute(){
	string filename = args[1];

	resources->erase(ResourceType::back_image, filename);

	is_executed = true;
	DEBUG putLog();
}

