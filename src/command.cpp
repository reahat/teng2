#include "command.h"

using namespace Teng;

Command::Command() : is_executed(false){};
Command::~Command(){};

void Command::putLog(){
	//printf("[INFO] Executed -> ");
	//for (auto str : args){ printf("%s ", str.c_str()); }
	string msg;
	for (auto str : args){
		msg += (str + " ");
	}
	log_info("Executed -> %s", msg.c_str());
	//printf("\n");
}