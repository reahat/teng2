#pragma once
#include "resource_object_factory.h"
#include "character_object.h"

namespace Teng{
	class CharacterObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}