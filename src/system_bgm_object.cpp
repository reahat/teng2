#include "system_bgm_object.h"

using namespace Teng;

SystemBgmObject::SystemBgmObject(vector<string> command){
	// [0] ディレクトリパス
	// [1] option_type
	// [2] ファイル名
	// [3] volume
	// [4] loop_point
	string fullpath = command[0] + command[2];

	DxLib::SetCreateSoundDataType(DX_SOUNDDATATYPE_MEMPRESS);

	handle = DxLib::LoadSoundMem(fullpath.c_str());
	if (!Resources::isFileExist(fullpath)){
		log_error("システム音声の読み込みに失敗しました: %s", command[2].c_str());
		RAISE(ENGINE_SOUND_SYSTEM_LOAD_FAILED);
	}
	file_name = command[2];
	option_type = stoi(command[1]);
	volume = stoi(command[3]);
	if (command.size() > 4){
		loop_point = stoi(command[4]);
		DxLib::SetLoopPosSoundMem(stoi(command[4]), handle);
	}
	else{
		loop_point = 0;
	}
	fade_power = 0;
	fade_type = FadeType::no_fade;
	is_playing = false;
	log_info("Sound File loading : %s", file_name.c_str());
}

void SystemBgmObject::play(Config* config){
	ConfigData& cf = config->getSetting();
	double cf_vol = (static_cast<double>(cf.master_volume) / 255.0) * (static_cast<double>(cf.bgm_volume) / 255.0);
	double play_vol = 255 * cf_vol * (static_cast<double>(volume) / 255.0);
	DxLib::ChangeVolumeSoundMem(static_cast<int>(play_vol), handle);
	if (DxLib::PlaySoundMem(handle, DX_PLAYTYPE_LOOP, TRUE) == -1)
		RAISE(ENGINE_SOUND_SYSTEM_PLAY_FAILED);
	is_playing = true;
}