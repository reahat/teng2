#pragma once
#include <string>
#include "command.h"
#include <vector>
#include "resources.h"
#include "scene_scenario.h"
#include "backlog.h"
using namespace std;
namespace Teng{

	class CmdBgLoad2 : public Command{
	public:
		CmdBgLoad2(vector<string> text_command, Resources* resources);
		~CmdBgLoad2();
		void execute();
	private:
		string tag;
		string filename;
		Resources* resources;
	};
	class CmdFaceLoad : public Command{
	public:
		CmdFaceLoad(vector<string> text_command, Resources* resources);
		~CmdFaceLoad();
		void execute();
	private:
		string tag;
		string filename;
		Resources* resources;
	};
	class CmdFaceClear : public Command{
	public:
		CmdFaceClear(vector<string> text_command, Resources* resources);
		~CmdFaceClear();
		void execute();
	private:
		string tag;
		string filename;
		Resources* resources;
	};
	class CmdFaceSet : public Command{
	public:
		CmdFaceSet(vector<string> text_command, Resources* resources);
		~CmdFaceSet();
		void execute();
	private:
		string tag;
		string filename;
		Resources* resources;
	};
	class CmdFaceFree : public Command{
	public:
		CmdFaceFree(vector<string> text_command, Resources* resources);
		~CmdFaceFree();
		void execute();
	private:
		string tag;
		string filename;
		Resources* resources;
	};
	class CmdRuleLoad : public Command{
	public:
		CmdRuleLoad(vector<string> text_command, Resources* resources);
		~CmdRuleLoad();
		void execute();
	private:
		string tag;
		string filename;
		Resources* resources;
	};
	class CmdRuleFree : public Command{
	public:
		CmdRuleFree(vector<string> text_command, Resources* resources);
		~CmdRuleFree();
		void execute();
	private:
		string tag;
		string filename;
		Resources* resources;
	};
	class CmdBgDraw : public Command{
	public:
		CmdBgDraw(vector<string> text_command, Resources* resources);
		~CmdBgDraw();
		void execute();
	private:
		string tag;
		string filename;
		Resources* resources;
	};
	class CmdBgChange : public Command{
	public:
		CmdBgChange(vector<string> text_command, Resources* resources);
		~CmdBgChange();
		void execute();
	private:
		string tag;
		string bg_filename;
		int fade_type;
		int fade_power;
		int fade_threshold;
		string rule_filename;
	};
	class CmdBgClear : public Command{
	public:
		CmdBgClear(vector<string> text_command, Resources* resources);
		~CmdBgClear();
		void execute();
	private:
		Resources* resources;
	};
	class CmdBgFree : public Command{
	public:
		CmdBgFree(vector<string> text_command, Resources* resources);
		~CmdBgFree();
		void execute();
	private:
		Resources* resources;
	};
	class CmdMsg : public Command{
	public:
		CmdMsg(vector<string> text_command, Resources* resources);
		~CmdMsg();
		void execute();
		void update();
		int draw();
		void setInput(const int any_input);
		bool isShiftJisFirstByte(const unsigned char c);
		bool isFrontProhibitChar(const unsigned char first_byte, const unsigned char second_byte);
		bool isEndProhibitChar	(const unsigned char first_byte, const unsigned char second_byte);
		bool isContinuousChars(const unsigned char first_1, const unsigned char first_2, const unsigned char second_1, const unsigned char second_2);
		int  getDrawState(){ return draw_state; }
		void setDrawState(const int state){ draw_state = state; }
		int send(vector<Command*>& queue, int& it_num);
		const int checkMsgAdvance();
		struct Rubi{
			int base_line_num;
			int base_loc_byte;
			string rubi_text;
		};
		static const int RUBI_FONT_SIZE = 10;
		static const int MSG_FONT_SIZE = 22;
	protected:
		string tag;
		string full_msg;
		vector<string> msg_line;
		int current_line;
		int current_char;
		int msg_color;
		handle msg_font;
		handle rubi_font;
		int draw_state;
		list<int> plus_location[3];
		list<Rubi> rubi_list;
		string char_name;
		int calcRubiXAdjust(const string& rubi);
	};
	class CmdSay : public CmdMsg{
	public:
		CmdSay(vector<string> text_command, Resources* resources);
		~CmdSay(){};
	};
	class CmdExMsg : public Command{
	public:
		CmdExMsg(vector<string> text_command, Resources* resources);
		~CmdExMsg(){};
		void execute(){};
		vector<string> getCmd(){ return hand_cmd; }
	private:
		vector<string> hand_cmd;
	};
	class CmdNameset : public Command{
	public:
		CmdNameset(vector<string> text_command, Resources* resources);
		~CmdNameset(){};
		void execute();
	};
	class CmdLabel : public Command{
	public:
		CmdLabel(vector<string> text_command, Resources* resources);
		~CmdLabel(){};
		void execute();
	};
	class CmdGoto : public Command{
	public:
		CmdGoto(vector<string> text_command, Resources* resources);
		~CmdGoto(){};
		void execute();
		int send(vector<Command*>& queue, int& it_num);
	};
	class CmdJump : public Command{
	public:
		CmdJump(vector<string> text_command, Resources* resources);
		~CmdJump(){};
		static int CmdJump::send(vector<Command*>& queue, const string& label_to, int& it_num);
		//int send(unique_ptr<CommandQueue> command_queue);
		void execute();
	};
	class CmdReturn : public Command{
	public:
		CmdReturn(vector<string> text_command, Resources* resources, SceneScenario* scene_scenario);
		~CmdReturn(){};
		void beforeExecute(void* arg1);
		//void beforeExecute(SceneScenario* scene_scenario);
		void execute();
		string removeExtension(const string file_name);
	};

}