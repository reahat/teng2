#include "command_common.h"

using namespace Teng;



CmdFaceFree::CmdFaceFree(vector<string> text_command, Resources* resources){
	// @face.free filename
	// [0] tag
	// [1] filename
	this->resources = resources;
	this->scene_scenario = scene_scenario;
	args = text_command;
	force_next = true;
}

CmdFaceFree::~CmdFaceFree(){

}


void CmdFaceFree::execute(){
	string filename = args[1];

	resources->erase(ResourceType::face, filename);

	is_executed = true;
	DEBUG putLog();
}

