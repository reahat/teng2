#include "command_common.h"

using namespace Teng;


CmdFaceLoad::~CmdFaceLoad(){
	
}

CmdFaceLoad::CmdFaceLoad(vector<string> text_command, Resources* resources){
	this->resources = resources;
	args = text_command;
	tag = text_command[0];
	filename = text_command[1];
	force_next = true;
}

void CmdFaceLoad::execute(){
	vector<string> script;
	script.push_back(filename);
	resources->add(ResourceType::face, script);
	is_executed = true;
	DEBUG putLog();
}