#include "common.h"


#include <windows.h>
#pragma warning ( disable : 4996 )

using namespace Teng;

void Logger::OutputLog(const LogType log_type, const char* lpszFuncName, const char* lpszFormat, ...){
//	char* lpszFuncName = (__FUNCTION__);
	va_list args;
	va_start(args, lpszFormat);
	TCHAR szBuffer[1024] = { 0 };
	vsprintf(szBuffer, lpszFormat, args);

	SYSTEMTIME st;
	GetSystemTime(&st);
	char szTime[25] = { 0 };
	wsprintf(szTime, "%04d-%02d-%02d %02d:%02d:%02d.%03d",
		st.wYear, st.wMonth, st.wDay,
		st.wHour + 9, st.wMinute, st.wSecond, st.wMilliseconds);

	char* logtype;
	switch (log_type){
	case LogType::info: logtype = "[INFO]"; break;
	case LogType::warn: logtype = "[WARN]"; break;
	case LogType::debug: logtype = "[DEBUG]"; break;
	case LogType::error: logtype = "[ERROR]"; break;
	default: logtype = ""; break;
	}
	printf("%s %s %s - %s\n", szTime, logtype, lpszFuncName, szBuffer);
	va_end(args);
}
