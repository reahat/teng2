#include "command_common.h"
#include "util.hpp"
using namespace Teng;


CmdReturn::CmdReturn(vector<string> text_command, Resources* resources, SceneScenario* scene_scenario){
	this->resources = resources;
	this->scene_scenario = scene_scenario;
	args = text_command;
	force_next = true;
}

//void CmdReturn::beforeExecute(SceneScenario* scene_scenario){
void CmdReturn::beforeExecute(void* arg1){
	//resources = static_cast<Resources*>(arg1);
	scene_scenario = static_cast<SceneScenario*>(arg1);
}

void CmdReturn::execute(){
	string from_script = scene_scenario->getJumpFromScript();
	from_script = removeExtension(from_script);

	scene_scenario->getCommandQueue().reset(
		new CommandQueue(resources, from_script, scene_scenario));
	scene_scenario->setCmdList(
		scene_scenario->getCommandQueue()->get());
	scene_scenario->setItNum(scene_scenario->getJumpFromLine());

	int i = 0;
	for (auto cmd : scene_scenario->getCommandQueue()->get()){
		if (i <= scene_scenario->getJumpFromLine()){
			cmd->isExecuted(true);
		}
		else{
			break;
		}
		i++;
	}

	is_executed = true;
	DEBUG putLog();
	log_info("Return to %s # %d", from_script.c_str(), scene_scenario->getJumpFromLine());
}


string CmdReturn::removeExtension(const std::string file_name){
	std::string str = file_name;
	std::string::size_type ext_pos = str.find_last_of('.');
	str.erase(ext_pos, str.size());
	return str;
}