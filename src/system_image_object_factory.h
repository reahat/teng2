#pragma once
#include "resource_object_factory.h"
#include "system_image_object.h"

namespace Teng{
	class SystemImageObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}