#include "resource_object.h"

using namespace Teng;


// 実装は各オブジェクトクラスに任せる
void ResourceObject::play(Config* config){}
void ResourceObject::stop(){};
void ResourceObject::fadein(){};
void ResourceObject::fadeout(){};