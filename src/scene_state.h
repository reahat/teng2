#pragma once
#include "common.h"
#include "resources.h"
#include "input.h"
#include "config.h"
#include "DxLib.h"

namespace Teng{
	// Interface
	class SceneState{
	public:
		virtual void preProcess() = 0;
		virtual void postProcess() = 0;
		virtual scene_t getCurrentState() = 0;

		virtual void checkInput() = 0;
		virtual void drawGraphic() = 0;
		virtual void operateButton() = 0;
		virtual void doScene() = 0;
		virtual void operateDialog() = 0;
		virtual void operateSound() = 0;
		virtual void execFade() = 0;
		virtual void addFrame(int add_count) = 0;
		virtual scene_t getNextScene() = 0;
	};
}