#pragma once
#include "resource_object_factory.h"
#include "font_object.h"

namespace Teng{
	class FontObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}