#include "command_common.h"

using namespace Teng;

CmdExMsg::CmdExMsg(vector<string> text_command, Resources* resources){
	string cmd1, cmd2;

	int target = 1;
	for (size_t i = 0; i < text_command[0].size(); i++){
		//コロン、セミコロンが出現するまで
		if (target == 1){
			string tmp;
			tmp += text_command[0][i];
			tmp += text_command[0][i + 1];
			//カギ括弧なし
			if (tmp == "："){
				target = 2;
				i += 2;
			}
			//カギ括弧あり
			else if (tmp == "；"){
				target = 3;
				i += 2;
			}
			else{
				cmd1 += text_command[0][i];
			}
		}

		//カギ括弧なし
		if (target == 2){
			cmd2 += text_command[0][i];
		}
		//カギ括弧あり
		else if (target == 3){
			if (cmd2.size() == 0) cmd2 += "「";
			cmd2 += text_command[0][i];
		}
	}

	if (target == 3){
		cmd2 += "」";
	}

	if (target == 1){
		string tag = "@msg";
		hand_cmd.push_back(tag);
		hand_cmd.push_back(cmd1);
	}
	else{
		string tag = "@say";
		hand_cmd.push_back(tag);
		hand_cmd.push_back(cmd1);
		hand_cmd.push_back(cmd2);
	}
}