#include "exception.h"

using namespace Teng;

Exception::Exception(const error_t error_code){
	an_error = Errors::find(error_code);
}

Exception::~Exception(){
	delete an_error;
}

void Exception::dialog(){
	ostringstream errormsg;
	errormsg << "E-0x" << hex << an_error->getErrorCode() << "\n" << an_error->what();
	string str = errormsg.str();
	MessageBox(DxLib::GetMainWindowHandle(), str.c_str(), "Application error", MB_ICONERROR);
}

void Exception::dialog(exception e){
	ostringstream errormsg;
	errormsg << "System Error" << "\n" << e.what();
	string str = errormsg.str();
	MessageBox(DxLib::GetMainWindowHandle(), str.c_str(), "Application error", MB_ICONERROR);
}

Exception* Exception::raise(const error_t error_code){
	throw new Exception(error_code);
}

error_t Exception::getErrorCode(){
	return an_error->getErrorCode();
}