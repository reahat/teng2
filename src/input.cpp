#include "input.h"

using namespace Teng;

Input::Input() :
key_space(FALSE),
key_return(FALSE),
key_control(FALSE),
key_escape(FALSE),
key_alt(FALSE),
key_f4(FALSE),
key_f5(FALSE),
key_f9(FALSE),
mouse_left(FALSE),
mouse_right(FALSE),
mouse_wheel_up(FALSE),
mouse_wheel_down(FALSE),
cursor_x(0),
cursor_y(0)
{

}

void Input::checkKeyInput(){
	int rot;
	key_space = DxLib::CheckHitKey(KEY_INPUT_SPACE);
	key_return = DxLib::CheckHitKey(KEY_INPUT_RETURN);
	key_control = DxLib::CheckHitKey(KEY_INPUT_LCONTROL);
	key_escape = DxLib::CheckHitKey(KEY_INPUT_ESCAPE);
	key_alt = DxLib::CheckHitKey(KEY_INPUT_LALT) | DxLib::CheckHitKey(KEY_INPUT_RALT);
	key_f4 = DxLib::CheckHitKey(KEY_INPUT_F4);
	key_f5 = DxLib::CheckHitKey(KEY_INPUT_F5);
	key_f9 = DxLib::CheckHitKey(KEY_INPUT_F9);

	mouse_left = DxLib::GetMouseInput() & MOUSE_INPUT_LEFT;
	mouse_right = DxLib::GetMouseInput() & MOUSE_INPUT_RIGHT;

	rot = DxLib::GetMouseWheelRotVol();
	if (rot == 0){
		mouse_wheel_down = FALSE;
		mouse_wheel_up = FALSE;
	}
	else if (rot < 0){
		mouse_wheel_down = TRUE;
		mouse_wheel_up = FALSE;
	}
	else if (rot > 0){
		mouse_wheel_down = FALSE;
		mouse_wheel_up = TRUE;
	}

	DxLib::GetMousePoint(&cursor_x, &cursor_y);

	if (key_alt & key_f4){
		log_warn("強制終了します。");
		RAISE(ENGINE_INPUT_TERMINATE);
	}
}

bool Input::mouseWheelUpped(){
	static bool before_frame_state = false;
	if (mouse_wheel_up == TRUE){
		// 未入力状態でクリック
		if (!before_frame_state){
			before_frame_state = true;
			return true;
		}
		// 前フレームで押下状態だった場合
		else{
			return false;
		}
	}
	before_frame_state = false;
	return false;
}

bool Input::mouseWheelDowned(){
	static bool before_frame_state = false;
	if (mouse_wheel_down != FALSE){
		// 未入力状態でクリック
		if (!before_frame_state){
			before_frame_state = true;
			return true;
		}
		// 前フレームで押下状態だった場合
		else{
			return false;
		}
	}
	before_frame_state = false;
	return false;
}


bool Input::mouseLeftClicked(){
	static bool before_frame_state = false;
	if (mouse_left != FALSE){
		// 未入力状態でクリック
		if (!before_frame_state){
			before_frame_state = true;
			return true;
		}
		// 前フレームで押下状態だった場合
		else{
			return false;
		}
	}
	before_frame_state = false;
	return false;
}

bool Input::mouseLeftPushing(){
	return mouse_left == 0 ? false : true;
	//return mouse_left;
}

bool Input::mouseRightPushing(){
	return mouse_right == 0 ? false : true;
}

bool Input::mouseRightClicked(){
	static bool before_frame_state = false;
	if (mouse_right != FALSE){
		// 未入力状態でクリック
		if (!before_frame_state){
			before_frame_state = true;
			return true;
		}
		// 前フレームで押下状態だった場合
		else{
			return false;
		}
	}
	before_frame_state = false;
	return false;
}


bool Input::keyF5Pushed(){
	static bool before_frame_state = false;
	if (key_f5 != FALSE){
		// 未入力状態でクリック
		if (!before_frame_state){
			before_frame_state = true;
			return true;
		}
		// 前フレームで押下状態だった場合
		else{
			return false;
		}
	}
	before_frame_state = false;
	return false;
}

bool Input::keyF9Pushed(){
	static bool before_frame_state = false;
	if (key_f9 != FALSE){
		// 未入力状態でクリック
		if (!before_frame_state){
			before_frame_state = true;
			return true;
		}
		// 前フレームで押下状態だった場合
		else{
			return false;
		}
	}
	before_frame_state = false;
	return false;
}

void Input::disableAllKeys(){
	key_space = 0;
	key_return = 0;
	key_control = 0;
	key_escape = 0;
	key_alt = 0;
	key_f4 = 0;
	key_f5 = 0;
	key_f9 = 0;
	mouse_left = 0;
	mouse_right = 0;
	mouse_wheel_up = 0;
	mouse_wheel_down = 0;
}

int Input::cursorX(){
	return cursor_x;
}

int Input::cursorY(){
	return cursor_y;
}


bool Input::cursorIn(const int left, const int right, const int top, const int bottom){
	if (cursor_x < left)    return false;
	if (cursor_x >= right)  return false;
	if (cursor_y < top)     return false;
	if (cursor_y >= bottom) return false;
	return true;
}