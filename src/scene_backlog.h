#pragma once
#include "scene_state.h"
#include "exception.h"
#include "util.hpp"

namespace Teng{
	class SceneBacklog : public SceneState{
	public:
		SceneBacklog(Resources* resources, Input* input, Config* config);
		void preProcess();
		void postProcess();
		scene_t getCurrentState();

		void checkInput();
		void drawGraphic();
		void operateButton();
		void doScene();
		void operateDialog();
		void operateSound();
		void execFade();
		void addFrame(int add_count);
		scene_t getNextScene(){ return next_scene; }
	private:
		static const int TENG_BACKLOG_POINT_X = 100;
		static const int TENG_BACKLOG_POINT_Y = 100;
		static const int TENG_BACKLOG_LINE_SPACE = 20;
		static const int TENG_MESSAGE_FONT_SIZE = 22;
		Resources* resources;
		Input* input;
		Config* config;
		ULONG frame;
		int   fade_type;
		scene_t next_scene;

		handle color_black;
		handle color_white;
		handle background;
		handle msg_font;
		int block_cnt;
		int line_cnt;
		Backlog logs;
		list<BacklogMessage> a_log;
		bool seeklog_flg;
		int log_line_cnt;
		list<BacklogMessage>::iterator a_log_it;
		list<string>::iterator msg_it;
		list<string> log_msgs;


		bool cursorInButton(ResourceObject& resource);
		bool cursorIn(const int left, const int right, const int top, const int bottom);
	};

}