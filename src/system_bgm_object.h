#pragma once
#include <string>
#include <vector>
#include "common.h"
#include "resource_object.h"
#include "resources.h"
using namespace std;

namespace Teng{
	class SystemBgmObject : public ResourceObject{
	public:
		SystemBgmObject(vector<string> command);
		void play(Config* config);
	};

}