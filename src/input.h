#pragma once
#include "DxLib.h"
#include "error_code.h"
#include "common.h"
#include "exception.h"

namespace Teng{
	class Input{
	public:
		Input();
		~Input(){};

		//キーの入力状態を取得する
		//必ず毎フレーム実行すること
		void checkKeyInput();

		bool mouseLeftClicked();
		bool mouseLeftPushing();
		bool mouseRightPushing();
		bool mouseRightClicked();
		bool mouseWheelUpped();
		bool mouseWheelDowned();
		bool keyF5Pushed();
		bool keyF9Pushed();
		int  cursorX();
		int  cursorY();
		bool cursorIn(const int left, const int right, const int top, const int bottom);

		//すべてのキー入力を1フレームだけ無効にする
		void disableAllKeys();

	private:
		int key_space;
		int key_return;
		int key_control;
		int key_escape;
		int key_alt;
		int key_f4;
		int key_f5;
		int key_f9;
		int mouse_left;
		int mouse_right;
		int mouse_wheel_up;
		int mouse_wheel_down;
		int cursor_x;
		int cursor_y;
	};
}