#include "common.h"
#include "initializer.h"

using namespace Teng;


void Initializer::initialize(){
	const char*  DEFAULT_WINDOW_TITLE  = "Ephialtes";
	const char*  DX_KEY_ARCHIVE_STRING = "@T3nG1Ne_cC_";
	const char*  DX_ARCHIVE_EXTENSION  = "data";
	const char*  BUILD_DATE       = __DATE__;
	const int    WINDOW_MODE      = TRUE;
	const int    FULLSCREEN_MODE  = FALSE;
	const size_t WINDOW_TITLE_MAX = 128;
	const int    SCREEN_WIDTH     = 1024;
	const int    SCREEN_HEIGHT    = 768;
	const int    SCREEN_COLOR_BIT = 16;


	//デフォルトのウィンドウテキスト
	char window_fullname[128] = "";
	char debug_suffix[128]    = "";
	DEBUG sprintf_s(debug_suffix, WINDOW_TITLE_MAX, " | DEBUG MODE - %s", BUILD_DATE);
	sprintf_s(window_fullname, WINDOW_TITLE_MAX, "%s%s", DEFAULT_WINDOW_TITLE, debug_suffix);
	DxLib::SetMainWindowText(window_fullname);
	
	//ウィンドウ設定
	DxLib::ChangeWindowMode(WINDOW_MODE);
	DxLib::SetGraphMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_COLOR_BIT);

	//ウィンドウ枠タイプの設定
	DxLib::SetWindowStyleMode(0);
	//DirectInputの使用可否
	DxLib::SetUseDirectInputFlag(IF_DEBUG_FALSE);

	//非同期ロード
	DxLib::SetUseASyncLoadFlag(TRUE);

	//アーカイブ設定
	DxLib::SetDXArchiveExtension(DX_ARCHIVE_EXTENSION);
	DxLib::SetDXArchiveKeyString(DX_KEY_ARCHIVE_STRING);
	//DxLibのログ出力
	DxLib::SetOutApplicationLogValidFlag(FALSE);
	//非アクティブ時に動作停止するかどうか
	DxLib::SetAlwaysRunFlag(IF_DEBUG_FALSE);
	//×ボタンで終了しない
	DxLib::SetWindowUserCloseEnableFlag(FALSE);

	//DxLibの初期化
	DxLib::DxLib_Init();

	//マウスカーソル表示
	DxLib::SetMouseDispFlag(TRUE);

	//コンソール画面の表示
	FILE *console;
	DEBUG AllocConsole();
	DEBUG freopen_s(&console, "CONOUT$", "w", stdout);
	DEBUG freopen_s(&console, "CONIN$", "r", stdin);
	//画面モード変更時にグラフィックハンドルをリセットしない
	DxLib::SetChangeScreenModeGraphicsSystemResetFlag(FALSE);
	Errors::initialize();
}


void Initializer::loadResources(Resources* resources){
	Errors::initialize();
	// ロゴ画像だけは起動時にロードする
	vector<string> cmd;
	
	cmd.push_back(Util::convertString(SystemImage::bg_logo_1));
	cmd.push_back("logo.png");
	cmd.push_back("0");
	cmd.push_back("0");
	resources->add(ResourceType::system_graphic, cmd);
}

