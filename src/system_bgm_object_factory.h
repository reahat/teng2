#pragma once
#include "resource_object_factory.h"
#include "system_bgm_object.h"

namespace Teng{
	class SystemBgmObjectFactory : public ResourceObjectFactory{
	public:
		ResourceObject* create(vector<string> command);
	};


}