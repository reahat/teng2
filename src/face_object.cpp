#include "face_object.h"

using namespace Teng;

FaceObject::FaceObject(vector<string> command){
	// [0] ファイル名
	string fullpath = Resources::PathFace() + command[0];
	handle = DxLib::LoadGraph(fullpath.c_str());
	if (!Resources::isFileExist(fullpath)){
		log_error("フェイス画像の読み込みに失敗しました: %s", command[0].c_str());
		RAISE(ENGINE_GRAPHIC_FACE_LOAD_FAILED);
	}
	file_name = command[0];
	is_visible = false;
	log_info("FaceImage file loading : %s", file_name.c_str());

}

