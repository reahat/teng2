#pragma once
#include "error_object.h"
#include "errors.h"
#include "common.h"
#include <string>
#include <map>
#include <sstream>
#include "DxLib.h"
#include <Windows.h>

using namespace std;

namespace Teng{
	class Exception{
	public:
		~Exception();
		static Exception* raise(const error_t error_code);
		void dialog();
		error_t getErrorCode();
		static void dialog(exception e); // TENG以外の例外発生時用
	private:
		Exception(const error_t error_code);
		ErrorObject* an_error;
	};
}